/*
 * File      : XDOS.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef _XDOS_H
#define _XDOS_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "TypeDef.h"

typedef void (*tpfnTaskEntry)(void);
typedef void (*tpfnIntTaskEntry)(tdu8 tick);
typedef struct
{
	tdbl TaskValid;//任务有效性
	tpfnTaskEntry fpTaskEntry;
}tXDOSTaskCtrlBlock;

typedef struct 
{
	tdbl TaskValid;//任务有效性
	tdu1 TaskTick;//任务计数器
	tdu1 TaskDiv;//任务分频数
	tpfnIntTaskEntry fpIntTaskEntry;
}tXDOSIntTaskCtrlBlock;

extern void XDOS_Init(void);

extern void XDOS_SetTaskDiv(tdu1 div);

extern tdu1 XDOS_GetTaskDiv(void);

extern tdbl XDOS_RegisterTask(tpfnTaskEntry task);

extern tdbl XDOS_RegisterIntTask(tdu2 taskDiv,tdu2 taskOffset,tpfnIntTaskEntry intTask);

extern void XDOS_Start(void);

extern void XDOS_ExecTask(void);

extern tdu8 XDOS_GetTick(void);

extern tdbl XDOS_GetTickFlag(void);

extern void XDOS_SetTickFlag(tdbl flag);
#ifdef __cplusplus
}
#endif
#endif /* __XDOS_H */

