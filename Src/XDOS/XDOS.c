/*
 * File      : XDOS.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "XDOS.h"
#include "../OsPort/OsPort.h"
#include "../OSShell/OsConfig.h"

static tdu2 OsTaskNum = 0;
static tdu2 OsIntTaskNum = 0;
static tdu1 OsTaskDiv = 20;//Task任务默认20分频
static tXDOSTaskCtrlBlock OsTaskArray[XDOS_TASK_MAX_NUM];
static tXDOSIntTaskCtrlBlock  OsIntTaskArray[XDOS_INTTASK_MAX_NUM];
static void XDOS_Tick(void);
void XDOS_Init()
{
	tdu2 i;
	OsTaskNum = 0;
	for (i=0;i<XDOS_TASK_MAX_NUM;i++)
	{
		OsTaskArray[i].TaskValid = FALSE;
		OsTaskArray[i].fpTaskEntry = NULL;
	}
	
	for (i=0;i<XDOS_INTTASK_MAX_NUM;i++)
	{
		OsIntTaskArray[i].TaskValid = FALSE;
		OsIntTaskArray[i].TaskTick = 0;
		OsIntTaskArray[i].TaskDiv = 1;
		OsIntTaskArray[i].fpIntTaskEntry = NULL;
	}
	//调用底层函数，初始化用于提供时钟的定时器
	OsPort_Init(XDOS_TICK_TIM_FRE);
	//向时钟中注册回调函数
	OsPort_RegisterTickFunc(XDOS_Tick);
}
void XDOS_SetTaskDiv(tdu1 div)
{
	OsTaskDiv = div;
}

tdu1 XDOS_GetTaskDiv(void)
{
	return OsTaskDiv;
}

tdbl XDOS_RegisterTask(tpfnTaskEntry task)
{
	if (task == NULL)
	{
		return FALSE;
	}
	if (OsTaskNum >= XDOS_TASK_MAX_NUM)
	{
		return FALSE;
	}
	OsTaskArray[OsTaskNum].TaskValid = TRUE;
	OsTaskArray[OsTaskNum].fpTaskEntry = task;
	OsTaskNum++;
	return TRUE;
}

tdbl XDOS_RegisterIntTask(tdu2 taskDiv,tdu2 taskOffset,tpfnIntTaskEntry intTask)
{
	if (intTask == NULL)
	{
		return FALSE;
	}
	if (taskDiv == 0)
	{
		return FALSE;
	}
	if (OsIntTaskNum >= XDOS_INTTASK_MAX_NUM)
	{
		return FALSE;
	}
	OsIntTaskArray[OsIntTaskNum].TaskValid = TRUE;
	OsIntTaskArray[OsIntTaskNum].TaskTick = taskOffset%taskDiv;
	OsIntTaskArray[OsIntTaskNum].TaskDiv = taskDiv;
	OsIntTaskArray[OsIntTaskNum].fpIntTaskEntry = intTask;
	OsIntTaskNum++;
	return TRUE;
}
static void XDOS_ExecIntTask(tdu4 tick)
{
	tdu1 i;
	for (i = 0;i<XDOS_INTTASK_MAX_NUM;i++)
	{
		if ((OsIntTaskArray[i].TaskValid == TRUE) && (OsIntTaskArray[i].fpIntTaskEntry != NULL)) 
		{
			OsIntTaskArray[i].TaskTick++;
			if (OsIntTaskArray[i].TaskTick == OsIntTaskArray[i].TaskDiv)
			{
				OsIntTaskArray[i].fpIntTaskEntry(tick);
				OsIntTaskArray[i].TaskTick = 0;
			}
		}
	}


	
}

void XDOS_ExecTask()
{
	tdu1 i;
	for (i = 0;i<OsTaskNum;i++)
	{
		if ((OsTaskArray[i].TaskValid == TRUE)&&(OsTaskArray[i].fpTaskEntry != NULL))
		{
			OsTaskArray[i].fpTaskEntry();
		}
	}
}



void XDOS_Start()
{
	OsPort_Start();
}

void XDOS_Stop()
{
	OsPort_Stop();
}
static volatile tdbl TickFlag = FALSE;
static volatile tdu8 TickCnt = 0;
static volatile tdu2 TickDiv = 0;
static void XDOS_Tick()
{

	TickCnt++;
	TickDiv++;  
	if (TickDiv == OsTaskDiv)
	{
		TickFlag = TRUE;
		TickDiv = 0;
	}
	XDOS_ExecIntTask(TickCnt);
}
tdu8 XDOS_GetTick(void)
{
    return TickCnt;
}
tdbl XDOS_GetTickFlag(void)
{
	return TickFlag;
}

void XDOS_SetTickFlag(tdbl flag)
{

	TickFlag = flag;
}


