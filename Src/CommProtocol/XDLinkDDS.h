/*
 * File      : XDLinkDDS.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __XDLINK_DDS_H
#define __XDLINK_DDS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TypeDef.h"


__packed typedef struct
{
	tdbl IsDDSInit;
	tpFnGetIDBytes SRHandler[256];
	tdu1 ThisID;
	tdu2 ThisLength;
	tdu1 *ThisData;
}tXDLinkDDS;

extern tdbl XDLinkDDS_Register(tdu1 ID, tpFnGetIDBytes fp, tXDLinkDDS *DDS);

extern void XDLinkDDS_Process(const tXDLinkDDS *DDS);

#ifdef __cplusplus
}
#endif

#endif

