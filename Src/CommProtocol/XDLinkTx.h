/*
 * File      : XDLinkTx.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __XDLINK_TX_H
#define __XDLINK_TX_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TypeDef.h"	

/*
 将一组data,按照ID号,组成一组合法的数据,
 formatData为外部输入的缓冲区,合法的数据将放在formatData里面,
 返回值为有效长度

*/
extern tdu2 XDLinkTx_Format(tdu1 *formatData, tdu2 formatDataLength, 
									tdu1 header1, tdu1 header2,
									tdu1 ID, tdu1 *data, tdu2 dataLength);

/*
 将一组data,按照ID号,组成一组合法的数据,
 formatData为外部输入的缓冲区,合法的数据将放在formatData里面,
 返回值为有效长度

*/
extern tdu2 XDLinkTx_Format2(tdu1 *formatData, tdu2 formatDataLength, 
									tdu1 header1, tdu1 header2,
									tdu1 ID1, tdu1 *data1, tdu2 dataLength1,
									tdu1 ID2, tdu1 *data2, tdu2 dataLength2);

extern tdu2 XDLinkTx_FormatHeader(tdu1 *formatData, tdu2 formatDataLength, 
									tdu1 header1, tdu1 header2);

extern tdbl XDLinkTx_AddPackage(tdu1 *formatData, tdu2 formatDataLength, 
									tdu2 *index,
									tdu1 ID, tdu1 *data, tdu2 dataLength);

extern tdu2 XDLinkTx_AddCRC(tdu1 *formatData, tdu2 formatDataLength);

__packed typedef struct
{
	tdbl Valid;
	tdu1 ID;
	tdu1 *Buf;
	tdu2 Length;
}tXDLinkTxPackage;

extern tdu2 XDLinkTx_FormatPackages(tdu1 *formatData, tdu2 formatDataLength, 
									tdu1 header1, tdu1 header2,
									tXDLinkTxPackage *package, tdu2 packageLength);

									
#ifdef __cplusplus
}
#endif
#endif

