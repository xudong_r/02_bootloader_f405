/*
 * File      : XDLinkRx.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */


#include <string.h>
#include "../Include/TypeDef.h"

//#include "../MacroLib/MathCal.h"
#include "../CommProtocol/XDLinkRx.h"
#include "XDLinkDef.h"
//#include "../MacroLib/AES.h"
#include "CRC.h"


void XDLinkRx_RawData(tXDLinkRxState *state, tdu1 data)
{
	tdu1 *buf;
	buf = state->RxBuffer[state->RxBufferIndex];
	if (state->Index == 0)
	{
		if (data == state->Header[0])
		{
			state->Index++;
			buf[0] = data;
		}
	}
	else if (state->Index == 1)
	{
		if (data == state->Header[1])
		{
			state->Index++;
			buf[1] = data;
		}
		else
			state->Index = 0;
	}
	else if (state->Index == 2)//length
	{
		buf[2] = data;
		state->Index++;
	}

	else if (state->Index < XDLink_CalFrameSize(buf[XDLINK_LENGTH_INDEX]))
	{
		buf[state->Index] = data;
		if (state->Index == XDLink_CalSumIndex(buf[XDLINK_LENGTH_INDEX]) + 1)
		{
			state->Index = 0;
			if(state->RxMsgUpdate == FALSE)
			{
				state->RxMsgUpdate = TRUE;
				state->RxBufferIndex = (state->RxBufferIndex + 1) % 2;
			}
			else
				state->LostCnt++;
		}
		else
			state->Index++;
	}
	else
		state->Index = 0;
}
static tdbl XDLinkRx_BytesToFrame(tpFnGetIDBytes fp, tdu1 data[], tdu2 length)
{
	// 计算剩余字节数
	tds2 remLength = data[XDLINK_LENGTH_INDEX] - 2;

	// 计算索引
	tdu1 frameIDIndex = XDLINK_PACKAGE1_ID_INDEX;
	tdu1 frameLengthIndex = XDLINK_PACKAGE1_LENGTH_INDEX;
	tdu1 frameDataIndex = XDLINK_PACKAGE1_DATA_INDEX;
	// 取数据
	tdu1 frameID = data[frameIDIndex];
	tds2 frameLength = data[frameLengthIndex];
	// 保证长度不溢出
	tds2 frameAllLength = frameLength + 2;// 子帧全长度
	if(frameAllLength > remLength)
		return FALSE;	
	// 发布一帧数据
	if(fp != NULL)
		fp(frameID, &data[frameDataIndex], data[frameLengthIndex]);
	// 剩下的字节足够解释出一阵数据, Length, data
	while(remLength - frameAllLength >= 3)
	{
		// 计算剩余字节数
		remLength = remLength - frameAllLength;	
		// 计算索引
		frameIDIndex = frameDataIndex + frameLength;
		frameLengthIndex = frameIDIndex + 1;
		frameDataIndex = frameLengthIndex + 1;
		// 取数据
		frameID = data[frameIDIndex];
		frameLength = data[frameLengthIndex];
		// 保证长度不溢出
		frameAllLength = frameLength + 2;// 子帧全长度
		if(frameAllLength > remLength)
			return FALSE;	
		// 发布一帧数据
		if(fp != NULL)
			fp(frameID, &data[frameDataIndex], data[frameLengthIndex]);
	}
	return TRUE;
}
// 裸数据解释
void XDLinkRx_Process(tpFnGetIDBytes fp, tXDLinkRxState *state)
{
	tdu2 crc = 0;
	tdu2 crcIndex;
	tdu1* buf;
	buf = state->RxBuffer[(state->RxBufferIndex + 1) % 2];
	crcIndex = XDLink_CalSumIndex(buf[XDLINK_LENGTH_INDEX]);
	crc = buf[crcIndex + 1];
	crc = (crc<<8) + buf[crcIndex];
	if(crc == CRC_16(buf,XDLink_CalFrameSize(buf[XDLINK_LENGTH_INDEX]) -2))
	{
		XDLinkRx_BytesToFrame(fp, buf, XDLink_CalFrameSize(buf[XDLINK_LENGTH_INDEX]));
	}
	else
		state->CRCErrCnt++;
	state->RxMsgUpdate = FALSE;
}
