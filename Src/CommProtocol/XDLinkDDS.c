/*
 * File      : XDLinkDDS.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "../CommProtocol/XDLinkDDS.h"
#include <string.h>

tdbl XDLinkDDS_Register(tdu1 ID, tpFnGetIDBytes fp, tXDLinkDDS *DDS)
{
	if(DDS->IsDDSInit == FALSE)
	{
		memset((void *)DDS, 0x00, sizeof(tXDLinkDDS));
		DDS->IsDDSInit = TRUE;
	}
	if(DDS->SRHandler[ID] == 0x00)
	{
		DDS->SRHandler[ID] = fp;
		return TRUE;
	}
	return FALSE;
}

void XDLinkDDS_Process(const tXDLinkDDS *DDS)
{
	if(DDS->IsDDSInit  == FALSE)
		return;
	if(DDS->SRHandler[DDS->ThisID] != 0x00)
	{
		DDS->SRHandler[DDS->ThisID](DDS->ThisID, DDS->ThisData, DDS->ThisLength);
	}
}

