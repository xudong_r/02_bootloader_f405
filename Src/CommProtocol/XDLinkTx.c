/*
 * File      : XDLinkTx.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include <string.h>
#include "../Include/TypeDef.h"

//#include "../MacroLib/MathCal.h"

#include "XDLinkDef.h"
#include "../CommProtocol/XDLinkTx.h"
#include "CRC.h"


/*
 将一组data,按照ID号,组成一组合法的数据,
 formatData为外部输入的缓冲区,合法的数据将放在formatData里面,
 返回值为有效长度

*/
tdu2 XDLinkTx_Format(tdu1 *formatData, tdu2 formatDataLength,
							tdu1 header1, tdu1 header2,
							tdu1 ID, tdu1 *data, tdu2 dataLength)
{
	tdu2 dataLengthMax;
	tdu2 formatDataMin;
	tdu2 crc = 0;
	tdu2 crcIndex;
	// 数据长度合法性判断
	dataLengthMax = 256 - 2 - 2;// 长度字节,最大256字节,256，256 - 校验2字节 - ID字节 - 子帧全长度
	if(dataLength > dataLengthMax)
		return 0;
	formatDataMin = dataLength + 1 + 1 + 2 + 2 + 1;//data[dataLength] + ID(1) + dataLength(1) + crc(2) + header(2) + length
	if(formatDataLength < formatDataMin)
		return 0;
	formatData[0] = header1;
	formatData[1] = header2;
	formatData[2] = dataLength + 1 + 1 + 2;//data[dataLength] + ID(1) + dataLength(1) + crc(2)
	formatData[3] = ID;
	formatData[4] = dataLength;
	memcpy((void*)&formatData[5], (void*)data, dataLength);
	
	crcIndex = XDLink_CalSumIndex(formatData[XDLINK_LENGTH_INDEX]);
	crc = CRC_16(formatData,XDLink_CalFrameSize(formatData[XDLINK_LENGTH_INDEX]) -2);
	formatData[crcIndex] = crc & 0xFF;
	formatData[crcIndex + 1] = crc >> 8;
	return XDLink_CalFrameSize(formatData[XDLINK_LENGTH_INDEX]);
}

/*
 将两组data, 按照ID号, 组成一组合法的数据,
 formatData为外部输入的缓冲区,合法的数据将放在formatData里面,
 返回值为有效长度

*/
tdu2 XDLinkTx_Format2(tdu1 *formatData, tdu2 formatDataLength, 
							tdu1 header1, tdu1 header2,
							tdu1 ID1, tdu1 *data1, tdu2 dataLength1,
							tdu1 ID2, tdu1 *data2, tdu2 dataLength2)
{
	tdu2 dataLengthMax;
	tdu2 formatDataMin;
	tdu2 crc = 0;
	tdu2 crcIndex;
	// 数据长度合法性判断
	// 长度字节,最大256字节,256，256 - 校验2字节 - ID字节 - 子帧全长度
	dataLengthMax = 256 - 2 - 2 - 2;
	if(dataLength1 + dataLength2 > dataLengthMax)
		return 0;
	formatDataMin = dataLength1 + 1 + 1 + dataLength2 + 1 + 1 + 2 + 2 + 1;
	if(formatDataLength < formatDataMin)
		return 0;
	formatData[0] = header1;
	formatData[1] = header2;
	formatData[2] = dataLength1 + 1 + 1 + dataLength2 + 1 + 1 + 2;

	formatData[3] = ID1;
	formatData[4] = dataLength1;
	memcpy((void*)&formatData[5], (void*)data1, dataLength1);
	
	formatData[5 + dataLength1] = ID2;
	formatData[6 + dataLength1] = dataLength2;
	memcpy((void*)&formatData[7 + dataLength1], (void*)data2, dataLength2);
	
	crcIndex = XDLink_CalSumIndex(formatData[XDLINK_LENGTH_INDEX]);
	crc = CRC_16(formatData,XDLink_CalFrameSize(formatData[XDLINK_LENGTH_INDEX]) -2);
	formatData[crcIndex] = crc & 0xFF;
	formatData[crcIndex + 1] = crc >> 8;
	return XDLink_CalFrameSize(formatData[XDLINK_LENGTH_INDEX]);
}


tdu2 XDLinkTx_FormatHeader(tdu1 *formatData, tdu2 formatDataLength, 
									tdu1 header1, tdu1 header2)
{
	if(formatDataLength < XDLINK_LENGTH_MIN)
		return 0;
	memset((void*)formatData, 0x00, formatDataLength);
	formatData[0] = header1;
	formatData[1] = header2;
	formatData[2] = 2;
	return 3;
}

tdbl XDLinkTx_AddPackage(tdu1 *formatData, tdu2 formatDataLength, 
									tdu2 *index,
									tdu1 ID, tdu1 *data, tdu2 dataLength)
{
	tdu2 length;
	tdu2 _index;
	_index = *index;
	length = _index + 1 + 1 + dataLength + 2;
	if(length > formatDataLength)
		return FALSE;
	length = formatData[2] + dataLength + 1 + 1;
	if(length > 0xFF)
		return FALSE;
	formatData[2] += dataLength + 1 + 1;
	formatData[_index++] = ID;
	formatData[_index++] = dataLength;
	memcpy((void*)&formatData[_index], (void*)data, dataLength);
	_index += dataLength;
	*index = _index;
	return TRUE;
}

tdu2 XDLinkTx_AddCRC(tdu1 *formatData, tdu2 formatDataLength)
{
	tdu2 crc = 0;
	tdu2 crcIndex;
	crcIndex = XDLink_CalSumIndex(formatData[XDLINK_LENGTH_INDEX]);
	crc = CRC_16(formatData,XDLink_CalFrameSize(formatData[XDLINK_LENGTH_INDEX]) -2);
	formatData[crcIndex] = crc & 0xFF;
	formatData[crcIndex + 1] = crc >> 8;
	return XDLink_CalFrameSize(formatData[XDLINK_LENGTH_INDEX]);
}

tdu2 XDLinkTx_FormatPackages(tdu1 *formatData, tdu2 formatDataLength, 
									tdu1 header1, tdu1 header2,
									tXDLinkTxPackage *package, tdu2 packageLength)
{
	tdu2 i;
	tdu2 length;
	tdbl result;
	length = XDLinkTx_FormatHeader(formatData, formatDataLength, header1, header2);
	if(length == 0)
		return 0;
	for(i=0;i<packageLength;i++)
	{
		if(package->Valid == TRUE)
		{
			result = XDLinkTx_AddPackage(formatData, formatDataLength, &length,
								package->ID, package->Buf, package->Length);
			if(result != TRUE)
				break;
			package->Valid = FALSE;
		}
	}
	length = XDLinkTx_AddCRC(formatData, formatDataLength);
	return length;
}
