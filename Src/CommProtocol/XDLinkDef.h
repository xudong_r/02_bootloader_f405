/*
 * File      : XDLinkDef.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef _XDLINK_DEF_H
#define _XDLINK_DEF_H

#ifdef __cplusplus
extern "C" {
#endif
 
#define XDLINK_LENGTH_INDEX						(2)
#define XDLINK_DATA_INDEX						(3)

// 内部逻辑宏定义
#define XDLINK_PACKAGE1_ID_INDEX				(3)
#define XDLINK_PACKAGE1_LENGTH_INDEX			(4)
#define XDLINK_PACKAGE1_DATA_INDEX				(5)

#define XDLink_CalSumIndex(x)					(x+1)
#define XDLink_CalFrameSize(x)					(x+3)

#define XDLINK_LENGTH_MIN						(8)
#define XDLINK_LENGTH_MAX						(258)

#ifdef __cplusplus
}
#endif

#endif

