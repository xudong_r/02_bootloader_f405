/*
 * File      : XDLinkRx.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef _XDLINK_RX_H
#define _XDLINK_RX_H

#ifdef __cplusplus
extern "C" {
#endif


__packed typedef struct
{
	tdbl RxMsgUpdate;
	tdu1 Header[2];
	tdu2 Index;

	tdu1 RxBuffer[2][256 + 3];
	tdu1 RxBufferIndex;
	
	tdu4 CRCErrCnt;
	tdu4 LostCnt;
}tXDLinkRxState;

extern void XDLinkRx_RawData(tXDLinkRxState *state, tdu1 data);

extern void XDLinkRx_Process(tpFnGetIDBytes fp, tXDLinkRxState *state);

#ifdef __cplusplus
}
#endif

#endif

