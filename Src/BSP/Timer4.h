/*
 * File      : Timer4.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __TIMER4_H
#define __TIMER4_H

#ifdef __cplusplus
extern "C"
{
#endif
#include "TypeDef.h"
#include "stm32f4xx_hal.h"
#include "Timer.h"
extern TIM_HandleTypeDef* Timer4_GetHandle(void);

extern void Timer4_Base_HwInit(tdu4 frequency,tdu4 period,tdu2 dutyCycle);

extern void Timer4_Start(void);

extern void Timer4_Start_IT(void);

extern void Timer4_PeriodElapsedCallback(void);

extern void Timer4_SetElapsedCallback(tpfnTimerElapsedCallback fp);
#ifdef __cplusplus
}
#endif
#endif /* __TIMER4_H */

