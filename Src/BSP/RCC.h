/*
 * File      : RCC.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef __RCC_H
#define __RCC_H
#include "BSPFile.h"

extern void Rcc_Init(void);
extern void RCC_DisablePeriph(void);

#endif /* __RCC_H */
