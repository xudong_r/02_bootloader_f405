/*
 * File      : BootLoader.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#include "BootLoader.h"
#include "stm32f4xx_hal.h"
#include "TypeDef.h"
#include "FLASH.h"
#include "BSPFile.h"
#include "RCC.h"
#include "system_stm32f4xx.h"

extern int main(void);
__asm void hvIrqDisable(void)
{
    CPSID   I		// 关闭中断
    BX      LR		
}
void JumpToBootLoader(void)
{
	#define LXYSTCTRL                  (*(volatile unsigned long *)0xE000E010)	
    
    RCC_DisablePeriph();

	LXYSTCTRL=0;			
	//Disable sysinttick	
	LXYSTCTRL = (0<<0) | (0<<1) ;
	
	hvIrqDisable();			
	__asm {msr CONTROL  ,0X00}			 
	(*((void (*)(void))(*(unsigned long *)0x2c)))();  
}



//tpFnAction InitHandle = 0;
//void BootLoader_SetInitHandlev(tpFnAction fp)
//{
//	InitHandle = fp;
//}


__asm void main_Init(void)
{
	IMPORT  __main
	LDR     R0, =__main
	BX      R0
}

__asm void hvIrqEnable(void)
{
    CPSIE   I		// 使能中断
    BX      LR		
}


void SVC_Handler(void)
{
	SCB->VTOR = FLASH_BASE;
	hvIrqEnable();
	SystemInit();
	main_Init();
	while(1);

}
