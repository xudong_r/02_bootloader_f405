/*
 * File       : UART.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */

#ifndef _UART_H
#define _UART_H

#ifdef __cplusplus
extern "C"
{
#endif
#include "../Include/TypeDef.h"
#include "../macroLib/Queue.h"
//#include "ISRHandler.h"
#include "stm32f4xx_hal.h"
	
	
/* 中断优先级配置 暂时放在这里 */
#define UART1_PRIPRIO 			(1)
#define UART1_SUBPRIO 			(0)

#define UART2_PRIPRIO 			(3)
#define UART2_SUBPRIO 			(0)

#define UART3_PRIPRIO 			(1)
#define UART3_SUBPRIO 			(0)

#define UART4_PRIPRIO 			(2)
#define UART4_SUBPRIO 			(0)

#define UART5_PRIPRIO 			(0)
#define UART5_SUBPRIO 			(0)

typedef void (*tpfnUartRxISRFunc)(tdu1 byte);
	
typedef enum
{
	UartBaudRate_9600 = 1,
	UartBaudRate_38400 = 2,
	UartBaudRate_57600 = 3,
	UartBaudRate_115200 = 4,
	UartBaudRate_230400 = 5,
	UartBaudRate_460800 = 6,
	UartBaudRate_921600 = 7
}tUartBaudRate;

typedef enum
{
	UartDataBits_8 = 1
}tUartDataBit;

typedef enum
{
	UartCheckBit_None = 1,
	UartCheckBit_Even = 2,
	UartCheckBit_Odd = 3
}tUartCheckBit;

typedef enum
{
	UartStopBits_1 = 1,
	UartStopBits_2 = 2
}tUartStopBits;

__packed typedef struct
{
	tUartBaudRate BaudRate;
	tUartDataBit DataBits;
	tUartCheckBit CheckBit;
	tUartStopBits StopBits;
}tUartHwPara;

typedef enum
{
	UartRxProtocol_0 = 0,		// 未使用
	UartRx_FcsGimbal = 1,
	UartRx_Gimbal = 2,
	UartRx_GimbalEx = 3,
	UartRxProtocol_4 = 4,
	UartRxProtocol_5 = 5
}tUartRxProtocol;

typedef enum
{
	UartTxProtocol_0 = 0,		// 未使用
	UartTx_FcsGimbal = 1,
	UartTx_Gimbal = 2,
	UartTx_GimbalEx = 3,
	UartTxProtocol_4 = 4,
	UartTxProtocol_5 = 5
}tUartTxProtocol;


__packed typedef struct
{
	tUartHwPara UartHwPara;
	tUartRxProtocol UartRxProtocol;
	tUartTxProtocol UartTxProtocol;
	tdu1 Reserved[8]; 	
}tUartPara;

extern void Uart_Init(void);
extern void Uart_SetRxISRFunc(tdu1 ch,tpfnUartRxISRFunc fp);
extern tpFnWrite Uart_GetWriteHandler(tdu1 ch);

extern tUartPara *Uart_GetUart1Para(void);
extern tUartPara *Uart_GetUart2Para(void);
extern tUartPara *Uart_GetUart3Para(void);
extern tUartPara *Uart_GetUart4Para(void);
extern tUartPara *Uart_GetUart5Para(void);
#ifdef __cplusplus
}
#endif
#endif /* __UART_H */

