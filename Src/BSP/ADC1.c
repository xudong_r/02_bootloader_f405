/*
 * File      : ADC1.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#include "ADC1.h"
#include "stm32f4xx_hal.h"
#include "DMA.h"
#include <string.h>
#include "BSPFile.h"

#ifdef ADC1_ENABLE


//ADC1对应DMA为DMA1_CH1,STM32硬件决定,禁止改动
#define ADC1_DMA			1
#define ADC1_DMA_CHANNEL	1

#ifdef ADC1_CHANNEL_TEMP_ENABLE
#define ADC1_CHANNEL_TEMP_NUM	(1)
#else
#define ADC1_CHANNEL_TEMP_NUM	(0)
#endif

#ifdef ADC1_CHANNEL_VREF_ENABLE
#define ADC1_CHANNEL_VREF_NUM	(1)
#else
#define ADC1_CHANNEL_VREF_NUM	(0)
#endif

#define ADC1_CHANNEL_NUM	(ADC1_CHANNEL_SAMPLE_NUM + ADC1_CHANNEL_TEMP_NUM + ADC1_CHANNEL_VREF_NUM)

#define ADC1_BUFFER_LENGTH	(ADC1_CHANNEL_NUM*ADC1_CHANNEL_FILTER)
static ADC_HandleTypeDef ADC1_Handle;
static tdu4 ADC1_SampleBuffer[ADC1_BUFFER_LENGTH] = {0};
static tdf4 ADC1_ConvValue[ADC1_CHANNEL_NUM] = {0.0f};
static void ADC1_RCC_Init()
{
	__HAL_RCC_ADC1_CLK_ENABLE();
}
static void ADC1_GPIO_Init()
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = ADC1_CHANNEL1_PIN; 
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ADC1_CHANNEL1_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = ADC1_CHANNEL2_PIN;
	HAL_GPIO_Init(ADC1_CHANNEL2_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = ADC1_CHANNEL3_PIN;
	HAL_GPIO_Init(ADC1_CHANNEL3_PORT, &GPIO_InitStructure);
/**********************************************************************************
	USER CODE 1 BEGIN:补充剩余采集通道GPIO配置代码
***********************************************************************************/
//	GPIO_InitStructure.Pin = ADC1_CH3_PIN;
//	HAL_GPIO_Init(ADC1_CH3_PORT, &GPIO_InitStructure);
/**********************************************************************************
	USER CODE 1 END
***********************************************************************************/
}

static void ADC1_ADC_Init()
{
	ADC1_Handle.Instance = ADC1;
	
	HAL_ADC_DeInit(&ADC1_Handle);
	ADC1_Handle.Init.DataAlign = ADC_DATAALIGN_RIGHT;

//	ADC1_Handle.Init.ScanConvMode = ADC_SCAN_ENABLE;    //MZW

	ADC1_Handle.Init.ContinuousConvMode = ENABLE;

	ADC1_Handle.Init.NbrOfConversion = ADC1_CHANNEL_NUM;
	
	ADC1_Handle.Init.ExternalTrigConv = ADC_SOFTWARE_START;

	HAL_ADC_Init(&ADC1_Handle);
}


static void ADC1_Channel_Init()
{
	static tdu1 rank = 1;
	ADC_ChannelConfTypeDef  AdcChannelStructure;

	AdcChannelStructure.Channel = ADC1_CHANNEL1;
	AdcChannelStructure.Rank = (tdu4)(rank++);
	AdcChannelStructure.SamplingTime = ADC_SAMPLETIME_480CYCLES;

	HAL_ADC_ConfigChannel(&ADC1_Handle, &AdcChannelStructure);

	AdcChannelStructure.Channel = ADC1_CHANNEL2;
	AdcChannelStructure.Rank = (tdu4)(rank++);
	HAL_ADC_ConfigChannel(&ADC1_Handle, &AdcChannelStructure);

	AdcChannelStructure.Channel = ADC1_CHANNEL3;
	AdcChannelStructure.Rank = (tdu4)(rank++);
	HAL_ADC_ConfigChannel(&ADC1_Handle, &AdcChannelStructure);
/**********************************************************************************
	USER CODE 2 BEGIN:补充剩余ADC采集通道配置代码
***********************************************************************************/
//	AdcChannelStructure.Channel = ADC1_CH3_CHANNEL;
//	AdcChannelStructure.Rank = (tdu4)(rank++);
//	HAL_ADC_ConfigChannel(&ADC1_Handle, &AdcChannelStructure);
/**********************************************************************************
	USER CODE 2 END
***********************************************************************************/	
#ifdef ADC1_CHANNEL_TEMP_ENABLE
	//温度采集通道
	AdcChannelStructure.Channel = ADC_CHANNEL_TEMPSENSOR;
	AdcChannelStructure.Rank = (tdu4)(rank++);
	HAL_ADC_ConfigChannel(&ADC1_Handle, &AdcChannelStructure);
#endif
	
#ifdef ADC1_CHANNEL_VREF_ENABLE
	//电压参考通道
	AdcChannelStructure.Channel = ADC_CHANNEL_VREFINT;
	AdcChannelStructure.Rank = (tdu4)(rank++);
	HAL_ADC_ConfigChannel(&ADC1_Handle, &AdcChannelStructure);
#endif

}

static void ADC1_DMA_Init()
{
	DMA_Init(ADC1_DMA,ADC1_DMA_CHANNEL);

	//ADC DMA 连接
	__HAL_LINKDMA(&ADC1_Handle, DMA_Handle, *(DMA_GetChannelHandle(ADC1_DMA,ADC1_DMA_CHANNEL)));

}

void ADC1_HwInit(void)
{
	ADC1_RCC_Init();
	ADC1_GPIO_Init();
	ADC1_ADC_Init();
	ADC1_Channel_Init();
	ADC1_DMA_Init();
	memset((void*)&ADC1_SampleBuffer[0],0,sizeof(ADC1_SampleBuffer));
	memset((void *)&ADC1_ConvValue[0], 0, sizeof(ADC1_ConvValue));
}

void ADC1_Start(void)
{
	HAL_ADC_Start_DMA(&ADC1_Handle,ADC1_SampleBuffer,ADC1_BUFFER_LENGTH);
}

static tdf4 ADC1_GetChannelRatio(tdu1 ch)
{
	tdf4 ratio = 1.0f;
	switch(ch)
	{
	case 1:
		ratio = ADC1_CHANNEL1_RATIO;
		break;
	case 2:
		ratio = ADC1_CHANNEL2_RATIO;
		break;
	case 3:
		ratio = ADC1_CHANNEL3_RATIO;
		break;
/**********************************************************************************
	USER CODE 3 BEGIN:补充其余ADC采集通道比例放大代码
***********************************************************************************/
//	case 3:
//		return ADC1_CHANNEL3_RATIO;
//		break;
/**********************************************************************************
	USER CODE 3 END
***********************************************************************************/		
	default:
		ratio = 1.0f;
		break;
	}
	return ratio;

}

static void ADC1_ConvAllToVolt(void)
{
	 //将采集值转换为电压值
	tdu1 i,j;
	tdu4 sum = 0;

	//采样数据清0
	for(i = 0;i<ADC1_CHANNEL_NUM;i++)
	{
		sum = 0;
		for(j = 0;j<ADC1_CHANNEL_FILTER;j++)
		{
			sum += ADC1_SampleBuffer[i + j * ADC1_CHANNEL_NUM];
		}
		ADC1_ConvValue[i] = sum*1.0f/ADC1_CHANNEL_FILTER/4096*3.3f;
	}
	//若使能参考电压通道,使用参考电压对采样值进行校准
#ifdef ADC1_CHANNEL_VREF_ENABLE
	for(i = 0;i<ADC1_CHANNEL_NUM - 1;i++)
	{
		ADC1_ConvValue[i] = ADC1_ConvValue[i] / ADC1_ConvValue[ADC1_CHANNEL_NUM - 1]*1.2f;//参考电压通道,2.5V
	}
#endif
	
	//对采样值通道采样值进行比例放大
	ADC1_ConvValue[0] *= ADC1_CHANNEL1_RATIO;
	ADC1_ConvValue[1] *= ADC1_CHANNEL2_RATIO;
	ADC1_ConvValue[2] *= ADC1_CHANNEL3_RATIO;


}

static void ADC1_ConvToVolt(tdu1 ind)
{
	 //将采集值转换为电压值
	tdu1 i;
	tdu4 sum = 0;

	//采样ch通道
	sum = 0;
	for(i = 0;i<ADC1_CHANNEL_FILTER;i++)
	{
		sum += ADC1_SampleBuffer[ind + i * ADC1_CHANNEL_NUM];
	}
	ADC1_ConvValue[ind] = sum*1.0f/ADC1_CHANNEL_FILTER/4096*3.3f;

	//若使能参考电压通道,使用参考电压对采样值进行校准
#ifdef ADC1_CHANNEL_VREF_ENABLE
	//采样参考电压通道
	sum = 0;
	for(i = 0;i<ADC1_CHANNEL_FILTER;i++)
	{
		sum += ADC1_SampleBuffer[ADC1_CHANNEL_NUM - 1 + i * ADC1_CHANNEL_NUM];
	}
	ADC1_ConvValue[ADC1_CHANNEL_NUM - 1] = sum*1.0f/ADC1_CHANNEL_FILTER/4096*3.3f;
	//校准采样通道
	ADC1_ConvValue[ind] = ADC1_ConvValue[ind] / ADC1_ConvValue[ADC1_CHANNEL_NUM - 1]*1.2f;//参考电压通道,1.2V
#endif
	
	//对采样值通道采样值进行比例放大
	ADC1_ConvValue[ind] *= ADC1_GetChannelRatio(ind);


}


void ADC1_GetAllVolt(tdf4* pVolt,tdu1 num)
{
	ADC1_ConvAllToVolt();
	if((pVolt != NULL)&&(num <= ADC1_CHANNEL_NUM))
	{
		memcpy((void*)pVolt,(void*)&ADC1_ConvValue[0],num*sizeof(tdf4));
	}
}

tdf4 ADC1_GetVolt(tdu1 ch)
{
	tdu1 channel = ch - 1;
	ADC1_ConvToVolt(channel);
	return ADC1_ConvValue[channel];
}
#endif //end of (#ifdef ADC1_ADC1_ENABLE)


