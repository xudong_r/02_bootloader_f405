/*
 * File      : I2C.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

// 接口不完整

#include "I2C.h"
#include "BSPFile.h"
#include "stm32f4xx_hal.h"

#ifdef I2C1_ENABLE
#include "I2C1.h"
#endif /* I2C1_ENABLE */

#ifdef I2C2_ENABLE
#include "I2C2.h"
#endif /* I2C2_ENABLE */

void I2C_HwInit(tdu1 ch,tdu4 clockSpeed)
{
#ifdef I2C1_ENABLE
	if (ch == 1)
	{
		I2C1_HwInit(clockSpeed);
	}
#endif /* I2C1_ENABLE */

#ifdef I2C2_ENABLE
	if (ch == 2)
	{
		I2C2_HwInit(clockSpeed);
	}
#endif /* I2C2_ENABLE */
}

void I2C_WriteByte(tdu2 devAddr,tdu1 ch,tdu1* pByte,tdu2 byteNum)
{
#ifdef I2C1_ENABLE
	if (ch == 1)
	{
		I2C1_WriteByte(devAddr,pByte,byteNum);
	}
#endif /* I2C1_ENABLE */

#ifdef I2C2_ENABLE
	if (ch == 2)
	{
		I2C2_WriteByte(devAddr,pByte,byteNum);
	}
#endif /* I2C2_ENABLE */
}

void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c)
{
#ifdef I2C1_ENABLE
	if (hi2c == I2C1_GetHandle())
	{
		I2C1_MspInit();
	}
#endif /* I2C1_ENABLE */

#ifdef I2C2_ENABLE
	if (hi2c == I2C2_GetHandle())
	{
		I2C2_MspInit();
	}
#endif /* I2C2_ENABLE */
}
