/*
 * File       : UART5.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */


#include "Uart5.h"
#include <stdio.h>
#include <string.h>
#include "../MacroLib/MemApply.h"
#include "BSPFile.h"

#ifdef OS_ENABLE
#include "OSPort.h"
#endif /* OS_ENABLE */

#ifdef UART5_ENABLE

static UART_HandleTypeDef Uart5Handle;

// 内部函数声明
static void Uart5_PrepareRecData(void);
static void Uart5_TxQueueInit(tdu2 length);

UART_HandleTypeDef* Uart5_GetHandle(void)
{
	return &Uart5Handle;
}

static void Uart5_Rcc_HwInit(void)
{
	__HAL_RCC_UART5_CLK_ENABLE();
}

static void Uart5_Gpio_HwInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.Pin = UART5_TX_GPIO_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Alternate = GPIO_AF8_UART5;	
	HAL_GPIO_Init(UART5_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = UART5_RX_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Alternate = GPIO_AF8_UART5;
	HAL_GPIO_Init(UART5_RX_GPIO_PORT, &GPIO_InitStructure);
}

static void Uart5_Uart_HwInit(tdu4 baudrate,char parity,tdf4 stopBits)
{
	Uart5Handle.Instance = UART5;

	HAL_UART_DeInit(&Uart5Handle);

	Uart5Handle.Init.BaudRate = baudrate;
	Uart5Handle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	Uart5Handle.Init.OverSampling = UART_OVERSAMPLING_16;
	Uart5Handle.Init.Mode = UART_MODE_TX_RX;

	if (stopBits == 2.0f)
	{
		Uart5Handle.Init.WordLength = UART_WORDLENGTH_9B;
	}
	else
	{
		Uart5Handle.Init.WordLength = UART_WORDLENGTH_8B;
	}

	if(stopBits == 2.0f)
		Uart5Handle.Init.StopBits = UART_STOPBITS_2;
	else//1.0f
		Uart5Handle.Init.StopBits = UART_STOPBITS_1;

	if(parity == 'O' || parity == 'o')
		Uart5Handle.Init.Parity = UART_PARITY_ODD;
	else if(parity == 'E' || parity == 'e')
		Uart5Handle.Init.Parity = UART_PARITY_EVEN;
	else//(parity == 'n' || parity == 'N')
		Uart5Handle.Init.Parity = UART_PARITY_NONE;

	HAL_UART_Init(&Uart5Handle);

}

static void Uart5_Nvic_HwInit(void)
{
	HAL_NVIC_SetPriority(UART5_IRQn, UART5_PRIPRIO,UART5_SUBPRIO);
	HAL_NVIC_EnableIRQ(UART5_IRQn);
}

void Uart5_HwInit(tdu4 baudrate,char parity,tdf4 stopBits,tdu2 bufferLength)
{
	Uart5_Rcc_HwInit();
	Uart5_Gpio_HwInit();
	Uart5_Nvic_HwInit();
	Uart5_Uart_HwInit(baudrate, parity, stopBits);
	__HAL_UART_ENABLE(&Uart5Handle);

	// 初始化发送队列
	Uart5_TxQueueInit(bufferLength);
	// 开启接收功能
	Uart5_PrepareRecData();
}


void Uart5_SetBaudrate(tdu4 baudrate)
{
	if (Uart5Handle.Instance == USART1)
	{
		Uart5Handle.Init.BaudRate = baudrate;
		HAL_UART_Init(&Uart5Handle);
	}
}


tdbl Uart5_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum)
{
	HAL_StatusTypeDef res;

	res = HAL_UART_Transmit_IT(&Uart5Handle,pBytes,byteNum);

	if (res == HAL_OK)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static tdu1 Uart5RxByte;
void UART5_IRQHandler(void)
{
#ifdef OS_ENABLE
	OSPort_Interrupt_Enter();
#endif /* OS_ENABLE */
	HAL_UART_IRQHandler(&Uart5Handle);
	if((__HAL_UART_GET_FLAG(&Uart5Handle, UART_FLAG_ORE) != RESET))
	{
		HAL_UART_Receive_IT(&Uart5Handle, &Uart5RxByte,1);
	}
#ifdef OS_ENABLE
	OSPort_Interrupt_Leave();
#endif /* OS_ENABLE */
}

/****** 以下代码与硬件平台无关 ******/

static tQueue Uart5TxQueue;
static tdu1* pUart5TxBuffer;
void Uart5_TxQueueInit(tdu2 length)
{
	Queue_Init(&Uart5TxQueue, 2*length);
	pUart5TxBuffer = (tdu1 *)MemApply(length);
	memset((void*)pUart5TxBuffer,0,length);
}
// 从队列中取出全部数据
tdu4 Uart5_TxAllDequeue(byte *data)
{

    return (Queue_PopBytes(&Uart5TxQueue, data, Uart5TxQueue.Count)); 

}


tdbl Uart5_WriteBytes(byte *pByte, U16 byteNum)
{
	tdbl res;
    if (byteNum==0) 
	{ 
        // 发送长度为0
        return FALSE;
    }

	//·发送字节
	res = Uart5_WriteBytes_IT(pByte,byteNum);
	if (res == FALSE)
	{
		//·发送失败,将数据放入队列
		Queue_Enqueue(&Uart5TxQueue,pByte,byteNum);

	}
	return TRUE;
}

tpFnWrite Uart5_GetWriteHandler(void)
{
	return Uart5_WriteBytes;
}


void Uart5_TxCpltCallBack(void)
{
    // 从队列中取出全部数据
    tdu4 byteNum;
    byteNum = Uart5_TxAllDequeue(pUart5TxBuffer);
    if (byteNum!=0) {
		// 将数据放送出去
		Uart5_WriteBytes_IT(pUart5TxBuffer, byteNum);
    }
}


static tpfnUartRxISRFunc fpUartRxISRFunc = NULL;

void Uart5_SetRxISRFunc(tpfnUartRxISRFunc fp)
{
	fpUartRxISRFunc = fp;
}

void Uart5_RxCpltCallBack(void)
{
	// 接收数据
	if (fpUartRxISRFunc != NULL)
	{
		fpUartRxISRFunc(Uart5RxByte);
	}
	// 开启中断继续接收
	HAL_UART_Receive_IT(&Uart5Handle, &Uart5RxByte,1);
}
void Uart5_ErrorCpltCallBack(void)
{
	// 开启中断继续接收 
	HAL_UART_Receive_IT(&Uart5Handle, &Uart5RxByte,1);
}

static void Uart5_PrepareRecData(void)
{
	HAL_UART_Receive_IT(&Uart5Handle, &Uart5RxByte,1);
}

#endif //end of (#ifdef UART5_ENABLE)
