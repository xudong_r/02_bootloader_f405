/*
 * File      : LED.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __LED_H
#define __LED_H

#include <stdio.h>

extern void LED_Init(void);

extern void LED_AllOn(void);

extern void LED_AllOff(void);

extern void LED1_On(void);

extern void LED1_Off(void);

extern void LED1_Toggle(void);

extern void LED1_Disable(void);

extern void LED1_Enable(void);

extern void LED2_On(void);

extern void LED2_Off(void);

extern void LED2_Toggle(void);

extern void LED2_Disable(void);

extern void LED2_Enable(void);

extern void LED3_On(void);

extern void LED3_Off(void);

extern void LED3_Toggle(void);

extern void LED3_Disable(void);

extern void LED3_Enable(void);

#endif /* __LED_H */
