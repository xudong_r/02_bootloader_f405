/*
 * File      : CAN2.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#include "CAN2.h"
#include <string.h>
#include "BSPFile.h"

#ifdef CAN2_ENABLE

//#define CAN2_BAUDRATE_666K
#define CAN2_BAUDRATE_1M

static CAN_HandleTypeDef CAN2_Handle;
static CanTxMsgTypeDef TxMessage;
static CanRxMsgTypeDef RxMessage;

static void CAN2_RCC_Init(void)
{
	__HAL_RCC_CAN2_CLK_ENABLE();
}
static void CAN2_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	//TX引脚 复用推挽输出
	GPIO_InitStructure.Pin = CAN2_TX_PIN; 
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL; 
	GPIO_InitStructure.Alternate = GPIO_AF9_CAN2;
	HAL_GPIO_Init(CAN2_TX_PORT, &GPIO_InitStructure);
	
	//RX引脚，上拉输入
	GPIO_InitStructure.Pin = CAN2_RX_PIN; 
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP; 
	GPIO_InitStructure.Pull = GPIO_NOPULL; 
	GPIO_InitStructure.Alternate = GPIO_AF9_CAN2;
	HAL_GPIO_Init(CAN2_RX_PORT, &GPIO_InitStructure);
}

static void CAN2_CAN_Init(void)
{
	CAN_FilterConfTypeDef CAN2_Filter;
	
	CAN2_Handle.Instance = CAN2;
	CAN2_Handle.pTxMsg = &TxMessage;
	CAN2_Handle.pRxMsg = &RxMessage;
#ifdef CAN2_BAUDRATE_666K
	//CAN总线波特率设置为666.66kbps
#ifdef RCC_PLL_CLK_64MHz
	//使用内部晶振,主频64MHz
	CAN1_Handle.Init.Prescaler = 3;
	CAN1_Handle.Init.BS1 = CAN_BS1_12TQ;
	CAN1_Handle.Init.BS2 = CAN_BS2_3TQ;
	CAN1_Handle.Init.SJW = CAN_SJW_2TQ;
#elif (defined(RCC_PLL_CLK_8_168MHz_CRYSTAL)||defined(RCC_PLL_CLK_8_168MHz_OSC))
	//使用外部晶振，主频168MHz
	CAN1_Handle.Init.Prescaler = 7;
	CAN1_Handle.Init.BS1 = CAN_BS1_4TQ;
	CAN1_Handle.Init.BS2 = CAN_BS2_4TQ;
	CAN1_Handle.Init.SJW = CAN_SJW_2TQ;
#else
#error "CAN2 Init Failed"
#endif
#endif

#ifdef CAN2_BAUDRATE_1M
	//CAN总线波特率设置为1000kbps
#ifdef RCC_PLL_CLK_64MHz
	//使用内部晶振,主频64MHz

#elif (defined(RCC_PLL_CLK_8_168MHz_CRYSTAL)||defined(RCC_PLL_CLK_8_168MHz_OSC))
	//使用外部晶振，主频168MHz
	CAN2_Handle.Init.Prescaler = 3;
	CAN2_Handle.Init.BS1 = CAN_BS1_7TQ;
	CAN2_Handle.Init.BS2 = CAN_BS2_6TQ;
	CAN2_Handle.Init.SJW = CAN_SJW_2TQ;
#else
#error "CAN2 Init Failed"
#endif

#endif
	CAN2_Handle.Init.Mode  =   CAN_MODE_NORMAL;
	CAN2_Handle.Init.ABOM  =   ENABLE;//使能自动离线
	CAN2_Handle.Init.AWUM  =   DISABLE;//禁能自动唤醒
	CAN2_Handle.Init.NART  =   DISABLE;//禁能自动重传
	CAN2_Handle.Init.RFLM  =   DISABLE;//新接收报文覆盖原有报文
	CAN2_Handle.Init.TTCM  =   DISABLE;//禁止时间触发通讯
	CAN2_Handle.Init.TXFP  =   DISABLE;//优先级有发送顺序决定
	
	HAL_CAN_Init(&CAN2_Handle);
	
	CAN2_Filter.FilterNumber = 0;
	CAN2_Filter.FilterMode = CAN_FILTERMODE_IDMASK;
	CAN2_Filter.FilterScale = CAN_FILTERSCALE_32BIT;
	CAN2_Filter.FilterIdHigh = 0x0000;
	CAN2_Filter.FilterIdLow = 0x0000;
	CAN2_Filter.FilterMaskIdHigh = 0x0000;
	CAN2_Filter.FilterMaskIdLow = 0x0000;
	CAN2_Filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	CAN2_Filter.FilterActivation = ENABLE;
	CAN2_Filter.BankNumber = 0;
	
	HAL_CAN_ConfigFilter(&CAN2_Handle, &CAN2_Filter);
}

static void CAN2_NVIC_Init(void)
{
	HAL_NVIC_SetPriority(CAN2_RX0_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(CAN2_RX0_IRQn);
	HAL_CAN_Receive_IT(&CAN2_Handle, CAN_FIFO0);
}

void CAN2_HwInit(void)
{
	CAN2_RCC_Init();
	CAN2_GPIO_Init();
	CAN2_CAN_Init();
	CAN2_NVIC_Init();
}

void CAN2_RX0_IRQHandler(void)
{
	HAL_CAN_IRQHandler(&CAN2_Handle);
}

void CAN2_Write(tdu2 ID,tdu1* data,tdu2 length)
{
	CanTxMsgTypeDef CANTxMsg;

	CANTxMsg.StdId  =   ID;
	CANTxMsg.RTR    =   CAN_RTR_DATA;
	CANTxMsg.ExtId  =   0;
	CANTxMsg.IDE    =   CAN_ID_STD;
	CANTxMsg.DLC    =   length;

	memcpy((tdu1 *)CANTxMsg.Data, (tdu1 *)data, length);

	CAN2_Handle.pTxMsg = &CANTxMsg;

	HAL_CAN_Transmit(&CAN2_Handle,10);
}

static tpFnGetIDBytes2 fpCAN2RxISRFunc = NULL;

void CAN2_SetRxISRFunc(tpFnGetIDBytes2 f)
{
	fpCAN2RxISRFunc = f;
}
void CAN2_RxCpltCallBack(CAN_HandleTypeDef *hcan)
{
	//CAN1接收数据指针非空
	if (fpCAN2RxISRFunc != NULL)
	{
		fpCAN2RxISRFunc(hcan->pRxMsg->StdId, hcan->pRxMsg->Data, hcan->pRxMsg->DLC);
	}
	//开启中断继续接收
	if(HAL_BUSY == HAL_CAN_Receive_IT(hcan, CAN_FIFO0))
	{
		__HAL_CAN_ENABLE_IT(hcan, CAN_IT_FOV0 | CAN_IT_FMP0);
	}
}

#endif // end of (#ifedef CAN2_ENABLE)
