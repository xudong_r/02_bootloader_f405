/*
 * File      : CAN1.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#ifndef __CAN1_H
#define __CAN1_H

#include "TypeDef.h"
#include "stm32f4xx_hal.h"
extern void CAN1_HwInit(void);

extern void CAN1_Write(tdu2 ID,tdu1* data,tdu2 length);

extern void CAN1_RxCpltCallBack(CAN_HandleTypeDef *hcan);

extern void CAN1_SetRxISRFunc(tpFnGetIDBytes2 f);

#endif /* __CAN1_H */

