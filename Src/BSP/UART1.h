/*
 * File       : UART1.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */

#ifndef _UART1_H
#define _UART1_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "Uart.h"
#include "../Include/TypeDef.h"


extern void Uart1_HwInit(tdu4 baudRate, char parity, tdf4 stopBits,tdu2 bufferLength);

extern void Uart1_SetBaudrate(tdu4 baudRate);

extern tpFnWrite Uart1_GetWriteHandler(void);

extern void Uart1_SetRxISRFunc(tpfnUartRxISRFunc fp);

extern UART_HandleTypeDef* Uart1_GetHandle(void);

extern void Uart1_TxCpltCallBack(void);

extern void Uart1_RxCpltCallBack(void);

extern void Uart1_ErrorCpltCallBack(void);


/** 临时 **/ 
extern tdbl Uart1_WriteBytes(byte *pByte, U16 byteNum);

extern tdbl Uart1_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum);
/** 临时 **/ 

#ifdef __cplusplus
}
#endif
#endif /* __UART1_H */


