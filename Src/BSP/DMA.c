/*
 * File      : DMA.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#include "DMA.h"
#include "BSPFile.h"

#ifdef DMA1_ENABLE 
#include "DMA1.h"
#endif /* DMA1_ENABLE */

#ifdef DMA2_ENABLE
#include "DMA2.h"
#endif /* DMA2_ENABLE */


DMA_HandleTypeDef* DMA_GetChannelHandle(tdu1 dmaIndex, tdu1 chIndex)
{
#ifdef DMA1_ENABLE
	if (dmaIndex == 1)
	{
		return DMA1_GetChannelHandle(chIndex);
	}
#endif /* DMA1_ENABLE */

#ifdef DMA2_ENABLE
	if (dmaIndex == 2)
	{
		return DMA2_GetChannelHandle(chIndex);
	}
#endif /* DMA2_ENABLE */
	
	return NULL;

}

void DMA_Init(tdu1 dmaIndex,tdu1 chIndex)
{
#ifdef DMA1_ENABLE
	if (dmaIndex == 1)
	{
		DMA1_Init(chIndex);
	}
#endif /* DMA1_ENABLE */

#ifdef DMA2_ENABLE
	if (dmaIndex == 2)
	{
		DMA2_Init(chIndex);
	}
#endif /* DMA2_ENABLE */
}
