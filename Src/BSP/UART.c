/*
 * File       : UART.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */


#include "UART.h"
#include "BSPFile.h"

#ifdef UART1_ENABLE
#include "UART1.h"
#endif

#ifdef UART2_ENABLE
#include "UART2.h"
#endif

#ifdef UART3_ENABLE
#include "UART3.h"
#endif

#ifdef UART4_ENABLE
#include "UART4.h"
#endif

#ifdef UART5_ENABLE
#include "UART5.h"
#endif


#ifdef UART1_ENABLE
tUartPara Uart1Para;
#endif

#ifdef UART2_ENABLE
tUartPara Uart2Para;
#endif

#ifdef UART3_ENABLE
tUartPara Uart3Para;
#endif

#ifdef UART4_ENABLE
tUartPara Uart4Para;
#endif

#ifdef UART5_ENABLE
tUartPara Uart5Para;
#endif

static tdbl UART_HwInit(tdu2 ch,tdu4 baudRate, char parity, tdf4 stopBits,tdu2 bufferLength)
{
#ifdef UART1_ENABLE
	if(ch == 1)
	{
		Uart1_HwInit(baudRate, parity, stopBits,bufferLength);	
		return TRUE;
	}
#endif

#ifdef UART2_ENABLE
	if(ch == 2)
	{
		Uart2_HwInit(baudRate, parity, stopBits,bufferLength);	
		return TRUE;
	}
#endif

#ifdef UART3_ENABLE
	if(ch == 3)
	{
		Uart3_HwInit(baudRate, parity, stopBits,bufferLength);
		return TRUE;
	}
#endif

#ifdef UART4_ENABLE
	if(ch == 4)
	{
		Uart4_HwInit(baudRate, parity, stopBits,bufferLength);
		return TRUE;
	}
#endif

#ifdef UART5_ENABLE
	if(ch == 5)
	{
		Uart5_HwInit(baudRate, parity, stopBits,bufferLength);
		return TRUE;
	}
#endif

	return FALSE;	
}

void Uart_SetRxISRFunc(tdu1 ch,tpfnUartRxISRFunc fp)
{
#ifdef UART1_ENABLE
	if (ch == 1)
	{
		Uart1_SetRxISRFunc(fp);
	}
#endif	
#ifdef UART2_ENABLE
	if (ch == 2)
	{
		Uart2_SetRxISRFunc(fp);
	}
#endif	
#ifdef UART3_ENABLE
	if (ch == 3)
	{
		Uart3_SetRxISRFunc(fp);
	}
#endif	
#ifdef UART4_ENABLE
	if (ch == 4)
	{
		Uart4_SetRxISRFunc(fp);
	}
#endif	
#ifdef UART5_ENABLE
	if (ch == 5)
	{
		Uart5_SetRxISRFunc(fp);
	}
#endif
	
}

tpFnWrite Uart_GetWriteHandler(tdu1 ch)
{
#ifdef UART1_ENABLE
	if (ch == 1)
	{
		return Uart1_GetWriteHandler();
	}
#endif

#ifdef UART2_ENABLE
	if (ch == 2)
	{
		return Uart2_GetWriteHandler();
	}
#endif
	
#ifdef UART3_ENABLE
	if (ch == 3)
	{
		return Uart3_GetWriteHandler();
	}
#endif

#ifdef UART4_ENABLE
	if (ch == 4)
	{
		return Uart4_GetWriteHandler();
	}
#endif

#ifdef UART5_ENABLE
	if (ch == 5)
	{
		return Uart5_GetWriteHandler();
	}
#endif
	return NULL;

}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
#ifdef UART1_ENABLE
	if (huart == Uart1_GetHandle()) 
	{
		Uart1_RxCpltCallBack();
	}
#endif

#ifdef UART2_ENABLE
	if (huart == Uart2_GetHandle()) 
	{
		Uart2_RxCpltCallBack();
	}
#endif

#ifdef UART3_ENABLE
	if (huart == Uart3_GetHandle()) 
	{
		Uart3_RxCpltCallBack();
	}
#endif

#ifdef UART4_ENABLE
	if (huart == Uart4_GetHandle()) 
	{
		Uart4_RxCpltCallBack();
	}
#endif

#ifdef UART5_ENABLE
	if (huart == Uart5_GetHandle()) 
	{
		Uart5_RxCpltCallBack();
	}
#endif
}



void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
#ifdef UART1_ENABLE
	if (huart == Uart1_GetHandle()) 
	{
		Uart1_TxCpltCallBack();
	}
#endif

#ifdef UART2_ENABLE
	if (huart == Uart2_GetHandle())
	{
		Uart2_TxCpltCallBack();
	}
#endif

#ifdef UART3_ENABLE
	if (huart == Uart3_GetHandle())
	{
		Uart3_TxCpltCallBack();
	}
#endif

#ifdef UART4_ENABLE
	if (huart == Uart4_GetHandle())
	{
		Uart4_TxCpltCallBack();
	}
#endif

#ifdef UART5_ENABLE
	if (huart == Uart5_GetHandle())
	{
		Uart5_TxCpltCallBack();
	}
#endif
}


void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
#ifdef UART1_ENABLE
	if (huart == Uart1_GetHandle()) 
	{
		Uart1_ErrorCpltCallBack();
	}
#endif

#ifdef UART2_ENABLE
	if (huart == Uart2_GetHandle())
	{
		Uart2_ErrorCpltCallBack();
	}
#endif

#ifdef UART3_ENABLE
	if (huart == Uart3_GetHandle())
	{
		Uart3_ErrorCpltCallBack();
	}
#endif

#ifdef UART4_ENABLE
	if (huart == Uart4_GetHandle())
	{
		Uart4_ErrorCpltCallBack();
	}
#endif

#ifdef UART5_ENABLE
	if (huart == Uart5_GetHandle())
	{
		Uart5_ErrorCpltCallBack();
	}
#endif

}



void Uart_Init(void)
{
	tdu4 baudRate = 115200;
	tds1 checkBit = 'N';
	tdu1 stopBits = 1;
#ifdef UART1_ENABLE
	switch (Uart1Para.UartHwPara.BaudRate)
	{
		case UartBaudRate_9600:
		{
			baudRate = 9600;
			break;
		}
		case UartBaudRate_38400:
		{
			baudRate = 38400;
			break;
		}
		case UartBaudRate_57600:
		{
			baudRate = 57600;
			break;
		}
		case UartBaudRate_115200:
		{
			baudRate = 115200;
			break;
		}
		case UartBaudRate_230400:
		{
			baudRate = 230400;
			break;
		}
		case UartBaudRate_460800:
		{
			baudRate = 460800;
			break;
		}
		case UartBaudRate_921600:
		{
			baudRate = 921600;
			break;
		}
		default:
		{			
			baudRate = 115200;
			break;
		}
	}
	switch(Uart1Para.UartHwPara.CheckBit)
	{
		case UartCheckBit_None:
		{
			checkBit = 'N';
			break;
		}
		case UartCheckBit_Even:
		{
			checkBit = 'E';
			break;
		}
		case UartCheckBit_Odd:
		{
			checkBit = 'O';
			break;
		}
		default:
		{
			checkBit = 'N';
			break;
		}
	}
	switch(Uart1Para.UartHwPara.StopBits)
	{
		case UartStopBits_1:
		{
			stopBits = 1;
			break;
		}
		case UartStopBits_2:
		{
			stopBits = 2;
			break;
		}
		default:
		{			
			stopBits = 1;
			break;
		}
	}
	UART_HwInit(1, baudRate, checkBit, stopBits, 128);
#endif

#ifdef UART2_ENABLE
	switch (Uart2Para.UartHwPara.BaudRate)
	{
		case UartBaudRate_9600:
		{
			baudRate = 9600;
			break;
		}
		case UartBaudRate_38400:
		{
			baudRate = 38400;
			break;
		}
		case UartBaudRate_57600:
		{
			baudRate = 57600;
			break;
		}
		case UartBaudRate_115200:
		{
			baudRate = 115200;
			break;
		}
		case UartBaudRate_230400:
		{
			baudRate = 230400;
			break;
		}
		case UartBaudRate_460800:
		{
			baudRate = 460800;
			break;
		}
		case UartBaudRate_921600:
		{
			baudRate = 921600;
			break;
		}
		default:
		{			
			baudRate = 115200;
			break;
		}
	}
	switch(Uart2Para.UartHwPara.CheckBit)
	{
		case UartCheckBit_None:
		{
			checkBit = 'N';
			break;
		}
		case UartCheckBit_Even:
		{
			checkBit = 'E';
			break;
		}
		case UartCheckBit_Odd:
		{
			checkBit = 'O';
			break;
		}
		default:
		{
			checkBit = 'N';
			break;
		}
	}
	switch(Uart2Para.UartHwPara.StopBits)
	{
		case UartStopBits_1:
		{
			stopBits = 1;
			break;
		}
		case UartStopBits_2:
		{
			stopBits = 2;
			break;
		}
		default:
		{			
			stopBits = 1;
			break;
		}
	}
	UART_HwInit(2, baudRate, checkBit, stopBits, 128);
#endif

#ifdef UART3_ENABLE
	switch (Uart3Para.UartHwPara.BaudRate)
	{
		case UartBaudRate_9600:
		{
			baudRate = 9600;
			break;
		}
		case UartBaudRate_38400:
		{
			baudRate = 38400;
			break;
		}
		case UartBaudRate_57600:
		{
			baudRate = 57600;
			break;
		}
		case UartBaudRate_115200:
		{
			baudRate = 115200;
			break;
		}
		case UartBaudRate_230400:
		{
			baudRate = 230400;
			break;
		}
		case UartBaudRate_460800:
		{
			baudRate = 460800;
			break;
		}
		case UartBaudRate_921600:
		{
			baudRate = 921600;
			break;
		}
		default:
		{			
			baudRate = 115200;
			break;
		}
	}
	switch(Uart3Para.UartHwPara.CheckBit)
	{
		case UartCheckBit_None:
		{
			checkBit = 'N';
			break;
		}
		case UartCheckBit_Even:
		{
			checkBit = 'E';
			break;
		}
		case UartCheckBit_Odd:
		{
			checkBit = 'O';
			break;
		}
		default:
		{
			checkBit = 'N';
			break;
		}
	}
	switch(Uart3Para.UartHwPara.StopBits)
	{
		case UartStopBits_1:
		{
			stopBits = 1;
			break;
		}
		case UartStopBits_2:
		{
			stopBits = 2;
			break;
		}
		default:
		{			
			stopBits = 1;
			break;
		}
	}
	UART_HwInit(3, baudRate, checkBit, stopBits, 128);
#endif

#ifdef UART4_ENABLE
	switch (Uart4Para.UartHwPara.BaudRate)
	{
		case UartBaudRate_9600:
		{
			baudRate = 9600;
			break;
		}
		case UartBaudRate_38400:
		{
			baudRate = 38400;
			break;
		}
		case UartBaudRate_57600:
		{
			baudRate = 57600;
			break;
		}
		case UartBaudRate_115200:
		{
			baudRate = 115200;
			break;
		}
		case UartBaudRate_230400:
		{
			baudRate = 230400;
			break;
		}
		case UartBaudRate_460800:
		{
			baudRate = 460800;
			break;
		}
		case UartBaudRate_921600:
		{
			baudRate = 921600;
			break;
		}
		default:
		{			
			baudRate = 115200;
			break;
		}
	}
	switch(Uart4Para.UartHwPara.CheckBit)
	{
		case UartCheckBit_None:
		{
			checkBit = 'N';
			break;
		}
		case UartCheckBit_Even:
		{
			checkBit = 'E';
			break;
		}
		case UartCheckBit_Odd:
		{
			checkBit = 'O';
			break;
		}
		default:
		{
			checkBit = 'N';
			break;
		}
	}
	switch(Uart4Para.UartHwPara.StopBits)
	{
		case UartStopBits_1:
		{
			stopBits = 1;
			break;
		}
		case UartStopBits_2:
		{
			stopBits = 2;
			break;
		}
		default:
		{			
			stopBits = 1;
			break;
		}
	}
	UART_HwInit(4, baudRate, checkBit, stopBits, 128);
#endif

#ifdef UART5_ENABLE
	switch (Uart5Para.UartHwPara.BaudRate)
	{
		case UartBaudRate_9600:
		{
			baudRate = 9600;
			break;
		}
		case UartBaudRate_38400:
		{
			baudRate = 38400;
			break;
		}
		case UartBaudRate_57600:
		{
			baudRate = 57600;
			break;
		}
		case UartBaudRate_115200:
		{
			baudRate = 115200;
			break;
		}
		case UartBaudRate_230400:
		{
			baudRate = 230400;
			break;
		}
		case UartBaudRate_460800:
		{
			baudRate = 460800;
			break;
		}
		case UartBaudRate_921600:
		{
			baudRate = 921600;
			break;
		}
		default:
		{			
			baudRate = 115200;
			break;
		}
	}
	switch(Uart5Para.UartHwPara.CheckBit)
	{
		case UartCheckBit_None:
		{
			checkBit = 'N';
			break;
		}
		case UartCheckBit_Even:
		{
			checkBit = 'E';
			break;
		}
		case UartCheckBit_Odd:
		{
			checkBit = 'O';
			break;
		}
		default:
		{
			checkBit = 'N';
			break;
		}
	}
	switch(Uart5Para.UartHwPara.StopBits)
	{
		case UartStopBits_1:
		{
			stopBits = 1;
			break;
		}
		case UartStopBits_2:
		{
			stopBits = 2;
			break;
		}
		default:
		{			
			stopBits = 1;
			break;
		}
	}
	UART_HwInit(5, baudRate, checkBit, stopBits, 128);
#endif
}


#ifdef UART1_ENABLE
tUartPara *Uart_GetUart1Para(void)
{
	return &Uart1Para;
}
#endif

#ifdef UART2_ENABLE
tUartPara *Uart_GetUart2Para(void)
{
	return &Uart2Para;
}
#endif

#ifdef UART3_ENABLE
tUartPara *Uart_GetUart3Para(void)
{
	return &Uart3Para;
}
#endif

#ifdef UART4_ENABLE
tUartPara *Uart_GetUart4Para(void)
{
	return &Uart4Para;
}
#endif

#ifdef UART5_ENABLE
tUartPara *Uart_GetUart5Para(void)
{
	return &Uart5Para;
}
#endif





