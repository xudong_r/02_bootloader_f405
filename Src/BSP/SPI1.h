/*
 * File      : SPI1.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */


#ifndef __SPI1_H
#define __SPI1_H

#include "TypeDef.h"



extern void SPI1_CS(u16 ch);

extern void SPI1_HardwareInit(u16 Cpol, u16 Cpha, u16 baudRatePrescaler);

extern void SPI1_SetWave(u16 Cpol, u16 Cpha, u16 baudRatePrescaler);

extern u16 SPI1_TxByte(u16 data);

extern u16 SPI1_RxByte(void);



#endif /* __SPI1_H */
