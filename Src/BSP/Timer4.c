/*
 * File      : Timer4.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */


#include "Timer4.h"
#include "stm32f4xx_hal.h"
#include "BSPFile.h"
#include "ISRHandler.h"

#ifdef TIM4_ENABLE
static TIM_HandleTypeDef Tim4Handle;
static tpfnTimerElapsedCallback fpOnTimer4Elapsed = NULL;
void Timer4_Base_HwInit(tdu4 frequency,tdu4 period,tdu2 dutyCycle)
{
	Tim4Handle.Instance = TIM4;

	HAL_TIM_Base_DeInit(&Tim4Handle);

	Tim4Handle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	Tim4Handle.Init.CounterMode = TIM_COUNTERMODE_UP;
	Tim4Handle.Init.Period = dutyCycle - 1;
	Tim4Handle.Init.RepetitionCounter = 0;
	if (frequency != 0)
	{
		Tim4Handle.Init.Prescaler = HAL_RCC_GetPCLK2Freq() / frequency / dutyCycle - 1;
	}
	else if (period != 0)
	{
		Tim4Handle.Init.Prescaler = HAL_RCC_GetPCLK2Freq() / dutyCycle * period - 1;
	}
	else
	{
		return;
	}

	HAL_TIM_Base_Init(&Tim4Handle);

#ifdef TIM4_IT_ENABLE
	HAL_NVIC_SetPriority(TIM4_IRQn,TIM4_PRIPRIO,TIM4_SUBPRIO);
	HAL_NVIC_EnableIRQ(TIM4_IRQn);
#endif

#ifdef TIM4_DMA_ENABLE

#endif
}


void Timer4_Start()
{
	HAL_TIM_Base_Start(&Tim4Handle);
}

void Timer4_Start_IT()
{
	HAL_TIM_Base_Start_IT(&Tim4Handle);
}

void TIM4_IRQHandler()
{
	HAL_TIM_IRQHandler(&Tim4Handle);
}

TIM_HandleTypeDef* Timer4_GetHandle(void)
{
	return &Tim4Handle;
}

void Timer4_PeriodElapsedCallback()
{
	//Do Nothing
	if (fpOnTimer4Elapsed != NULL)
	{
		fpOnTimer4Elapsed();
	}
}

void Timer4_SetElapsedCallback(tpfnTimerElapsedCallback fp)
{
	fpOnTimer4Elapsed = fp;
}

#endif //end of (#ifdef TIM4_ENABLE)

