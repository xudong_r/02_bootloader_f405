/*
 * File       : UART3.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */

#ifndef _UART3_H
#define _UART3_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "Uart.h"
#include "../Include/TypeDef.h"


extern void Uart3_HwInit(tdu4 baudRate, char parity, tdf4 stopBits,tdu2 bufferLength);

extern void Uart3_SetBaudrate(tdu4 baudRate);

extern tpFnWrite Uart3_GetWriteHandler(void);

extern void Uart3_SetRxISRFunc(tpfnUartRxISRFunc fp);

extern UART_HandleTypeDef* Uart3_GetHandle(void);

extern void Uart3_TxCpltCallBack(void);

extern void Uart3_RxCpltCallBack(void);

extern void Uart3_ErrorCpltCallBack(void);


/** 临时 **/ 
extern tdbl Uart3_WriteBytes(byte *pByte, U16 byteNum);

extern tdbl Uart3_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum);
/** 临时 **/ 

#ifdef __cplusplus
}
#endif
#endif /* __UART3_H */


