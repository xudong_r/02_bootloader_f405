/*
 * File      : ISRHandler.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __ISRHANDLER_H
#define __ISRHANDLER_H


#include "TypeDef.h"
#include "stm32f4xx_hal.h"
/* Preemption Priority Group -------------------------------------------------
#define NVIC_PriorityGroup_0          ((u32)0x700)  0 bits for pre-emption priority	0
                                                      4 bits for subpriority 		0~15
                                                      
#define NVIC_PriorityGroup_1          ((u32)0x600)  1 bits for pre-emption priority 0~1
                                                      3 bits for subpriority 		0~7
                                                      
#define NVIC_PriorityGroup_2          ((u32)0x500)  2 bits for pre-emption priority	0~3
                                                      2 bits for subpriority 		0~3
                                                      
#define NVIC_PriorityGroup_3          ((u32)0x400)  3 bits for pre-emption priority	0~7
                                                      1 bits for subpriority 		0~1
                                                      
#define NVIC_PriorityGroup_4          ((u32)0x300)  4 bits for pre-emption priority	0~15
                                                      0 bits for subpriority 		0
-------------------------------------------------*/                                                      
#define SYS_PriorityGroup					NVIC_PRIORITYGROUP_4

#define TIM4_PRIPRIO			(0)
#define TIM4_SUBPRIO			(0)

#define UART1_PRIPRIO 			(1)
#define UART1_SUBPRIO 			(0)

#define UART2_PRIPRIO 3
#define UART2_SUBPRIO 0

#define UART3_PRIPRIO 1
#define UART3_SUBPRIO 0

#define UART4_PRIPRIO 2
#define UART4_SUBPRIO 0

#define UART5_PRIPRIO 0
#define UART5_SUBPRIO 0

#define DMA1_CHANNEL1_PRIPRIO 5
#define DMA1_CHANNEL1_SUBPRIO 0

#define EXTI9_5_PreemptionPriority			6
#define EXTI9_5_SubPriority					0

//#define CAN1_PreemptionPriority				2
//#define CAN1_SubPriority					0


/**
 * 中断回调函数定义
 */
typedef void (*tpfnUartRxISRFunc)(tdu1 byte);

typedef void (*UARTRxISRFunc)(byte data);


typedef tdu4 (*UARTTcISRFunc)(void);


typedef void (*TimerISRFunc)(U32 cnt);
typedef void (*SystemTickISRFunc)(U32 cnt);
typedef void (*EXTIISRFunc)(U32 cnt);
typedef void (*CANRxISRFunc)(tdu4 ID, tdu1 *data);
typedef struct
{
	UARTRxISRFunc UART1Rx;
	UARTRxISRFunc UART2Rx;
	UARTRxISRFunc UART3Rx;
	UARTRxISRFunc UART4Rx;
	UARTRxISRFunc UART5Rx;
	UARTRxISRFunc UART6Rx;

    UARTTcISRFunc UART1Tc;
    UARTTcISRFunc UART2Tc;
    UARTTcISRFunc UART3Tc;
    UARTTcISRFunc UART4Tc;
    UARTTcISRFunc UART5Tc;
    UARTTcISRFunc UART6Tc;


	TimerISRFunc Timer1;
	TimerISRFunc Timer2;
	TimerISRFunc Timer3;
	TimerISRFunc Timer4;
	TimerISRFunc Timer5;
	
	SystemTickISRFunc SystemTick;

	EXTIISRFunc EXTI0;
	EXTIISRFunc EXTI1;
	EXTIISRFunc EXTI10;

	CANRxISRFunc CAN1Rx;
	CANRxISRFunc CAN2Rx;
}ISRHandlerStruct;

extern ISRHandlerStruct ISRHandler;

extern void ISRHandler_Init(void);

#endif /* __ISRHANDLER_H */
