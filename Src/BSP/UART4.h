/*
 * File       : UART4.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */

#ifndef _UART4_H
#define _UART4_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "Uart.h"
#include "../Include/TypeDef.h"


extern void Uart4_HwInit(tdu4 baudRate, char parity, tdf4 stopBits,tdu2 bufferLength);

extern void Uart4_SetBaudrate(tdu4 baudRate);

extern tpFnWrite Uart4_GetWriteHandler(void);

extern void Uart4_SetRxISRFunc(tpfnUartRxISRFunc fp);

extern UART_HandleTypeDef* Uart4_GetHandle(void);

extern void Uart4_TxCpltCallBack(void);

extern void Uart4_RxCpltCallBack(void);

extern void Uart4_ErrorCpltCallBack(void);


/** 临时 **/ 
extern tdbl Uart4_WriteBytes(byte *pByte, U16 byteNum);

extern tdbl Uart4_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum);
/** 临时 **/ 

#ifdef __cplusplus
}
#endif
#endif /* __UART4_H */


