/*
 * File      : SysTick.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __SYSTICK_H
#define __SYSTICK_H

#include "TypeDef.h"

extern void SysTick_Init(tdu4 sysFre);

extern void SysTick_Start(void);

extern void SysTick_Stop(void);

#endif /* __SYSTICK_H */

