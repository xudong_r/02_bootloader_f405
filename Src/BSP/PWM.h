/*
 * File      : PWM.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __PWM_H
#define __PWM_H


#define PWM_PERIOD_US		0
#define PWM_PERIOD_MS		1

//mode用于选择周期，可设置为PWM_PERIOD_US，或PWM_PERIOD_MS
//PWM_PERIOD_US:
//PWM_PERIOD_MS:
extern tdbl PWM_Init(tdu1 groupID, tdu1 mode, tdf4 period);

//ch 1~8
//width 单位根据初始化时选择为us或ms
//
//
extern tdbl PWM_SetWidth(tdu1 ch, tdf4 width);

extern void PWM_Close(void);

extern tdbl PWM_CloseGroup(tdu1 groupID);

#endif /* __PWM_H */
