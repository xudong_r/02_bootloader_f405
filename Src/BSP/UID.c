/*
 * File      : UID.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "UID.h"
#include <string.h>
static tdu1 ChipUid[12] = {0};
static tdu1 ChipCap[2] = {0};

void UID_Init(void)
{
	memcpy((void*)(&ChipUid[0]),(void*)(tdu4*)(UID_BASEADDR),4);
    memcpy((void*)(&ChipUid[4]),(void*)((tdu4*)(UID_BASEADDR)+1),4);
    memcpy((void*)(&ChipUid[8]),(void*)((tdu4*)(UID_BASEADDR)+2),4);
	memcpy((void *)(&ChipCap[0]), (void *)((tdu2 *)(UID_FLASHADDR)),2);
}
tdu1* UID_GetUID(void)
{
	return &ChipUid[0];
}

tdu1* UID_GetFlashCap(void)
{
	return &ChipCap[0];
}
