/*
 * File       : UART5.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */

#ifndef _UART5_H
#define _UART5_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "Uart.h"
#include "../Include/TypeDef.h"


extern void Uart5_HwInit(tdu4 baudRate, char parity, tdf4 stopBits,tdu2 bufferLength);

extern void Uart5_SetBaudrate(tdu4 baudRate);

extern tpFnWrite Uart5_GetWriteHandler(void);

extern void Uart5_SetRxISRFunc(tpfnUartRxISRFunc fp);

extern UART_HandleTypeDef* Uart5_GetHandle(void);

extern void Uart5_TxCpltCallBack(void);

extern void Uart5_RxCpltCallBack(void);

extern void Uart5_ErrorCpltCallBack(void);


/** 临时 **/ 
extern tdbl Uart5_WriteBytes(byte *pByte, U16 byteNum);

extern tdbl Uart5_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum);
/** 临时 **/ 

#ifdef __cplusplus
}
#endif
#endif /* __UART5_H */


