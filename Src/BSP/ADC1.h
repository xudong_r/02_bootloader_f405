/*
 * File      : ADC1.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#ifndef __ADC1_H
#define __ADC1_H

#include "../Include/TypeDef.h"

extern void ADC1_HwInit(void);

extern void ADC1_Start(void);

extern void ADC1_GetAllVolt(tdf4* volt,tdu1 chNum);

extern tdf4 ADC1_GetVolt(tdu1 ch);


#endif /* __ADC1_H */
