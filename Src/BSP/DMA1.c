/*
 * File      : DMA1.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#include "DMA1.h"
#include "ISRHandler.h"

#include "BSPFile.h"

#ifdef DMA1_ENABLE

#ifdef DMA1_CHANNEL1_ENABLE

static DMA_HandleTypeDef DMA1_Channel1_Handle;

void DMA1_Channel1_Init(void)
{


	__HAL_RCC_DMA1_CLK_ENABLE();

	DMA1_Channel1_Handle.Instance = DMA1_Channel1;

	HAL_DMA_DeInit(&DMA1_Channel1_Handle);
	DMA1_Channel1_Handle.Init.Direction = DMA_PERIPH_TO_MEMORY;
	DMA1_Channel1_Handle.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
	DMA1_Channel1_Handle.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	DMA1_Channel1_Handle.Init.MemInc = DMA_MINC_ENABLE;
	DMA1_Channel1_Handle.Init.PeriphInc = DMA_PINC_DISABLE;
	DMA1_Channel1_Handle.Init.Mode = DMA_CIRCULAR;
	DMA1_Channel1_Handle.Init.Priority = DMA_PRIORITY_VERY_HIGH;

	HAL_DMA_Init(&DMA1_Channel1_Handle);

	// 开启中断
	HAL_NVIC_SetPriority(DMA1_Channel1_IRQn,DMA1_CHANNEL1_PRIPRIO,DMA1_CHANNEL1_SUBPRIO);
	HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}


void DMA1_Channel1_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&DMA1_Channel1_Handle);
}

#endif	//end of (#ifdef DMA1_CHANNEL1_ENABLE)





#ifdef DMA1_CHANNEL2_ENABLE
static DMA_HandleTypeDef DMA1_Channel2_Handle;

void DMA1_Channel1_Init()
{
	HAL_DMA_DeInit(&DMA1_Channel1_Handle);

	DMA1_Channel1_Handle.Instance = DMA1_Channel1;

	DMA1_Channel1_Handle.Init.Direction = DMA_PERIPH_TO_MEMORY;
	DMA1_Channel1_Handle.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
	DMA1_Channel1_Handle.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	DMA1_Channel1_Handle.Init.MemInc = DMA_MINC_ENABLE;
	DMA1_Channel1_Handle.Init.PeriphInc = DMA_PINC_DISABLE;
	DMA1_Channel1_Handle.Init.Mode = DMA_CIRCULAR;
	DMA1_Channel1_Handle.Init.Priority = DMA_PRIORITY_VERY_HIGH;

	HAL_DMA_Init(&ADC1_DMA_Handle);

	// 开启中断
	HAL_NVIC_SetPriority(DMA1_Channel1_IRQn,DMA1_CHANNEL1_PRIPRIO,DMA1_CHANNEL1_SUBPRIO);
	HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}


void DMA1_Channel2_IRQHandler()
{
	HAL_DMA_IRQHandler(&DMA1_Channel2_Handle);
}

#endif //end of (#ifdef DMA1_CHANNEL2_ENABLE)

void DMA1_Init(tdu1 chIndex)
{
#ifdef DMA1_CHANNEL1_ENABLE
	if (chIndex == 1)
	{
		DMA1_Channel1_Init();
	}
#endif /* DMA1_CHANNEL1_ENABLE */

#ifdef DMA1_CHANNEL2_ENABLE
	if (chIndex == 2)
	{
		DMA1_Channel2_Init();
	}
#endif /* DMA1_CHANNEL2_ENABLE */

#ifdef DMA1_CHANNEL3_ENABLE
	if (chIndex == 3)
	{
		DMA1_Channel3_Init();
	}
#endif /* DMA1_CHANNEL3_ENABLE */

#ifdef DMA1_CHANNEL4_ENABLE
	if (chIndex == 4)
	{
		DMA1_Channel4_Init();
	}
#endif /* DMA1_CHANNEL4_ENABLE */

#ifdef DMA1_CHANNEL5_ENABLE
	if (chIndex == 5)
	{
		DMA1_Channel5_Init();
	}
#endif /* DMA1_CHANNEL5_ENABLE */

#ifdef DMA1_CHANNEL6_ENABLE
	if (chIndex == 6)
	{
		DMA1_Channel6_Init();
	}
#endif /* DMA1_CHANNEL6_ENABLE */

#ifdef DMA1_CHANNEL7_ENABLE
	if (chIndex == 7)
	{
		DMA1_Channel7_Init();
	}
#endif /* DMA1_CHANNEL7_ENABLE */
}

DMA_HandleTypeDef* DMA1_GetChannelHandle(tdu1 chIndex)
{
#ifdef DMA1_CHANNEL1_ENABLE
	if (chIndex == 1)
	{
		return &DMA1_Channel1_Handle;
	}
#endif /* DMA1_CHANNEL1_ENABLE */

#ifdef DMA1_CHANNEL2_ENABLE
	if (chIndex == 2)
	{
		return &DMA1_Channel2_Handle;
	}
#endif /* DMA1_CHANNEL2_ENABLE */
	
#ifdef DMA1_CHANNEL3_ENABLE
	if (chIndex == 3) 
	{
		return &DMA1_Channel3_Handle;
	}
#endif /* DMA1_CHANNEL3_ENABLE */
	
#ifdef DMA1_CHANNEL4_ENABLE
	if (chIndex == 4)
	{
		return &DMA1_Channel4_Handle;
	}
#endif /* DMA1_CHANNEL4_ENABLE */
	
#ifdef DMA1_CHANNEL5_ENABLE
	if (chIndex == 5) 
	{
		return &DMA1_Channel5_Handle;
	}
#endif /* DMA1_CHANNEL5_ENABLE */

#ifdef DMA1_CHANNEL6_ENABLE
	if (chIndex == 6)
	{
		return &DMA1_Channel6_Handle;
	}
#endif /* DMA1_CHANNEL6_ENABLE */
	
#ifdef DMA1_CHANNEL7_ENABLE
	if (chIndex == 7) 
	{
		return &DMA1_Channel7_Handle;
	}
#endif /* DMA1_CHANNEL7_ENABLE */
	return NULL;
}

#endif //end of (#ifdef DMA1_ENABLE)

