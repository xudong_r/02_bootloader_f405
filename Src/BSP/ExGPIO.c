/*
 * File      : ExGPIO.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "ExGpio.h"
#include "BSPFile.h"
#include "stm32f4xx_hal.h"



#ifdef EXGPIO1_ENABLE
static void ExGpio1_Set(void)
{
	HAL_GPIO_WritePin(EXGPIO1_PORT, EXGPIO1_PIN, GPIO_PIN_SET);
}
#endif /* EXGPIO1_ENABLE*/

#ifdef EXGPIO2_ENABLE
static void ExGpio2_Set(void)
{
	HAL_GPIO_WritePin(EXGPIO2_PORT, EXGPIO2_PIN, GPIO_PIN_SET);
}
#endif /* EXGPIO2_ENABLE*/

#ifdef EXGPIO3_ENABLE
static void ExGpio3_Set(void)
{
	HAL_GPIO_WritePin(EXGPIO3_PORT, EXGPIO3_PIN, GPIO_PIN_SET);
}
#endif /* EXGPIO3_ENABLE*/

#ifdef EXGPIO1_ENABLE
static void ExGpio1_Reset(void)
{
	HAL_GPIO_WritePin(EXGPIO1_PORT, EXGPIO1_PIN, GPIO_PIN_RESET);
}
#endif /* EXGPIO1_ENABLE*/

#ifdef EXGPIO2_ENABLE
static void ExGpio2_Reset(void)
{
	HAL_GPIO_WritePin(EXGPIO2_PORT, EXGPIO2_PIN, GPIO_PIN_RESET);
}
#endif /* EXGPIO2_ENABLE*/

#ifdef EXGPIO3_ENABLE
static void ExGpio3_Reset(void)
{
	HAL_GPIO_WritePin(EXGPIO3_PORT, EXGPIO3_PIN, GPIO_PIN_RESET);
}
#endif /* EXGPIO3_ENABLE*/

#ifdef EXGPIO1_ENABLE
static void ExGpio1_Toggle(void)
{
	HAL_GPIO_TogglePin(EXGPIO1_PORT, EXGPIO1_PIN);
}
#endif /* EXGPIO1_ENABLE*/

#ifdef EXGPIO2_ENABLE
static void ExGpio2_Toggle(void)
{
	HAL_GPIO_TogglePin(EXGPIO2_PORT, EXGPIO2_PIN);
}
#endif /* EXGPIO2_ENABLE*/

#ifdef EXGPIO3_ENABLE
static void ExGpio3_Toggle(void)
{
	HAL_GPIO_TogglePin(EXGPIO3_PORT, EXGPIO3_PIN);
}
#endif /* EXGPIO3_ENABLE*/

#ifdef EXGPIO1_ENABLE
static tdu1 ExGpio1_Read(void)
{
	if (HAL_GPIO_ReadPin(EXGPIO1_PORT, EXGPIO1_PIN) == GPIO_PIN_SET) 
	{
		return 1;
	}
	else 
	{
		return 0;
	}
}
#endif /* EXGPIO1_ENABLE*/

#ifdef EXGPIO2_ENABLE
static tdu1 ExGpio2_Read(void)
{
	if (HAL_GPIO_ReadPin(EXGPIO2_PORT, EXGPIO2_PIN) == GPIO_PIN_SET) 
	{
		return 1;
	}
	else 
	{
		return 0;
	}
}
#endif /* EXGPIO2_ENABLE*/

#ifdef EXGPIO3_ENABLE
static tdu1 ExGpio3_Read(void)
{
	if (HAL_GPIO_ReadPin(EXGPIO3_PORT, EXGPIO3_PIN) == GPIO_PIN_SET) 
	{
		return 1;
	}
	else 
	{
		return 0;
	}
}
#endif /* EXGPIO3_ENABLE*/


void ExGpio_Init(void)
{
#if ((defined EXGPIO1_ENABLE) || (defined EXGPIO2_ENABLE) || (defined EXGPIO3_ENABLE))	
	GPIO_InitTypeDef GPIO_InitStructure; 
#endif
	
#ifdef 	EXGPIO1_ENABLE
	GPIO_InitStructure.Pin = EXGPIO1_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(EXGPIO1_PORT, &GPIO_InitStructure);
#endif /* EXGPIO1_ENABLE*/
	
#ifdef 	EXGPIO2_ENABLE
	GPIO_InitStructure.Pin = EXGPIO2_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(EXGPIO2_PORT, &GPIO_InitStructure);
#endif /* EXGPIO2_ENABLE*/
	
#ifdef 	EXGPIO3_ENABLE
	GPIO_InitStructure.Pin = EXGPIO3_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(EXGPIO3_PORT, &GPIO_InitStructure);
#endif /* EXGPIO3_ENABLE*/

}

void ExGpio_Set(tdu1 ch)
{
#ifdef EXGPIO1_ENABLE
	if (ch == 1) 
	{
		ExGpio1_Set();
	}
#endif /* EXGPIO1_ENABLE*/
#ifdef EXGPIO2_ENABLE
	if (ch == 2)
	{
		ExGpio2_Set();
	}
#endif /* EXGPIO2_ENABLE*/
#ifdef EXGPIO3_ENABLE
	if (ch == 3) 
	{
		ExGpio3_Set();
	}
#endif /* EXGPIO3_ENABLE*/
}

void ExGpio_Reset(tdu1 ch)
{
#ifdef EXGPIO1_ENABLE
	if (ch == 1) 
	{
		ExGpio1_Reset();
	}
#endif /* EXGPIO1_ENABLE*/
#ifdef EXGPIO2_ENABLE
	if (ch == 2)
	{
		ExGpio2_Reset();
	}
#endif
#ifdef EXGPIO3_ENABLE
	if (ch == 3)
	{
		ExGpio3_Reset();
	}
#endif /* EXGPIO3_ENABLE*/
}

void ExGpio_Toggle(tdu1 ch)
{
#ifdef EXGPIO1_ENABLE
	if (ch == 1) 
	{
		ExGpio1_Toggle();
	}
#endif /* EXGPIO1_ENABLE*/
#ifdef EXGPIO2_ENABLE
	if (ch == 2)
	{
		ExGpio2_Toggle();
	}
#endif /* EXGPIO2_ENABLE*/
#ifdef EXGPIO3_ENABLE
	if (ch == 3) 
	{
		ExGpio3_Toggle();
	}
#endif /* EXGPIO3_ENABLE*/
}

tds1 ExGpio_Read(tdu1 ch)
{
#ifdef EXGPIO1_ENABLE
	if (ch == 1)
	{
		return ExGpio1_Read();
	}
#endif /* EXGPIO1_ENABLE*/
#ifdef EXGPIO2_ENABLE
	if (ch == 2)
	{
		return ExGpio2_Read();
	}
#endif /* EXGPIO2_ENABLE*/
#ifdef EXGPIO3_ENABLE
	if (ch == 3) 
	{
		return ExGpio3_Read();
	}
#endif /* EXGPIO3_ENABLE*/
	return -1;
}

