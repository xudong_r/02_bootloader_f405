/*
 * File      : Timer.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef __TIMER_H
#define __TIMER_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "TypeDef.h"
typedef void (*tpfnTimerElapsedCallback)(void);
extern tdbl Timer_HwInit(tdu1 ch, tdu4 frequency,tdu4 period,tdu2 dutyCycle);

extern void Timer_Start(tdu1 ch);

extern void Timer_Stop_IT(tdu1 ch);

extern void Timer_Start_IT(tdu1 ch);

extern void Timer_SetElapsedCallback(tdu1 ch,tpfnTimerElapsedCallback fp);
#ifdef __cplusplus
}
#endif
#endif /* __TIMER_H */

