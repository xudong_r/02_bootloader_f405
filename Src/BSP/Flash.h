/*
 * File      : Flash.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */


//	此文件耦合程度偏高

#ifndef __FLASH_H
#define __FLASH_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "TypeDef.h"
#include "../MacroLib/IPA.h"
	
#define FLASH_PAGEADDR(PageIndex) (FLASH_BASE+PageIndex*FLASH_PAGE_SIZE)

typedef unsigned int (*tpFnWriteFlash)(unsigned int pFlashAddress, unsigned int* pWordByte, unsigned int wordLength);

typedef unsigned int (*tpFnEraserFlash)(unsigned short sectorIndex, unsigned short numOfSector);

extern void FLASH_Unlock(void);
	
extern tdu4 FLASH_EraserPages(tdu2 PageIndex,tdu2 PageNum);
	
extern tdu4 FLASH_WritePage(tdu2 PageIndex,tdu1* pByte,tdu2 ByteNum);
	
extern void FLASH_ReadPage(tdu2 PageIndex,tdu2 PageOffset,tdu1* pByte,tdu2 ByteNum);

extern tpFnWriteFlash Flash_GetWriteWorsHandle(void);

extern tpFnEraserFlash FLASH_GetEraserPagesHandle(void);

#ifdef __cplusplus
}
#endif
#endif /* __FLASH_H */

