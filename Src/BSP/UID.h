/*
 * File      : UID.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __UID_H
#define __UID_H

#ifdef __cplusplus
extern "C" 
{
#endif

#include "TypeDef.h"

#ifdef STM32F103xC
#define UID_BASEADDR (0x1FFFF7E8)
#define UID_FLASHADDR (0x1FFFF7E0)
#endif

#ifdef STM32F405xx
#define UID_BASEADDR (0x1FFF7A10)
#define UID_FLASHADDR (0x1FFF7A22)
#endif
	
#ifdef STM32F103xB
#define UID_BASEADDR (0x1FFFF7E8)
#define UID_FLASHADDR (0x1FFFF7E0)
#endif

extern void UID_Init(void);

extern tdu1* UID_GetUID(void);
    
extern tdu1* UID_GetFlashCap(void);


#ifdef __cplusplus
}
#endif
#endif /* __UID_H */ 
