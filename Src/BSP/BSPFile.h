/*
 * File      : BSPFile.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __BSP_FILE_H
#define __BSP_FILE_H

#ifdef __cplusplus
extern "C"
{
#endif
#define HW_DATA_HUB				(0x07)				// DataHub

#include "../Include/TypeDef.h"

#if HW_PLATFORM == HW_DATA_HUB
#include "BSPFile_DataHub.h"
#endif


#ifdef __cplusplus
}
#endif
#endif /* __BSP_FILE_H */

