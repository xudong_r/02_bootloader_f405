/*
 * File      : ADC.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#ifndef __ADC_H
#define __ADC_H

#include "../Include/TypeDef.h"

extern void ADC_HwInit(tdu1 adc);

extern void ADC_Start(tdu1 adc);

extern void ADC_GetAllVolt(tdu1 adc,tdf4* volt,tdu1 chNum);

extern tdf4 ADC_GetVolt(tdu1 adc,tdu1 ch);

#endif /* __ADC_H */
