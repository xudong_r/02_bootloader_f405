/*
 * File      : BootLoader.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#ifndef __BOOTLOADER_H
#define __BOOTLOADER_H

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef STM32F103xC
#define BOOTLOADER_OFFSET (0xA000)//20Page*2Kb/Page
#endif

#ifdef STM32F103xB
#define BOOTLOADER_OFFSET (0x5000)//20Page*1Kb/Page
#endif
	
#ifdef STM32F405xx
#define BOOTLOADER_OFFSET (0x10000)
#endif

extern void JumpToBootLoader(void);

#ifdef __cplusplus
}
#endif
#endif /* __BOOTLOADER_H */

