/*
 * File       : UART2.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */

#ifndef _UART2_H
#define _UART2_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "Uart.h"
#include "../Include/TypeDef.h"


extern void Uart2_HwInit(tdu4 baudRate, char parity, tdf4 stopBits,tdu2 bufferLength);

extern void Uart2_SetBaudrate(tdu4 baudRate);

extern tpFnWrite Uart2_GetWriteHandler(void);

extern void Uart2_SetRxISRFunc(tpfnUartRxISRFunc fp);

extern UART_HandleTypeDef* Uart2_GetHandle(void);

extern void Uart2_TxCpltCallBack(void);

extern void Uart2_RxCpltCallBack(void);

extern void Uart2_ErrorCpltCallBack(void);


/** 临时 **/ 
extern tdbl Uart2_WriteBytes(byte *pByte, U16 byteNum);

extern tdbl Uart2_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum);
/** 临时 **/ 

#ifdef __cplusplus
}
#endif
#endif /* __UART2_H */


