/*
 * File       : UART1.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */


#include "Uart1.h"
#include <stdio.h>
#include <string.h>
#include "../MacroLib/MemApply.h"
#include "BSPFile.h"

#ifdef OS_ENABLE
#include "OSPort.h"
#endif /* OS_ENABLE */

#ifdef UART1_ENABLE

static UART_HandleTypeDef Uart1Handle;

// 内部函数声明
static void Uart1_PrepareRecData(void);
static void Uart1_TxQueueInit(tdu2 length);

UART_HandleTypeDef* Uart1_GetHandle(void)
{
	return &Uart1Handle;
}

static void Uart1_Rcc_HwInit(void)
{
	__HAL_RCC_USART1_CLK_ENABLE();
}

static void Uart1_Gpio_HwInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.Pin = UART1_TX_GPIO_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Alternate = GPIO_AF7_USART1;	
	HAL_GPIO_Init(UART1_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = UART1_RX_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Alternate = GPIO_AF7_USART1;
	HAL_GPIO_Init(UART1_RX_GPIO_PORT, &GPIO_InitStructure);
}

static void Uart1_Uart_HwInit(tdu4 baudrate,char parity,tdf4 stopBits)
{
	Uart1Handle.Instance = USART1;

	HAL_UART_DeInit(&Uart1Handle);

	Uart1Handle.Init.BaudRate = baudrate;
	Uart1Handle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	Uart1Handle.Init.OverSampling = UART_OVERSAMPLING_16;
	Uart1Handle.Init.Mode = UART_MODE_TX_RX;

	if (stopBits == 2.0f)
	{
		Uart1Handle.Init.WordLength = UART_WORDLENGTH_9B;
	}
	else
	{
		Uart1Handle.Init.WordLength = UART_WORDLENGTH_8B;
	}

	if(stopBits == 2.0f)
		Uart1Handle.Init.StopBits = UART_STOPBITS_2;
	else//1.0f
		Uart1Handle.Init.StopBits = UART_STOPBITS_1;

	if(parity == 'O' || parity == 'o')
		Uart1Handle.Init.Parity = UART_PARITY_ODD;
	else if(parity == 'E' || parity == 'e')
		Uart1Handle.Init.Parity = UART_PARITY_EVEN;
	else//(parity == 'n' || parity == 'N')
		Uart1Handle.Init.Parity = UART_PARITY_NONE;

	HAL_UART_Init(&Uart1Handle);

}

static void Uart1_Nvic_HwInit(void)
{
	HAL_NVIC_SetPriority(USART1_IRQn, UART1_PRIPRIO,UART1_SUBPRIO);
	HAL_NVIC_EnableIRQ(USART1_IRQn);
}

void Uart1_HwInit(tdu4 baudrate,char parity,tdf4 stopBits,tdu2 bufferLength)
{
	Uart1_Rcc_HwInit();
	Uart1_Gpio_HwInit();
	Uart1_Nvic_HwInit();
	Uart1_Uart_HwInit(baudrate, parity, stopBits);
	__HAL_UART_ENABLE(&Uart1Handle);

	// 初始化发送队列
	Uart1_TxQueueInit(bufferLength);
	// 开启接收功能
	Uart1_PrepareRecData();
}


void Uart1_SetBaudrate(tdu4 baudrate)
{
	if (Uart1Handle.Instance == USART1)
	{
		Uart1Handle.Init.BaudRate = baudrate;
		HAL_UART_Init(&Uart1Handle);
	}
}


tdbl Uart1_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum)
{
	HAL_StatusTypeDef res;

	res = HAL_UART_Transmit_IT(&Uart1Handle,pBytes,byteNum);

	if (res == HAL_OK)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static tdu1 Uart1RxByte;
void USART1_IRQHandler(void)
{
#ifdef OS_ENABLE
	OSPort_Interrupt_Enter();
#endif /* OS_ENABLE */
	HAL_UART_IRQHandler(&Uart1Handle);
	if((__HAL_UART_GET_FLAG(&Uart1Handle, UART_FLAG_ORE) != RESET))
	{
		HAL_UART_Receive_IT(&Uart1Handle, &Uart1RxByte,1);
	}
#ifdef OS_ENABLE
	OSPort_Interrupt_Leave();
#endif /* OS_ENABLE */
}


/****** 以下代码与硬件平台无关 ******/

static tQueue Uart1TxQueue;
static tdu1* pUart1TxBuffer;
void Uart1_TxQueueInit(tdu2 length)
{
	Queue_Init(&Uart1TxQueue, 2*length);
	pUart1TxBuffer = (tdu1 *)MemApply(length);
	memset((void*)pUart1TxBuffer,0,length);
}
// 从队列中取出全部数据
tdu4 Uart1_TxAllDequeue(byte *data)
{

    return (Queue_PopBytes(&Uart1TxQueue, data, Uart1TxQueue.Count)); 

}


tdbl Uart1_WriteBytes(byte *pByte, U16 byteNum)
{
	tdbl res;
    if (byteNum==0) 
	{ 
        // 发送长度为0
        return FALSE;
    }

	//·发送字节
	res = Uart1_WriteBytes_IT(pByte,byteNum);
	if (res == FALSE)
	{
		//·发送失败,将数据放入队列
		Queue_Enqueue(&Uart1TxQueue,pByte,byteNum);

	}
	return TRUE;
}

tpFnWrite Uart1_GetWriteHandler(void)
{
	return Uart1_WriteBytes;
}


void Uart1_TxCpltCallBack(void)
{
    // 从队列中取出全部数据
    tdu4 byteNum;
    byteNum = Uart1_TxAllDequeue(pUart1TxBuffer);
    if (byteNum!=0) {
		// 将数据放送出去
		Uart1_WriteBytes_IT(pUart1TxBuffer, byteNum);
    }
}



static tpfnUartRxISRFunc fpUartRxISRFunc = NULL;

void Uart1_SetRxISRFunc(tpfnUartRxISRFunc fp)
{
	fpUartRxISRFunc = fp;
}

void Uart1_RxCpltCallBack(void)
{
	// 接收数据
	if (fpUartRxISRFunc != NULL)
	{
		fpUartRxISRFunc(Uart1RxByte);
	}
	// 开启中断继续接收
	HAL_UART_Receive_IT(&Uart1Handle, &Uart1RxByte,1);
}
void Uart1_ErrorCpltCallBack(void)
{
	// 开启中断继续接收
	HAL_UART_Receive_IT(&Uart1Handle, &Uart1RxByte,1);
}

static void Uart1_PrepareRecData(void)
{
	HAL_UART_Receive_IT(&Uart1Handle, &Uart1RxByte,1);
}

#endif //end of (#ifdef UART1_ENABLE)
