/*
 * File      : ExGPIO.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef __EXGPIO_H
#define __EXGPIO_H

#ifdef __cplusplus
extern "C"
{
#endif
#include "TypeDef.h"
extern void ExGpio_Init(void);
extern void ExGpio_Set(tdu1 ch);
extern void ExGpio_Reset(tdu1 ch);
extern void ExGpio_Toggle(tdu1 ch);
extern tds1 ExGpio_Read(tdu1 ch);

#ifdef __cplusplus
}
#endif
#endif /* __EXGPIO_H */

