/*
 * File      : RCC.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "RCC.h"
#include "stm32f4xx_hal.h"
#include "BSPFile.h"
static void RCC_EnablePeriph(void);

void Rcc_Init()
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
#if ((defined RCC_USE_USB) || (defined RCC_USE_ADC))
	RCC_PeriphCLKInitTypeDef PeriphClkInit;
#endif
	__HAL_RCC_PWR_CLK_ENABLE();

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
/**
 * 使用外部有源晶振
 */
#ifdef RCC_PLL_CLK_8_168MHz_OSC
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;			// 使用外部有源晶振
	RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;						// 外部有源
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;						// 打开PLL
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;				// PLL时钟选择为HSE
	RCC_OscInitStruct.PLL.PLLM = 8;										// 主PLL分频系数
	RCC_OscInitStruct.PLL.PLLN = 336;									// 主PLL倍频系数
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;							// 系统时钟的主PLL分频系数
	RCC_OscInitStruct.PLL.PLLQ = 7;										// USB/SDIO/随机数产生器的主PLL分频系数
	while(HAL_RCC_OscConfig(&RCC_OscInitStruct)!=HAL_OK);				// 初始化
#endif

/**
 * 使用外部无源晶振
 */
#ifdef RCC_PLL_CLK_8_168MHz_CRYSTAL

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;			// 使用外部晶振
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;							// 外部无源
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;						// 打开PLL
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;				// PLL时钟选择为HSE
	RCC_OscInitStruct.PLL.PLLM = 8;										// 主PLL分频系数
	RCC_OscInitStruct.PLL.PLLN = 336;									// 主PLL倍频系数
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;							// 系统时钟的主PLL分频系数
	RCC_OscInitStruct.PLL.PLLQ = 7;										// USB/SDIO/随机数产生器的主PLL分频系数
	while(HAL_RCC_OscConfig(&RCC_OscInitStruct)!=HAL_OK);				// 初始化
#endif

/**
 * 使用内部晶振
 */
//#ifdef RCC_PLL_CLK_64MHz
//RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
//RCC_OscInitStruct.HSIState = RCC_HSI_ON;
//RCC_OscInitStruct.HSICalibrationValue = 16;
//RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
//RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
//RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;  
//while(HAL_RCC_OscConfig(&RCC_OscInitStruct)!=HAL_OK);
//#endif

	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
							  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	
	while(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK);

/**
 * 初始化USB时钟，48MHz，仅在外部晶振时可用
// */
//#ifdef RCC_USE_USB

//  #ifdef RCC_PLL_CLK_64MHz
//  #error "CAN NOT USE USB WHEN CLK 64MHZ";
//  #endif
//  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
//  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
//  while(HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK);
//#endif

///**
// * 初始化ADC时钟
// */
//#ifdef RCC_USE_ADC
//  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
//  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
//  while(HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit)!= HAL_OK);
//#endif

/**
 * 初始化SysTick滴答定时器
 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

	RCC_EnablePeriph();
}

void RCC_EnablePeriph()
{
/****************************************************** 
                   SECTION：GPIO外设时钟
*******************************************************/
#ifdef GPIOA_ENABLE
    __HAL_RCC_GPIOA_CLK_ENABLE(); 
#endif

#ifdef GPIOB_ENABLE
   __HAL_RCC_GPIOB_CLK_ENABLE();
#endif

#ifdef GPIOC_ENABLE
   __HAL_RCC_GPIOC_CLK_ENABLE();
#endif

#ifdef GPIOD_ENABLE
   __HAL_RCC_GPIOD_CLK_ENABLE();
#endif

/****************************************************** 
                   SECTION：AFIO外设时钟
*******************************************************/
#ifdef AFIO_ENABLE
   __HAL_RCC_AFIO_CLK_ENABLE();
#endif

/****************************************************** 
                   SECTION：UART外设时钟
*******************************************************/
#ifdef UART1_ENABLE
   __HAL_RCC_USART1_CLK_ENABLE();
#endif

#ifdef UART2_ENABLE
   __HAL_RCC_USART2_CLK_ENABLE();
#endif

#ifdef UART3_ENABLE
   __HAL_RCC_USART3_CLK_ENABLE();
#endif

#ifdef UART4_ENABLE
   __HAL_RCC_UART4_CLK_ENABLE();
#endif

#ifdef UART5_ENABLE
   __HAL_RCC_UART5_CLK_ENABLE();
#endif

/****************************************************** 
                   SECTION：TIM外设时钟
*******************************************************/
#ifdef TIM1_ENABLE
    __HAL_RCC_TIM1_CLK_ENABLE();
#endif

#ifdef TIM2_ENABLE
   __HAL_RCC_TIM2_CLK_ENABLE();
#endif

#ifdef TIM3_ENABLE
   __HAL_RCC_TIM3_CLK_ENABLE();
#endif

#ifdef TIM4_ENABLE
    __HAL_RCC_TIM4_CLK_ENABLE();
#endif

#ifdef TIM5_ENABLE
    __HAL_RCC_TIM5_CLK_ENABLE();
#endif

#ifdef TIM6_ENABLE
    __HAL_RCC_TIM6_CLK_ENABLE();
#endif

#ifdef TIM7_ENABLE
    __HAL_RCC_TIM7_CLK_ENABLE();
#endif

#ifdef TIM8_ENABLE
    __HAL_RCC_TIM8_CLK_ENABLE();
#endif

/****************************************************** 
                   SECTION：SPI外设时钟
*******************************************************/
#ifdef SPI1_ENABLE
    __HAL_RCC_SPI1_CLK_ENABLE();
#endif

#ifdef SPI2_ENABLE
    __HAL_RCC_SPI2_CLK_ENABLE();
#endif

/****************************************************** 
                   SECTION：DMA外设时钟
*******************************************************/
#ifdef DMA1_ENABLE
    __HAL_RCC_DMA1_CLK_ENABLE();
#endif

#ifdef DMA2_ENABLE
    __HAL_RCC_DMA2_CLK_ENABLE();
#endif

/****************************************************** 
                   SECTION：SDIO外设时钟
*******************************************************/
#ifdef SDIO_ENABLE
    __HAL_RCC_SDIO_CLK_ENABLE();
#endif

/****************************************************** 
                   SECTION：CAN外设时钟
*******************************************************/
#ifdef CAN_ENABLE
    __HAL_RCC_CAN1_CLK_ENABLE();
#endif

  
/****************************************************** 
                   SECTION：IIC外设时钟
*******************************************************/  
#ifdef I2C1_ENABLE
    __HAL_RCC_I2C1_CLK_ENABLE();
#endif

#ifdef I2C2_ENABLE
    __HAL_RCC_I2C2_CLK_ENABLE();
#endif

/****************************************************** 
                   SECTION：ADC外设时钟
*******************************************************/
#ifdef ADC1_ENABLE
    __HAL_RCC_ADC1_CLK_ENABLE();
    
#endif

#ifdef ADC2_ENABLE
    __HAL_RCC_ADC2_CLK_ENABLE();
#endif

#ifdef ADC3_ENABLE
    __HAL_RCC_ADC3_CLK_ENABLE();
#endif

}

void RCC_DisablePeriph(void)
{
/****************************************************** 
                   SECTION：GPIO外设时钟
*******************************************************/
#ifdef GPIOA_ENABLE
    __HAL_RCC_GPIOA_CLK_DISABLE(); 
#endif

#ifdef GPIOB_ENABLE
   __HAL_RCC_GPIOB_CLK_DISABLE();
#endif

#ifdef GPIOC_ENABLE
   __HAL_RCC_GPIOC_CLK_DISABLE();
#endif

#ifdef GPIOD_ENABLE
   __HAL_RCC_GPIOD_CLK_DISABLE();
#endif

/****************************************************** 
                   SECTION：AFIO外设时钟
*******************************************************/
#ifdef AFIO_ENABLE
   __HAL_RCC_AFIO_CLK_DISABLE();
#endif

/****************************************************** 
                   SECTION：UART外设时钟
*******************************************************/
#ifdef UART1_ENABLE
   __HAL_RCC_USART1_CLK_DISABLE();
#endif

#ifdef UART2_ENABLE
   __HAL_RCC_USART2_CLK_DISABLE();
#endif

#ifdef UART3_ENABLE
   __HAL_RCC_USART3_CLK_DISABLE();
#endif

#ifdef UART4_ENABLE
   __HAL_RCC_UART4_CLK_DISABLE();
#endif

#ifdef UART5_ENABLE
   __HAL_RCC_UART5_CLK_DISABLE();
#endif

/****************************************************** 
                   SECTION：TIM外设时钟
*******************************************************/
#ifdef TIM1_ENABLE
    __HAL_RCC_TIM1_CLK_DISABLE();
#endif

#ifdef TIM2_ENABLE
   __HAL_RCC_TIM2_CLK_DISABLE();
#endif

#ifdef TIM3_ENABLE
   __HAL_RCC_TIM3_CLK_DISABLE();
#endif

#ifdef TIM4_ENABLE
    __HAL_RCC_TIM4_CLK_DISABLE();
#endif

#ifdef TIM5_ENABLE
    __HAL_RCC_TIM5_CLK_DISABLE();
#endif

#ifdef TIM6_ENABLE
    __HAL_RCC_TIM6_CLK_DISABLE();
#endif

#ifdef TIM7_ENABLE
    __HAL_RCC_TIM7_CLK_DISABLE();
#endif

#ifdef TIM8_ENABLE
    __HAL_RCC_TIM8_CLK_DISABLE();
#endif

/****************************************************** 
                   SECTION：SPI外设时钟
*******************************************************/
#ifdef SPI1_ENABLE
    __HAL_RCC_SPI1_CLK_DISABLE();
#endif

#ifdef SPI2_ENABLE
    __HAL_RCC_SPI2_CLK_DISABLE();
#endif

/****************************************************** 
                   SECTION：DMA外设时钟
*******************************************************/
#ifdef DMA1_ENABLE
    __HAL_RCC_DMA1_CLK_DISABLE();
#endif

#ifdef DMA2_ENABLE
    __HAL_RCC_DMA2_CLK_DISABLE();
#endif

/****************************************************** 
                   SECTION：SDIO外设时钟
*******************************************************/
#ifdef SDIO_ENABLE
    __HAL_RCC_SDIO_CLK_DISABLE();
#endif

/****************************************************** 
                   SECTION：CAN外设时钟
*******************************************************/
#ifdef CAN1_ENABLE
    __HAL_RCC_CAN1_CLK_DISABLE();
#endif

#ifdef CAN2_ENABLE
    __HAL_RCC_CAN2_CLK_DISABLE();
#endif
  
/****************************************************** 
                   SECTION：IIC外设时钟
*******************************************************/  
#ifdef I2C1_ENABLE
    __HAL_RCC_I2C1_CLK_DISABLE();
#endif

#ifdef I2C2_ENABLE
    __HAL_RCC_I2C2_CLK_DISABLE();
#endif

/****************************************************** 
                   SECTION：ADC外设时钟
*******************************************************/
#ifdef ADC1_ENABLE
    __HAL_RCC_ADC1_CLK_DISABLE();
    
#endif

#ifdef ADC2_ENABLE
    __HAL_RCC_ADC2_CLK_DISABLE();
#endif

#ifdef ADC3_ENABLE
    __HAL_RCC_ADC3_CLK_DISABLE();
#endif

}
