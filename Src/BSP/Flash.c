/*
 * File      : Flash.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "Flash.h"
#include "stm32f4xx_hal.h"
#include <string.h>
#include "BSPFile.h"

void FLASH_Unlock()
{
    HAL_FLASH_Unlock();
}


tdu4 FLASH_EraserPages(tdu2 sectorIndex, unsigned short numOfSector)
{
    tdu4 PageError = 0;
	tdu1 res = 0xFF;
	
 	HAL_FLASH_Unlock();

	FLASH_EraseInitTypeDef FLASH_EraseInitStructure;

    FLASH_EraseInitStructure.TypeErase = FLASH_TYPEERASE_SECTORS;

    FLASH_EraseInitStructure.Banks = FLASH_BANK_1;
	
	FLASH_EraseInitStructure.Sector = sectorIndex;

    FLASH_EraseInitStructure.NbSectors = numOfSector;

    FLASH_EraseInitStructure.VoltageRange = FLASH_VOLTAGE_RANGE_3;
    
    res = HAL_FLASHEx_Erase(&FLASH_EraseInitStructure, &PageError);
	
	HAL_FLASH_Lock(); 

	if(res == HAL_OK || PageError == 0xFFFFFFFFU)
	{
		//擦出错误
		res = 0x00;
	}
	
	return res;
}

// 读取Flash中指定位置的数据，暂时无法使用，
// 待办：1、判断其实地址，读取字节数是否为偶数。
void FLASH_ReadBytes(tdu2 pageIndex,tdu2 pageOffset,tdu1* pByte,tdu2 byteNum)
{
//	tdu4 startAddr;
//	tdu4 endAddr;
//	tdbl startAddrFlag = TRUE;
//	tdu4 currAddr;
//	tdu2 i;
//	startAddr =  pageOffset;

//    endAddr = startAddr + byteNum;

//    if (startAddr%2) 
//    {
//        startAddr--;
//        startAddrFlag = FALSE;
//    }
//    if (endAddr%2) 
//    {
//        endAddr++;
//    }
//    for (currAddr = startAddr,i = 0;currAddr<endAddr;currAddr+=2) 
//    {
//        tempBuffer[i] = (*(tdu2*)currAddr);
//        i++;
//    }

//    if (startAddrFlag == TRUE) 
//    {
//        memcpy((void*)pByte,(void*)&tempBuffer[0],byteNum);
//    }
//    else
//    {
//        memcpy((void*)pByte,(void*)&tempBuffer[1],byteNum);
//    }
//   
}


tdu4 FLASH_WritePage(tdu2 pageIndex, tdu1* pByte, tdu2 byteNum)
{
	tdu1 res = 0xFF;
	//pageNum,offset,*pByte,byteNum
	tdu2 WordLength;
	tdu1 tryTime = 5;

	tdu2 i;
	tdu1 FlashWriteBuf[256] = {0xFF};

	FLASH_Unlock();
	
    do
	{
		res = FLASH_EraserPages(pageIndex,1);
	} while(res && tryTime--);
	
	if(res == 0x00)
	{
		// 擦除正确
		memcpy((void*)&FlashWriteBuf[0], pByte, byteNum);

		WordLength = (256 % 4) ? (256 / 4 + 1) : (256 / 4);
		
		HAL_FLASH_Unlock();
		
		for (i = 0; i < WordLength; i++) 
		{
			res = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, (0x0800C000 + i * 4), (*(tdu4*)(&FlashWriteBuf[i * 4])));
			if(res != HAL_OK)
			{
				HAL_FLASH_Lock(); 
				return 1;
			}
		}
		HAL_FLASH_Lock(); 
	}
	else
	{
		// 擦出错误
	}
	return res;
}




tdu4 FLash_WriteWords(tdu4 pFlashAddress, tdu4* pWordByte ,tdu4 wordLength)
{
    tdu2 i;
    
    HAL_FLASH_Unlock();
    
    for(i = 0;i<wordLength;i++)
    {
        if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, pFlashAddress + 4 * i, pWordByte[i]) != HAL_OK)
        {
            return 1;
        }
    }
	
    HAL_FLASH_Lock();
    
    return 0;
}

tpFnWriteFlash Flash_GetWriteWorsHandle(void)
{
	return FLash_WriteWords;
}

tpFnEraserFlash FLASH_GetEraserPagesHandle(void)
{
	return FLASH_EraserPages;
}
