/*
 * File      : ADC.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#include "ADC.h"
#include "BSPFile.h"

#ifdef ADC1_ENABLE
#include "ADC1.h"
#endif /* ADC1_ENABLE */

#ifdef ADC2_ENABLE
#include "ADC2.h"
#endif /* ADC2_ENABLE */

void ADC_HwInit(tdu1 ch)
{
#ifdef ADC1_ENABLE
	if (ch == 1)
	{
		ADC1_HwInit();
	}
#endif

#ifdef ADC2_ENABLE
	if (ch == 2)
	{
		ADC2_HwInit();
	}
#endif

}

void ADC_Start(tdu1 adc)
{
#ifdef ADC1_ENABLE
	if (adc == 1)
	{
		ADC1_Start();
	}
#endif

#ifdef ADC2_ENABLE
	if (adc == 2)
	{
		ADC2_Start();
	}
#endif
	
}

void ADC_GetAllVolt(tdu1 adc,tdf4* volt,tdu1 chNum)
{
#ifdef ADC1_ENABLE
	if (adc == 1) 
	{
		ADC1_GetAllVolt(volt,chNum);
	}
#endif

#ifdef ADC2_ENABLE
	if (adc == 2)
	{
		ADC2_GetAllVolt(volt,chNum);
	}
#endif
}

tdf4 ADC_GetVolt(tdu1 adc,tdu1 ch)
{
#ifdef ADC1_ENABLE
	if (adc == 1) 
	{
		return ADC1_GetVolt(ch);
	}
#endif

#ifdef ADC2_ENABLE
	if (adc == 2)
	{
		return ADC2_GetVolt(ch);
	}
#endif
	return 0.0f;
}



