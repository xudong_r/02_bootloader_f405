/*
 * File      : BSPFile_DataHub.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __BSPFILE_DATA_HUB_H
#define __BSPFILE_DATA_HUB_H

#ifdef __cplusplus
extern "C"
{
#endif


#define DEMO_MODE	//DEMO MODE

#ifdef STM32F405xx
//#define MCU_FLASH_PAGE_NUM		(128)
#define MCU_FLASH_DR_PAGE_NUM	(6)
#define MCU_FLASH_DR_PAGE_SIZE	(128)
#endif

//#ifdef STM32F103xE
//#define MCU_FLASH_PAGE_NUM		(256)
//#define MCU_FLASH_DR_PAGE_NUM	(5)
//#define MCU_FLASH_DR_PAGE_SIZE	(512)
//#endif
	
/****************************************************** 
                   STEP1：系统时钟配置
*******************************************************/
#define RCC_PLL_CLK_8_168MHz_OSC				//外部有源晶振
//#define RCC_PLL_CLK_8_168MHz_CRYSTAL			//外部无源晶振
//#define RCC_PLL_CLK_64MHz					//内部晶振

//#define RCC_USE_ADC							//使用ADC需开启此宏，ADC最大采样时钟为
//#define RCC_USE_USB							//使用USB需开启此宏，配置USB时钟，仅在72MHz时钟下使用
/******************************************************/ 


/****************************************************** 
                   STEP2：系统端口复用配置
*******************************************************/
//#define AFIO_CAN1_REMAP_2					//不适用于36脚封装
//#define AFIO_CAN1_REMAP_3					//只适用于100 144脚 
//#define AFIO_JTAG_1   					//JTAG+SW 但没有JNTRST
#define AFIO_JTAG_2   						//关闭JTAG启用SW
//#define AFIO_JTAG_4   					//关闭JTAG关闭SW
//#define AFIO_TIM1_1 						//ETR:PA12 CH1:PA8 CH2:PA9 CH3:PA10 CH4 PA11 BKIN:PA6 CH1N:PA7 CH2N:PB0 CH3N:PB1
//#define AFIO_TIM1_3 						//ETR:PE7 CH1:PE9 CH2:PE11 CH3:PE13 CH4 PE14 BKIN:PE15 CH1N:PE8 CH2N:PE10 CH3N:PE12
//#define AFIO_TIM2_1 						//CH1:PA15 CH2:PB3 CH3:PA2 CH4:PA3
//#define AFIO_TIM2_2 						//CH1:PA0 CH2:PA1 CH3:PB10 CH4:PB11
//#define AFIO_TIM2_3 						//CH1:PA15 CH2:PB3 CH3:PB10 CH4:PB11
//#define AFIO_TIM3_2 						//CH1:PB4:CH2:PB5 CH3:PB0 CH4:PB1
//#define AFIO_TIM3_3 						//CH1:PC6 CH2:PC7 CH3:PC8 CH4:PC9
//#define AFIO_TIM4 						//CH1:PD12 CH2:PD13 CH3:PD14 CH4:PD15
//#define AFIO_TIM5 						//TIM5_CH4与LSI内部时钟相连
//#define AFIO_UART1 						//TX:PB6 RX:PB7
//#define AFIO_UART2 						//TX:PD5 RX:PD6
//#define AFIO_UART3_1 						//TX:PC10 RX:PC11
//#define AFIO_UART3_2 						//TX:PD8 RX：PD9
//#define AFIO_I2C1 						//SCL：PB8 SDA：PB9
//#define AFIO_SPI1 						//SCK PB3 MISO PB4 MOSI PB5
//#define AFIO_SPI3 						//SCK PC10 MISO PC11 MOSI PC12
/******************************************************/ 


/****************************************************** 
                   STEP3：系统使用GPIO口配置
*******************************************************/
#define GPIOA_ENABLE
#define GPIOB_ENABLE
#define GPIOC_ENABLE
#define GPIOD_ENABLE
/****************************************************** 
                   STEP4：系统使用LED灯配置
*******************************************************/

#define LED_MAX_NUM			(3)				//LED使用数目

#define LED1_GPIO_PORT		GPIOC
#define LED1_GPIO_PIN		GPIO_PIN_13
#define LED1_GPIO_POLARITY	LED_POLARITY_POSITIVE

#define LED2_GPIO_PORT		GPIOC
#define LED2_GPIO_PIN		GPIO_PIN_14
#define LED2_GPIO_POLARITY	LED_POLARITY_NEGATIVE

#define LED3_GPIO_PORT		GPIOC
#define LED3_GPIO_PIN		GPIO_PIN_15
#define LED3_GPIO_POLARITY	LED_POLARITY_POSITIVE

//#define LED4_GPIO_PORT		GPIOA
//#define LED4_GPIO_PIN		GPIO_PIN_7
//#define LED4_GPIO_POLARITY	LED_POLARITY_NEGATIVE


/****************************************************** 
                   STEP5：系统使用EXGPIO配置
*******************************************************/
//#define EXGPIO_MAX_NUM			(3)

#define EEXGPIO1_ENABLE
#define EXGPIO1_PORT		GPIOA
#define EXGPIO1_PIN			GPIO_PIN_0

#define EEXGPIO2_ENABLE
#define EXGPIO2_PORT		GPIOA
#define EXGPIO2_PIN			GPIO_PIN_1

#define EEXGPIO2_ENABLE
#define EXGPIO3_PORT		GPIOA
#define EXGPIO3_PIN			GPIO_PIN_2

//to do: add pin work mode config 

/****************************************************** 
                   STEP6：系统使用Uart配置
*******************************************************/

#define UART1_ENABLE
#define UART2_ENABLE
#define UART3_ENABLE
#define UART4_ENABLE
#define UART5_ENABLE
//#define UART6_ENABLE

/**
 * UART1
 */
#define UART1_TX_GPIO_PORT	GPIOA
#define UART1_TX_GPIO_PIN	GPIO_PIN_9

#define UART1_RX_GPIO_PORT	GPIOA
#define UART1_RX_GPIO_PIN	GPIO_PIN_10


/**
 * UART2
 */
#define UART2_TX_GPIO_PORT	GPIOA
#define UART2_TX_GPIO_PIN	GPIO_PIN_2

#define UART2_RX_GPIO_PORT	GPIOA
#define UART2_RX_GPIO_PIN	GPIO_PIN_3

/**
 * UART3
 */
#define UART3_TX_GPIO_PORT	GPIOB
#define UART3_TX_GPIO_PIN	GPIO_PIN_10

#define UART3_RX_GPIO_PORT	GPIOB
#define UART3_RX_GPIO_PIN	GPIO_PIN_11

/**
 * UART4
 */
#define UART4_TX_GPIO_PORT	GPIOA
#define UART4_TX_GPIO_PIN	GPIO_PIN_0

#define UART4_RX_GPIO_PORT	GPIOA
#define UART4_RX_GPIO_PIN	GPIO_PIN_1

/**
 * UART5
 */
#define UART5_TX_GPIO_PORT	GPIOC
#define UART5_TX_GPIO_PIN	GPIO_PIN_12

#define UART5_RX_GPIO_PORT	GPIOD
#define UART5_RX_GPIO_PIN	GPIO_PIN_2

/**
 * UART6
 */
#define UART6_TX_GPIO_PORT	GPIOG
#define UART6_TX_GPIO_PIN	GPIO_PIN_14

#define UART6_RX_GPIO_PORT	GPIOG
#define UART6_RX_GPIO_PIN	GPIO_PIN_9
 
/****************************************************** 
                   STEP7：系统使用TIM配置
*******************************************************/ 
//#define TIM1_ENABLE
//#define TIM2_ENABLE
//#define TIM3_ENABLE
#define TIM4_ENABLE
//#define TIM5_ENABLE
//#define TIM6_ENABLE
//#define TIM7_ENABLE
//#define TIM8_ENABLE


#ifdef TIM4_ENABLE
#define TIM4_IT_ENABLE
//#define TIM4_DMA_ENABLE
#endif

 
/****************************************************** 
                   STEP8：系统使用ADC配置
*******************************************************/ 
//#define ADC1_ENABLE					//占用DMA1_CH1通道
//#define ADC2_ENABLE


#define ADC1_CHANNEL_SAMPLE_NUM		(3)				//ADC使用通道数目，不包含温度采样通道和内部参考电压通道
#define ADC1_CHANNEL_FILTER			(10)			//ADC单通道采样长度

//#define ADC1_CHANNEL_TEMP_ENABLE						//使能STM32内部温度采样，不使用不建议开启
#define ADC1_CHANNEL_VREF_ENABLE						//使能STM32内部参考电压采样,建议开启提高采样精度

//ADC1 CH1 PA0
#define ADC1_CHANNEL1					ADC_CHANNEL_0
#define ADC1_CHANNEL1_PORT				GPIOA
#define ADC1_CHANNEL1_PIN				GPIO_PIN_0
#define ADC1_CHANNEL1_RATIO				(1.0f)

#define ADC1_CHANNEL2   				ADC_CHANNEL_1
#define ADC1_CHANNEL2_PORT				GPIOA
#define ADC1_CHANNEL2_PIN				GPIO_PIN_1
#define ADC1_CHANNEL2_RATIO				(1.0f)

#define ADC1_CHANNEL3					ADC_CHANNEL_2
#define ADC1_CHANNEL3_PORT				GPIOA
#define ADC1_CHANNEL3_PIN				GPIO_PIN_0
#define ADC1_CHANNEL3_RATIO				(1.0f)

/****************************************************** 
                   STEP9：系统使用CAN配置
*******************************************************/ 
#define CAN1_ENABLE
#define CAN2_ENABLE

#define CAN1_TX_PORT		GPIOA
#define CAN1_TX_PIN			GPIO_PIN_12

#define CAN1_RX_PORT		GPIOA
#define CAN1_RX_PIN			GPIO_PIN_11

#define CAN2_TX_PORT		GPIOB
#define CAN2_TX_PIN			GPIO_PIN_13

#define CAN2_RX_PORT		GPIOB
#define CAN2_RX_PIN			GPIO_PIN_12

/****************************************************** 
                   STEP10：系统使用I2C配置
*******************************************************/ 
//#define I2C1_ENABLE

#define I2C1_SCL_GPIO_PORT GPIOB
#define I2C1_SCL_GPIO_PIN  GPIO_PIN_6

#define I2C1_SDA_GPIO_PORT GPIOB
#define I2C1_SDA_GPIO_PIN  GPIO_PIN_7



#ifdef __cplusplus
}
#endif
#endif /* __BSPFILE_DATA_HUB_H */

