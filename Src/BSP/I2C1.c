/*
 * File      : I2C1.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

// 接口不统一

#include "I2C1.h"
#include "stm32f4xx_hal.h"
#include "TypeDef.h"
#include "BSPFile.h"

#ifdef I2C1_ENABLE
static I2C_HandleTypeDef I2C1_Handle;

static void I2C1_Rcc_HwInit()
{
	__HAL_RCC_I2C1_CLK_ENABLE();
}
static void I2C1_Gpio_HwInit()
{
	// GPIO初始化MspInit中进行
}

static void I2C1_I2c_HwInit(tdu4 clockSpeed)
{

	I2C1_Handle.Instance = I2C1;

	HAL_I2C_DeInit(&I2C1_Handle);

	I2C1_Handle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	I2C1_Handle.Init.ClockSpeed = clockSpeed;
	I2C1_Handle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
	I2C1_Handle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	I2C1_Handle.Init.DutyCycle = I2C_DUTYCYCLE_2;
	I2C1_Handle.Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;
	I2C1_Handle.Init.OwnAddress1 = 0x0A;
	I2C1_Handle.Init.OwnAddress2 = 0x00;

	HAL_I2C_Init(&I2C1_Handle);
}

void I2C1_MspInit()
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = I2C1_SCL_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(I2C1_SCL_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = I2C1_SDA_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(I2C1_SDA_GPIO_PORT, &GPIO_InitStructure);
	
	// 将引脚强制拉高
	HAL_GPIO_WritePin(I2C1_SCL_GPIO_PORT, I2C1_SCL_GPIO_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(I2C1_SDA_GPIO_PORT, I2C1_SDA_GPIO_PIN, GPIO_PIN_SET);

	I2C1_Handle.Instance->CR1 = I2C_CR1_SWRST;
	I2C1_Handle.Instance->CR1 = 0;
	
	GPIO_InitStructure.Pin = I2C1_SCL_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(I2C1_SCL_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = I2C1_SDA_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(I2C1_SDA_GPIO_PORT, &GPIO_InitStructure);
}
void I2C1_HwInit(tdu4 clockSpeed)
{
	I2C1_Rcc_HwInit();
	I2C1_Gpio_HwInit();
	I2C1_I2c_HwInit(clockSpeed);
	__HAL_I2C_ENABLE(&I2C1_Handle);

}


void I2C1_WriteByte(tdu2 devAddr,tdu1* pByte,tdu2 byteNum)
{
	HAL_I2C_Master_Transmit(&I2C1_Handle, devAddr, pByte, byteNum,500);
}


I2C_HandleTypeDef *I2C1_GetHandle()
{
	return &I2C1_Handle;
}

#endif //end of (#ifdef I2C1_ENABLE)

