/*
 * File      : SPI.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __SPI_H
#define __SPI_H

#include "TypeDef.h"

#define SPI_CONFIG_1CLK_LOW     0
#define SPI_CONFIG_1CLK_HIGH    1
#define SPI_CONFIG_2CLK_LOW     2
#define SPI_CONFIG_2CLK_HIGH    3

extern void SPI_Init(tdu1 spiIndex,tdu1 spiConfig);

extern void SPI_WriteBytes(tdu1 index,tdu1* pBytes,tdu2 byteNum);

extern void SPI_ReadBytes(tdu1 index,tdu1* pBytes,tdu2 byteNum);


#endif/* __SPI_H */
