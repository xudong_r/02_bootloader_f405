/*
 * File      : CAN.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#include "CAN.h"
#include "BSPFile.h"

#ifdef CAN1_ENABLE
	#include "CAN1.h"
#endif /* CAN1_ENABLE */

#ifdef CAN2_ENABLE
	#include "CAN2.h"
#endif /* CAN2_ENABLE */

void CAN_HwInit(tdu1 canIndex)
{
#ifdef CAN1_ENABLE	
	if(canIndex == 1)
		CAN1_HwInit();
#endif
#ifdef CAN2_ENABLE
	if(canIndex == 2)	
		CAN2_HwInit();
#endif	

}


void CAN_Write(tdu1 canIndex,tdu2 ID,tdu1* data,tdu1 length)
{
	if(ID > 0x7FF)
	{
		return;
	}
#ifdef CAN1_ENABLE
	if(canIndex == 1)
		CAN1_Write(ID,data,length);
#endif	
#ifdef CAN2_ENABLE
	if(canIndex == 2)
		CAN2_Write(ID,data,length);
#endif
}

void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef *hcan)
{
#ifdef CAN1_ENABLE
	if(hcan->Instance == CAN1)
	{
		CAN1_RxCpltCallBack(hcan);
	}
#endif
#ifdef CAN2_ENABLE
	if(hcan->Instance == CAN2)
	{
		CAN2_RxCpltCallBack(hcan);
	}
#endif
}


void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan)
{
#ifdef CAN1_ENABLE
	if(hcan->Instance == CAN1)
	{
		if(HAL_BUSY == HAL_CAN_Receive_IT(hcan, CAN_FIFO0))
		{
			__HAL_CAN_ENABLE_IT(hcan, CAN_IT_FOV0 | CAN_IT_FMP0);
		}
	}
#endif
#ifdef CAN2_ENABLE
	if (hcan->Instance == CAN2)
	{
		if(HAL_BUSY == HAL_CAN_Receive_IT(hcan, CAN_FIFO0))
		{
			__HAL_CAN_ENABLE_IT(hcan, CAN_IT_FOV0 | CAN_IT_FMP0);
		}
	}
#endif
}

void CAN_SetRxISRFunc(tdu1 ch,tpFnGetIDBytes2 fp)
{
#ifdef CAN1_ENABLE
	if (ch == 1)
	{
		CAN1_SetRxISRFunc(fp);
	}
#endif	
#ifdef CAN2_ENABLE
	if (ch == 2)
	{
		CAN2_SetRxISRFunc(fp);
	}
#endif
	
}

