/*
 * File      : LED.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */


#include "TypeDef.h"
#include "LED.h"
#include "BSPFile.h"
#include "stm32f4xx_hal.h"
static tdbl LED1Disable = FALSE;
static tdbl LED2Disable = FALSE;
static tdbl LED3Disable = FALSE;

void LED_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure; 

	GPIO_InitStructure.Pin = LED1_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(LED1_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = LED2_GPIO_PIN;
	HAL_GPIO_Init(LED2_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = LED3_GPIO_PIN;
	HAL_GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStructure);

}

void LED_AllOn(void)
{
	LED1_On();
	LED2_On();
	LED3_On();
}
void LED_AllOff(void)
{
	LED1_Off();
	LED2_Off();
	LED3_Off();
}
void LED1_On(void)
{
	if(LED1Disable == TRUE)
		return;
	#if LED1_GPIO_POLARITY == LED_POLARITY_POSITIVE
	HAL_GPIO_WritePin(LED1_GPIO_PORT, LED1_GPIO_PIN, GPIO_PIN_SET);
	#else
	HAL_GPIO_WritePin(LED1_GPIO_PORT, LED1_GPIO_PIN, GPIO_PIN_RESET);
	#endif
}
void LED1_Off(void)
{
	#if LED1_GPIO_POLARITY == LED_POLARITY_POSITIVE
	HAL_GPIO_WritePin(LED1_GPIO_PORT, LED1_GPIO_PIN, GPIO_PIN_RESET);
	#else
	HAL_GPIO_WritePin(LED1_GPIO_PORT, LED1_GPIO_PIN, GPIO_PIN_SET)
	#endif
}
void LED1_Toggle(void)
{
	HAL_GPIO_TogglePin(LED1_GPIO_PORT,LED1_GPIO_PIN);
}

void LED1_Disable(void)
{
	LED1Disable = TRUE;
	LED1_Off();
}

void LED1_Enable(void)
{
	LED1Disable = FALSE;
}

void LED2_On(void)
{
	if(LED2Disable == TRUE)
		return;
	#if LED2_GPIO_POLARITY == LED_POLARITY_POSITIVE
	HAL_GPIO_WritePin(LED2_GPIO_PORT, LED2_GPIO_PIN, GPIO_PIN_SET);
	#else
	HAL_GPIO_WritePin(LED2_GPIO_PORT, LED2_GPIO_PIN, GPIO_PIN_RESET);
	#endif
}
void LED2_Off(void)
{
	#if LED2_GPIO_POLARITY == LED_POLARITY_POSITIVE
	HAL_GPIO_WritePin(LED2_GPIO_PORT, LED2_GPIO_PIN, GPIO_PIN_RESET);
	#else
	HAL_GPIO_WritePin(LED2_GPIO_PORT, LED2_GPIO_PIN, GPIO_PIN_SET);
	#endif
}
void LED2_Toggle(void)
{
	HAL_GPIO_TogglePin(LED2_GPIO_PORT,LED2_GPIO_PIN);
}

void LED2_Disable(void)
{
	LED2Disable = TRUE;
	LED2_Off();
}

void LED2_Enable(void)
{
	LED2Disable = FALSE;
}

void LED3_On(void)
{
	if(LED3Disable == TRUE)
		return;
	#if LED3_GPIO_POLARITY == LED_POLARITY_POSITIVE
	HAL_GPIO_WritePin(LED3_GPIO_PORT, LED3_GPIO_PIN, GPIO_PIN_SET);
	#else
	HAL_GPIO_WritePin(LED3_GPIO_PORT, LED3_GPIO_PIN, GPIO_PIN_RESET);
	#endif
}
void LED3_Off(void)
{
	#if LED3_GPIO_POLARITY == LED_POLARITY_POSITIVE
	HAL_GPIO_WritePin(LED3_GPIO_PORT, LED3_GPIO_PIN, GPIO_PIN_RESET);
	#else
	HAL_GPIO_WritePin(LED3_GPIO_PORT, LED3_GPIO_PIN, GPIO_PIN_SET);
	#endif
}
void LED3_Toggle(void)
{
	HAL_GPIO_TogglePin(LED3_GPIO_PORT,LED3_GPIO_PIN);
}
void LED3_Disable(void)
{
	LED3Disable = TRUE;
	LED3_Off();
}

void LED3_Enable(void)
{
	LED3Disable = FALSE;
} 
