/*
 * File      : CAN2.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */
#ifndef __CAN2_H
#define __CAN2_H

#include "TypeDef.h"
#include "stm32f4xx_hal.h"

extern void CAN2_HwInit(void);

extern void CAN2_Write(tdu2 ID,tdu1* data,tdu2 length);

extern void CAN2_RxCpltCallBack(CAN_HandleTypeDef *hcan);

extern void CAN2_SetRxISRFunc(tpFnGetIDBytes2 f);

#endif /* __CAN2_H */

