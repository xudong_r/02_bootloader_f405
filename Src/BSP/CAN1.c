/*
 * File      : CAN1.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#include "CAN1.h"
#include <string.h>
#include "stm32f4xx_hal.h"
#include "BSPFile.h"

//#define CAN1_BAUDRATE_666K
#define CAN1_BAUDRATE_1M


#ifdef CAN1_ENABLE

static CAN_HandleTypeDef CAN1_Handle;
static CanTxMsgTypeDef TxMessage;
static CanRxMsgTypeDef RxMessage;

static void CAN1_RCC_Init(void)
{
	__HAL_RCC_CAN1_CLK_ENABLE();
}
static void CAN1_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	//TX引脚 复用推挽输出
	GPIO_InitStructure.Pin = CAN1_TX_PIN; 
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL; 
	GPIO_InitStructure.Alternate = GPIO_AF9_CAN1;
	HAL_GPIO_Init(CAN1_TX_PORT, &GPIO_InitStructure);
	
	//RX引脚，上拉输入
	GPIO_InitStructure.Pin = CAN1_RX_PIN; 
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP; 
	GPIO_InitStructure.Pull = GPIO_NOPULL; 
	GPIO_InitStructure.Alternate = GPIO_AF9_CAN1;
	HAL_GPIO_Init(CAN1_RX_PORT, &GPIO_InitStructure);
}

static void CAN1_CAN_Init(void)
{
	CAN_FilterConfTypeDef CAN1_Filter;
	
	CAN1_Handle.Instance = CAN1;
	CAN1_Handle.pTxMsg = &TxMessage;
	CAN1_Handle.pRxMsg = &RxMessage;
#ifdef CAN1_BAUDRATE_666K
	//CAN总线波特率设置为666.66kbps
#ifdef RCC_PLL_CLK_64MHz
	//使用内部晶振,主频64MHz
	CAN1_Handle.Init.Prescaler = 3;
	CAN1_Handle.Init.BS1 = CAN_BS1_12TQ;
	CAN1_Handle.Init.BS2 = CAN_BS2_3TQ;
	CAN1_Handle.Init.SJW = CAN_SJW_2TQ;
#elif (defined(RCC_PLL_CLK_8_168MHz_CRYSTAL)||defined(RCC_PLL_CLK_8_168MHz_OSC))
	//使用外部晶振，主频168MHz
	CAN1_Handle.Init.Prescaler = 7;
	CAN1_Handle.Init.BS1 = CAN_BS1_4TQ;
	CAN1_Handle.Init.BS2 = CAN_BS2_4TQ;
	CAN1_Handle.Init.SJW = CAN_SJW_2TQ;
#else
#error "CAN1 Init Failed"
#endif
#endif

#ifdef CAN1_BAUDRATE_1M
	//CAN总线波特率设置为1000kbps
#ifdef RCC_PLL_CLK_64MHz
	//使用内部晶振,主频64MHz

#elif (defined(RCC_PLL_CLK_8_168MHz_CRYSTAL)||defined(RCC_PLL_CLK_8_168MHz_OSC))
	//使用外部晶振，主频168MHz
	CAN1_Handle.Init.Prescaler = 3;
	CAN1_Handle.Init.BS1 = CAN_BS1_7TQ;
	CAN1_Handle.Init.BS2 = CAN_BS2_6TQ;
	CAN1_Handle.Init.SJW = CAN_SJW_2TQ;
#else
#error "CAN1 Init Failed"
#endif

#endif
	CAN1_Handle.Init.Mode  =   CAN_MODE_NORMAL;
	CAN1_Handle.Init.ABOM  =   ENABLE;//使能自动离线
	CAN1_Handle.Init.AWUM  =   DISABLE;//禁能自动唤醒
	CAN1_Handle.Init.NART  =   DISABLE;//禁能自动重传
	CAN1_Handle.Init.RFLM  =   DISABLE;//新接收报文覆盖原有报文
	CAN1_Handle.Init.TTCM  =   DISABLE;//禁止时间触发通讯
	CAN1_Handle.Init.TXFP  =   DISABLE;//优先级有发送顺序决定
	
	HAL_CAN_Init(&CAN1_Handle);
	
	CAN1_Filter.FilterNumber = 0;
	CAN1_Filter.FilterMode = CAN_FILTERMODE_IDMASK;
	CAN1_Filter.FilterScale = CAN_FILTERSCALE_32BIT;
	CAN1_Filter.FilterIdHigh = 0x0000;
	CAN1_Filter.FilterIdLow = 0x0000;
	CAN1_Filter.FilterMaskIdHigh = 0x0000;
	CAN1_Filter.FilterMaskIdLow = 0x0000;
	CAN1_Filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	CAN1_Filter.FilterActivation = ENABLE;
	CAN1_Filter.BankNumber = 0;
	
	HAL_CAN_ConfigFilter(&CAN1_Handle, &CAN1_Filter);
}

static void CAN1_NVIC_Init(void)
{
	HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
	HAL_CAN_Receive_IT(&CAN1_Handle, CAN_FIFO0);
}

void CAN1_HwInit(void)
{
	CAN1_RCC_Init();
	CAN1_GPIO_Init();
	CAN1_CAN_Init();
	CAN1_NVIC_Init();
}


void CAN1_RX0_IRQHandler(void)
{
	HAL_CAN_IRQHandler(&CAN1_Handle);	
}


void CAN1_Write(tdu2 ID,tdu1* data,tdu2 length)
{
	CanTxMsgTypeDef CANTxMsg;

	CANTxMsg.StdId  =   ID;
	CANTxMsg.RTR    =   CAN_RTR_DATA;
	CANTxMsg.ExtId  =   0;
	CANTxMsg.IDE    =   CAN_ID_STD;
	CANTxMsg.DLC    =   length;

	memcpy((tdu1 *)CANTxMsg.Data, (tdu1 *)data, length);

	CAN1_Handle.pTxMsg = &CANTxMsg;
	
	HAL_CAN_Transmit(&CAN1_Handle, 10);
}



static tpFnGetIDBytes2 fpCAN1RxISRFunc = NULL;

void CAN1_SetRxISRFunc(tpFnGetIDBytes2 f)
{
	fpCAN1RxISRFunc = f;
}
void CAN1_RxCpltCallBack(CAN_HandleTypeDef *hcan)
{
	//CAN1接收数据指针非空
	if (fpCAN1RxISRFunc != NULL)
	{
		fpCAN1RxISRFunc(hcan->pRxMsg->StdId, hcan->pRxMsg->Data, hcan->pRxMsg->DLC);
	}
	//开启中断继续接收
	if(HAL_BUSY == HAL_CAN_Receive_IT(hcan, CAN_FIFO0))
	{
		__HAL_CAN_ENABLE_IT(hcan, CAN_IT_FOV0 | CAN_IT_FMP0);
	}
}


#endif // end of (#ifedef CAN1_ENABLE)
