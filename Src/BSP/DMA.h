/*
 * File      : DMA.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     Ren Xudong   first implementation
 */

#ifndef __DMA_H
#define __DMA_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "ISRHandler.h"
#include "stm32f4xx_hal.h"


extern DMA_HandleTypeDef* DMA_GetChannelHandle(tdu1 dmaIndex,tdu1 chIndex);

extern void DMA_Init(tdu1 dmaIndex,tdu1 chIndex);


#ifdef __cplusplus
}
#endif
#endif /* __DMA_H */

