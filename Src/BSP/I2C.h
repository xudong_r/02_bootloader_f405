/*
 * File      : I2C.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

// 接口不完整

#ifndef __I2C_H
#define __I2C_H

#ifdef __cplusplus
extern "C"
{
#endif
#include "TypeDef.h"

extern void I2C_HwInit(tdu1 ch,tdu4 clockSpeed);


#ifdef __cplusplus
}
#endif
#endif /* __I2C_H */

