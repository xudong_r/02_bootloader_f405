#ifndef __CAN_H
#define __CAN_H

#include "TypeDef.h"


extern void CAN_HwInit(tdu1 canIndex);

extern void CAN_Write(tdu1 canIndex,tdu2 ID,tdu1* data,tdu1 length);

extern void CAN_SetRxISRFunc(tdu1 ch,tpFnGetIDBytes2 fp);

#endif

