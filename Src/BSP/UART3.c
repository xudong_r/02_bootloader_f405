/*
 * File       : UART3.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */


#include "Uart3.h"
#include <stdio.h>
#include <string.h>
#include "../MacroLib/MemApply.h"
#include "BSPFile.h"

#ifdef OS_ENABLE
#include "OSPort.h"
#endif /* OS_ENABLE */

#ifdef UART3_ENABLE

static UART_HandleTypeDef Uart3Handle;

// 内部函数声明
static void Uart3_PrepareRecData(void);
static void Uart3_TxQueueInit(tdu2 length);

UART_HandleTypeDef* Uart3_GetHandle(void)
{
	return &Uart3Handle;
}

static void Uart3_Rcc_HwInit(void)
{
	__HAL_RCC_USART3_CLK_ENABLE();
}

static void Uart3_Gpio_HwInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.Pin = UART3_TX_GPIO_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Alternate = GPIO_AF7_USART3;	
	HAL_GPIO_Init(UART3_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = UART3_RX_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Alternate = GPIO_AF7_USART3;
	HAL_GPIO_Init(UART3_RX_GPIO_PORT, &GPIO_InitStructure);
}

static void Uart3_Uart_HwInit(tdu4 baudrate,char parity,tdf4 stopBits)
{
	Uart3Handle.Instance = USART3;

	HAL_UART_DeInit(&Uart3Handle);

	Uart3Handle.Init.BaudRate = baudrate;
	Uart3Handle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	Uart3Handle.Init.OverSampling = UART_OVERSAMPLING_16;
	Uart3Handle.Init.Mode = UART_MODE_TX_RX;

	if (stopBits == 2.0f)
	{
		Uart3Handle.Init.WordLength = UART_WORDLENGTH_9B;
	}
	else
	{
		Uart3Handle.Init.WordLength = UART_WORDLENGTH_8B;
	}

	if(stopBits == 2.0f)
		Uart3Handle.Init.StopBits = UART_STOPBITS_2;
	else//1.0f
		Uart3Handle.Init.StopBits = UART_STOPBITS_1;

	if(parity == 'O' || parity == 'o')
		Uart3Handle.Init.Parity = UART_PARITY_ODD;
	else if(parity == 'E' || parity == 'e')
		Uart3Handle.Init.Parity = UART_PARITY_EVEN;
	else//(parity == 'n' || parity == 'N')
		Uart3Handle.Init.Parity = UART_PARITY_NONE;

	HAL_UART_Init(&Uart3Handle);

}

static void Uart3_Nvic_HwInit(void)
{
	HAL_NVIC_SetPriority(USART3_IRQn, UART3_PRIPRIO,UART3_SUBPRIO);
	HAL_NVIC_EnableIRQ(USART3_IRQn);
}

void Uart3_HwInit(tdu4 baudrate,char parity,tdf4 stopBits,tdu2 bufferLength)
{
	Uart3_Rcc_HwInit();
	Uart3_Gpio_HwInit();
	Uart3_Nvic_HwInit();
	Uart3_Uart_HwInit(baudrate, parity, stopBits);
	__HAL_UART_ENABLE(&Uart3Handle);

	// 初始化发送队列
	Uart3_TxQueueInit(bufferLength);
	// 开启接收功能
	Uart3_PrepareRecData();
}


void Uart3_SetBaudrate(tdu4 baudrate)
{
	if (Uart3Handle.Instance == USART3)
	{
		Uart3Handle.Init.BaudRate = baudrate;
		HAL_UART_Init(&Uart3Handle);
	}
}


tdbl Uart3_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum)
{
	HAL_StatusTypeDef res;

	res = HAL_UART_Transmit_IT(&Uart3Handle,pBytes,byteNum);

	if (res == HAL_OK)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static tdu1 Uart3RxByte;
void USART3_IRQHandler(void)
{
#ifdef OS_ENABLE
	OSPort_Interrupt_Enter();
#endif /* OS_ENABLE */
	HAL_UART_IRQHandler(&Uart3Handle);
	if((__HAL_UART_GET_FLAG(&Uart3Handle, UART_FLAG_ORE) != RESET))
	{
		HAL_UART_Receive_IT(&Uart3Handle, &Uart3RxByte,1);
	}
#ifdef OS_ENABLE
	OSPort_Interrupt_Leave();
#endif /* OS_ENABLE */
}

/****** 以下代码与硬件平台无关 ******/

static tQueue Uart3TxQueue;
static tdu1* pUart3TxBuffer;
void Uart3_TxQueueInit(tdu2 length)
{
	Queue_Init(&Uart3TxQueue, 2*length);
	pUart3TxBuffer = (tdu1 *)MemApply(length);
	memset((void*)pUart3TxBuffer,0,length);
}
// 从队列中取出全部数据
tdu4 Uart3_TxAllDequeue(byte *data)
{

    return (Queue_PopBytes(&Uart3TxQueue, data, Uart3TxQueue.Count)); 

}


tdbl Uart3_WriteBytes(byte *pByte, U16 byteNum)
{
	tdbl res;
    if (byteNum==0) 
	{ 
        // 发送长度为0
        return FALSE;
    }

	// 发送字节
	res = Uart3_WriteBytes_IT(pByte,byteNum);
	if (res == FALSE)
	{
		//·发送失败,将数据放入队列
		Queue_Enqueue(&Uart3TxQueue,pByte,byteNum);

	}
	return TRUE;
}

tpFnWrite Uart3_GetWriteHandler(void)
{
	return Uart3_WriteBytes;
}


void Uart3_TxCpltCallBack(void)
{
    // 从队列中取出全部数据
    tdu4 byteNum;
    byteNum = Uart3_TxAllDequeue(pUart3TxBuffer);
    if (byteNum!=0) {
		// 将数据放送出去
		Uart3_WriteBytes_IT(pUart3TxBuffer, byteNum);
    }
}



static tpfnUartRxISRFunc fpUartRxISRFunc = NULL;

void Uart3_SetRxISRFunc(tpfnUartRxISRFunc fp)
{
	fpUartRxISRFunc = fp;
}

void Uart3_RxCpltCallBack(void)
{
	// 接收数据
	if (fpUartRxISRFunc != NULL)
	{
		fpUartRxISRFunc(Uart3RxByte);
	}
	// 开启中断继续接收
	HAL_UART_Receive_IT(&Uart3Handle, &Uart3RxByte,1);
}
void Uart3_ErrorCpltCallBack(void)
{
	// 开启中断继续接收
	HAL_UART_Receive_IT(&Uart3Handle, &Uart3RxByte,1);
}

static void Uart3_PrepareRecData(void)
{
	HAL_UART_Receive_IT(&Uart3Handle, &Uart3RxByte,1);
}

#endif //end of (#ifdef UART3_ENABLE)
