/*
 * File      : I2C1.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __I2C1_H
#define __I2C1_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "TypeDef.h"
#include "stm32f4xx_hal.h"
extern void I2C1_HwInit(tdu4 clockSpeed);

extern void I2C1_WriteByte(tdu2 devAddr,tdu1* pByte,tdu2 byteNum);

extern I2C_HandleTypeDef *I2C1_GetHandle(void);

extern void I2C1_MspInit(void);	
#ifdef __cplusplus
}
#endif
#endif /* __I2C1_H */

