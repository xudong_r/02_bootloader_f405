/*
 * File      : SysTick.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "SysTick.h"
#include "BSPFile.h"
#include "stm32f4xx_hal.h"
#ifdef RCC_PLL_CLK_8_168MHz_OSC
#define    SYS_HCLK_FREQ    (168000000L)
#endif

#ifdef RCC_PLL_CLK_8_168MHz_CRYSTAL
#define    SYS_HCLK_FREQ    (168000000L)
#endif

#ifdef RCC_PLL_CLK_64MHz
#define    SYS_HCLK_FREQ    (64000000L)
#endif


void SysTick_Init(tdu4 sysFre)
{
    SysTick->LOAD = (tdu4)((SYS_HCLK_FREQ)/sysFre - 1);
    SysTick->VAL = 0UL;
    SysTick->CTRL |= (1<<2);//AHB做为时钟源
    SysTick->CTRL |= (1<<1);//使能SysTick中断

    //设置SysTick中断优先级为最高
}

void SysTick_Start()
{
    SysTick->CTRL |= (1<<0);//使能SysTick
}

void SysTick_Stop()
{
    SysTick->CTRL &= ~(1<<0);//禁能SysTick
}

