/*
 * File      : Timer.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "Timer.h"
#include "stm32f4xx_Hal.h"
#include "BSPFile.h"

#ifdef TIM1_ENABLE
#include "Timer1.h"
#endif

#ifdef TIM2_ENABLE
#include "Timer2.h"
#endif

#ifdef TIM3_ENABLE
#include "Timer3.h"
#endif

#ifdef TIM4_ENABLE
#include "Timer4.h"
#endif

#ifdef TIM5_ENABLE
#include "Timer5.h"
#endif

#ifdef TIM6_ENABLE
#include "Timer6.h"
#endif

#ifdef TIM7_ENABLE
#include "Timer7.h"
#endif

#ifdef TIM8_ENABLE
#include "Timer8.h"
#endif


tdbl Timer_HwInit(tdu1 ch, tdu4 frequency,tdu4 period,tdu2 dutyCycle)
{
#ifdef TIM1_ENABLE
	if(ch == 1)
	{
		Timer1_Base_HwInit(frequency,period,dutyCycle);
	}
#endif

#ifdef TIM2_ENABLE
	if(ch == 2)
	{
		Timer2_Base_HwInit(frequency,period,dutyCycle);
	}
#endif

#ifdef TIM3_ENALBE
	if (ch == 3)
	{
		Timer3_Base_HwInit(frequency,period,dutyCycle);
	}
#endif

#ifdef TIM4_ENABLE
	if (ch == 4)
	{
		Timer4_Base_HwInit(frequency,period,dutyCycle);
	}
#endif

#ifdef TIM5_ENABLE
	if (ch == 5)
	{
		Timer5_Base_HwInit(frequency,period,dutyCycle);
	}
#endif

#ifdef TIM8_ENABLE
	if (ch == 8)
	{
		Timer8_Base_HwInit(frequency,period,dutyCycle);
	}
#endif
	return FALSE;
}



void Timer_Start(tdu1 ch)
{
#ifdef TIM1_ENABLE
	if (ch == 1)
	{
		Timer1_Start();
	}
#endif

#ifdef TIM2_ENABLE
	if (ch == 2)
	{
		Timer2_Start();
	}
#endif

#ifdef TIM3_ENABLE
	if (ch == 3)
	{
		Timer3_Start();
	}
#endif

#ifdef TIM4_ENABLE
	if (ch == 4)
	{
		Timer4_Start();
	}
#endif

}
void Timer_Stop_IT(tdu1 ch)
{
	
}
void Timer_Start_IT(tdu1 ch)
{
#ifdef TIM1_ENABLE
	if (ch == 1)
	{
		Timer1_Start_IT();
	}
#endif

#ifdef TIM2_ENABLE
	if (ch == 2)
	{
		Timer2_Start_IT();
	}
#endif

#ifdef TIM3_ENABLE
	if (ch == 3)
	{
		Timer3_Start_IT();
	}
#endif

#ifdef TIM4_ENABLE
	if (ch == 4)
	{
		Timer4_Start_IT();
	}
#endif
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
#ifdef TIM1_ENABLE
	if (htim == Timer1_GetHandle())
	{
		Timer1_PeriodElapsedCallback();
	}
#endif

#ifdef TIM2_ENABLE
	if (htim == Timer2_GetHandle())
	{
		Timer2_PeriodElapsedCallback();
	}
#endif

#ifdef TIM3_ENABLE
	if (htim == Timer3_GetHandle())
	{
		Timer3_PeriodElapsedCallback();
	}
#endif

#ifdef TIM4_ENABLE
	if (htim == Timer4_GetHandle())
	{
		Timer4_PeriodElapsedCallback();
	}
#endif
}
void Timer_SetElapsedCallback(tdu1 ch,tpfnTimerElapsedCallback fp)
{
#ifdef TIM1_ENABLE
	if (ch == 1)
	{
		Timer1_SetElapsedCallback(fp);
	}
#endif

#ifdef TIM2_ENABLE
	if (ch == 2)
	{
		Timer2_SetElapsedCallback(fp);
	}
#endif

#ifdef TIM3_ENABLE
	if (ch == 3)
	{
		Timer3_SetElapsedCallback(fp);
	}
#endif

#ifdef TIM4_ENABLE
	if (ch == 4)
	{
		Timer4_SetElapsedCallback(fp);
	}
#endif
}
