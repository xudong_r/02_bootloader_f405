/*
 * File       : UART2.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */


#include "Uart2.h"
#include <stdio.h>
#include <string.h>
#include "../MacroLib/MemApply.h"
#include "BSPFile.h"

#ifdef OS_ENABLE
#include "OSPort.h"
#endif /* OS_ENABLE */

#ifdef UART2_ENABLE

static UART_HandleTypeDef Uart2Handle;

// 内部函数声明
static void Uart2_PrepareRecData(void);
static void Uart2_TxQueueInit(tdu2 length);

UART_HandleTypeDef* Uart2_GetHandle(void)
{
	return &Uart2Handle;
}

static void Uart2_Rcc_HwInit(void)
{
	__HAL_RCC_USART2_CLK_ENABLE();
}

static void Uart2_Gpio_HwInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.Pin = UART2_TX_GPIO_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Alternate = GPIO_AF7_USART2;	
	HAL_GPIO_Init(UART2_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = UART2_RX_GPIO_PIN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Alternate = GPIO_AF7_USART2;
	HAL_GPIO_Init(UART2_RX_GPIO_PORT, &GPIO_InitStructure);
}

static void Uart2_Uart_HwInit(tdu4 baudrate,char parity,tdf4 stopBits)
{
	Uart2Handle.Instance = USART2;

	HAL_UART_DeInit(&Uart2Handle);

	Uart2Handle.Init.BaudRate = baudrate;
	Uart2Handle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	Uart2Handle.Init.OverSampling = UART_OVERSAMPLING_16;
	Uart2Handle.Init.Mode = UART_MODE_TX_RX;

	if (stopBits == 2.0f)
	{
		Uart2Handle.Init.WordLength = UART_WORDLENGTH_9B;
	}
	else
	{
		Uart2Handle.Init.WordLength = UART_WORDLENGTH_8B;
	}

	if(stopBits == 2.0f)
		Uart2Handle.Init.StopBits = UART_STOPBITS_2;
	else//1.0f
		Uart2Handle.Init.StopBits = UART_STOPBITS_1;

	if(parity == 'O' || parity == 'o')
		Uart2Handle.Init.Parity = UART_PARITY_ODD;
	else if(parity == 'E' || parity == 'e')
		Uart2Handle.Init.Parity = UART_PARITY_EVEN;
	else//(parity == 'n' || parity == 'N')
		Uart2Handle.Init.Parity = UART_PARITY_NONE;

	HAL_UART_Init(&Uart2Handle);

}

static void Uart2_Nvic_HwInit(void)
{
	HAL_NVIC_SetPriority(USART2_IRQn, UART2_PRIPRIO,UART2_SUBPRIO);
	HAL_NVIC_EnableIRQ(USART2_IRQn);
}

void Uart2_HwInit(tdu4 baudrate,char parity,tdf4 stopBits,tdu2 bufferLength)
{
	Uart2_Rcc_HwInit();
	Uart2_Gpio_HwInit();
	Uart2_Nvic_HwInit();
	Uart2_Uart_HwInit(baudrate, parity, stopBits);
	__HAL_UART_ENABLE(&Uart2Handle);

	// 初始化发送队列
	Uart2_TxQueueInit(bufferLength);
	// 开启接收功能
	Uart2_PrepareRecData();
}


void Uart2_SetBaudrate(tdu4 baudrate)
{
	if (Uart2Handle.Instance == USART2)
	{
		Uart2Handle.Init.BaudRate = baudrate;
		HAL_UART_Init(&Uart2Handle);
	}
}


tdbl Uart2_WriteBytes_IT(tdu1* pBytes,tdu2 byteNum)
{
	HAL_StatusTypeDef res;

	res = HAL_UART_Transmit_IT(&Uart2Handle,pBytes,byteNum);

	if (res == HAL_OK)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static tdu1 Uart2RxByte;
void USART2_IRQHandler(void)
{
#ifdef OS_ENABLE
	OSPort_Interrupt_Enter();
#endif /* OS_ENABLE */
	HAL_UART_IRQHandler(&Uart2Handle);
	if((__HAL_UART_GET_FLAG(&Uart2Handle, UART_FLAG_ORE) != RESET))
	{
		HAL_UART_Receive_IT(&Uart2Handle, &Uart2RxByte,1);
	}
#ifdef OS_ENABLE
	OSPort_Interrupt_Leave();
#endif /* OS_ENABLE */
}

/****** 以下代码与硬件平台无关 ******/

static tQueue Uart2TxQueue;
static tdu1* pUart2TxBuffer;
void Uart2_TxQueueInit(tdu2 length)
{
	Queue_Init(&Uart2TxQueue, 2*length);
	pUart2TxBuffer = (tdu1 *)MemApply(length);
	memset((void*)pUart2TxBuffer,0,length);
}
// 从队列中取出全部数据
tdu4 Uart2_TxAllDequeue(byte *data)
{

    return (Queue_PopBytes(&Uart2TxQueue, data, Uart2TxQueue.Count)); 

}


tdbl Uart2_WriteBytes(byte *pByte, U16 byteNum)
{
	tdbl res;
    if (byteNum==0) 
	{ 
        // 发送长度为0
        return FALSE;
    }

	//·发送字节
	res = Uart2_WriteBytes_IT(pByte,byteNum);
	if (res == FALSE)
	{
		//·发送失败,将数据放入队列
		Queue_Enqueue(&Uart2TxQueue,pByte,byteNum);

	}
	return TRUE;
}

tpFnWrite Uart2_GetWriteHandler(void)
{
	return Uart2_WriteBytes;
}


void Uart2_TxCpltCallBack(void)
{
    // 从队列中取出全部数据
    tdu4 byteNum;
    byteNum = Uart2_TxAllDequeue(pUart2TxBuffer);
    if (byteNum!=0) {
		// 将数据放送出去
		Uart2_WriteBytes_IT(pUart2TxBuffer, byteNum);
    }
}



static tpfnUartRxISRFunc fpUartRxISRFunc = NULL;

void Uart2_SetRxISRFunc(tpfnUartRxISRFunc fp)
{
	fpUartRxISRFunc = fp;
}

void Uart2_RxCpltCallBack(void)
{
	// 接收数据
	if (fpUartRxISRFunc != NULL)
	{
		fpUartRxISRFunc(Uart2RxByte);
	}
	// 开启中断继续接收
	HAL_UART_Receive_IT(&Uart2Handle, &Uart2RxByte,1);
}
void Uart2_ErrorCpltCallBack(void)
{
	// 开启中断继续接收
	HAL_UART_Receive_IT(&Uart2Handle, &Uart2RxByte,1);
}

static void Uart2_PrepareRecData(void)
{
	HAL_UART_Receive_IT(&Uart2Handle, &Uart2RxByte,1);
}

#endif //end of (#ifdef UART2_ENABLE)
