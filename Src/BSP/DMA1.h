/*
 * File      : DMA1.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __DMA1_H
#define __DMA1_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "TypeDef.h"
#include "stm32f4xx_hal.h"

extern DMA_HandleTypeDef* DMA1_GetChannelHandle(tdu1 chIndex);

extern void DMA1_Init(tdu1 chIndex);

#ifdef __cplusplus
}
#endif
#endif /* __DMA1_H */

