/*
 * File       : OsPort.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "OsPort.h"
#include "../Bsp/SysTick.h"
#include "stm32f4xx_hal.h"

static tpfnTimerElapsedCallback fpTimerElapsedCallback = NULL;
/**
 * 
 * @brief OS时钟接口初始化，初始化SysTick定时器
 * 
 * @param tickFre 定时器频率
 */
void OsPort_Init(tdu4 tickFre)
{
    //初始化Systic  Timer
    SysTick_Init(tickFre);
}
void OsPort_Start()
{
    SysTick_Start();
}
void OsPort_Stop()
{
    SysTick_Stop();
}
void OsPort_RegisterTickFunc(tpfnTimerElapsedCallback fp)
{
    fpTimerElapsedCallback = fp;
}

/**
 * 
 * @brief Systick定时器中断处理函数
 */
void SysTick_Handler(void)
{

    if (fpTimerElapsedCallback != NULL)
    {
        fpTimerElapsedCallback();
    }

    HAL_IncTick();

}
