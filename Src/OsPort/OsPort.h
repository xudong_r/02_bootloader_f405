/*
 * File       : OsPort.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __OSPORT_H
#define __OSPORT_H

#ifdef __cplusplus
extern "C"
{
#endif
#include "TypeDef.h"

typedef void (*tpfnTimerElapsedCallback)(void);
    
extern void OsPort_Init(tdu4 tickFre);
extern void OsPort_Start(void);
extern void OsPort_Stop(void);
extern void OsPort_RegisterTickFunc(tpfnTimerElapsedCallback fp);
#ifdef __cplusplus
}
#endif
#endif /* __OSPORT_H */

