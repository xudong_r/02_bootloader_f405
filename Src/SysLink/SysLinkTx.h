/*
 * File       : SysLinTx.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */


#ifndef __SYS_LINK_TX_H
#define __SYS_LINK_TX_H


#include "TypeDef.h"

extern void SysLinkTx_SetWriteHandler(tpFnWrite pf);

extern tdbl SysLinkTx_PostData(tdu1 ID, tdu1 *data, tdu2 length);

extern void SysLinkTx_Process(void);

#endif/* __SYS_LINK_TX_H */

