/*
 * File       : SysLinkRx.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */


#ifndef __SYS_LINK_RX_H
#define __SYS_LINK_RX_H


#ifdef __cplusplus
extern "C" {
#endif

#include "TypeDef.h"

#define XDLINK_ENTER_BOOTLOADER		(0xE1)
#define XDLINK_DEVICE_VERSION		(0xE2)
#define XDLINK_DEVICE_BUILD_TIME	(0xE3)
#define XDLINK_DEVICE_SN			(0xE4)
#define XDLINK_ACCREDIT				(0xE5)

#define BLFRM_CMD_DWPARA 			(0x21)  //下载APP程序的配置参数
#define BLFRM_CMD_RDPARA 			(0x22)  //读取APP程序的配置信息
#define BLFRM_CMD_DWBLPA 			(0x23)  //下载Bootloader程序的参数
#define BLFRM_CMD_RDBLPA 			(0x24)  //读取Bootloader程序的参数
#define BLFRM_CMD_DWPRG  			(0x31)  //下载APP程序文件
#define BLFRM_CMD_DWBL   			(0x32)  //下载BootLoader程序文件
#define BLFRM_CMD_RUNAPP 			(0x50)  //运行APP程序
#define BLFRM_CMD_ERASE  			(0x60)  //擦除Flash

extern void SysLinkRx_Init(void);

extern void SysLinkRx_RawDataRx(tdu1 data);

extern void SysLinkRx_Process(void);


#ifdef __cplusplus
}
#endif

#endif /* __SYS_LINK_RX_H */
