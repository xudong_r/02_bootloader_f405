/*
 * File       : SysLinkRx.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "SysLinkRx.h"
#include "../CommProtocol/XDLinkRx.h"
#include "../CommProtocol/XDLinkDDS.h"
#include <string.h>
#include "../include/TypeDef.h"
#include "../include/MacroDef.h"


#include "../MacroLib/Accredit.h"
#include "Version.h"

#include "../Bsp/Uart.h"
#include "../SysLink/SysLinkTx.h"

#include "../MacroLib/IPA.h"
#include "../Task/BLTask.h"
#include "../MacroLib/AES.h"
#include "../Task/BLConfig.h"

#ifdef BOOTLOADER_ENABLE
#include "../BSP/BootLoader.h"
#endif

#include "CRC.h"


static tXDLinkRxState RxState;
static tXDLinkDDS DDS;



static void OnFrameReceived(tdu1 ID, tdu1 *data, tdu2 length)
{
	DDS.ThisID = ID;
	DDS.ThisLength = length;
	DDS.ThisData = data;
	XDLinkDDS_Process(&DDS);
}

void SysLinkRx_Process(void)
{
	if(RxState.RxMsgUpdate == TRUE)
	{
		XDLinkRx_Process(OnFrameReceived, &RxState);
	}
}



void SysLinkRx_RawDataRx(tdu1 data)
{
	XDLinkRx_RawData(&RxState, data);
}



static void EnterBootLoader(tdu1 ID,tdu1 *data,tdu2 length)
{
#ifdef BOOTLOADER_ENABLE

	JumpToBootLoader();
#endif
}

static void GetDeviceVersion(tdu1 ID,tdu1 *data,tdu2 length)
{
	tdu1 tempBuffer[4];
	tVersion* pVersion;
	if (length == 1)
	{
		pVersion = Version_Get();
		memcpy((void *)&tempBuffer[0], (void *)pVersion,4);
		SysLinkTx_PostData(ID,&tempBuffer[0],4);
	}
}

static void GetDeviceBuildTime(tdu1 ID,tdu1* data,tdu2 length)
{
	tBuildTime *pBuildTime;
	tdu1 tempBuffer[7];
	if (length == 1)
	{
		pBuildTime = Version_GetBuildTime();
		memcpy((void *)&(tempBuffer[0]), (void *)pBuildTime,7);
		SysLinkTx_PostData(ID,&tempBuffer[0],7);
	}
}
static void GetDeviceSN(tdu1 ID,tdu1 *data,tdu2 length)
{
	tdu1 tempBuffer[18];
	if (length == 1)
	{
		tempBuffer[0] = VERSION_SYS_TYPE;
		tempBuffer[1] = FALSE;
		memcpy((void *)&tempBuffer[2], (void *)Accredit_GetSN(), 16);
		SysLinkTx_PostData(ID,&tempBuffer[0],18);
	}
}

static void DeviceAccredit(tdu1 ID,tdu1 *data,tdu2 length)
{
    tdbl res;
	if (length == 17)
	{
		//验证设备ID号
		if (*data == VERSION_SYS_TYPE)
		{
			Accredit_SetExKey(data+1);
			res = Accredit_VerifyKey();
		}
		else
		{
			res = FALSE;
		}
		SysLinkTx_PostData(ID,(tdu1*)&res,1);
	}
	if (res == TRUE)
	{
		//授权成功 写入授权信息
//		ParaLog_Save(DR_SECTION_1);
    }

}

static void GetAppInfo(tdu1 ID,tdu1 *data,tdu2 length)
{
	//返回APP信息
	SysLinkTx_PostData(ID, (void *)BL_APPLICATION_INFO_ADDR, sizeof(tProgramMessage));
}

static tdu2 PackNum = 0;
static tProgramMessage ProgramMessage;
static void SetAppInfo(tdu1 ID,tdu1 *data,tdu2 length)
{
	tdu2 res = 0xFF;
	
	AES_SetKey((tdc1 *)&data[0], 16);
	AES_Decrypt((tdc1 *)&data[16],length - 16);
	res  = sizeof(tProgramMessage);
	
	// 先拷贝数据到结构体,用于对比CRC
	memcpy((void*)&ProgramMessage, (void*)&data[16], sizeof(tProgramMessage));
	// 校验tProgramMessage结构体里面的数据
	res =  CRC_16(&data[16], sizeof(tProgramMessage) - 2);
	
	if(res != ProgramMessage.CRC16)
	{
		// 参数校验错误,清除信息结构体
		memset((void*)&ProgramMessage, 0, sizeof(tProgramMessage));
		data[2] = 0x04;
		
		SysLinkTx_PostData(BLFRM_CMD_DWPRG, data, 3);
		return;
	}
		
	res = FLASH_WritePage(BL_APPLICATION_INFO_SECTOR_INDEX, (void*)&data[16], length);
		
	if(res != 0x00)
	{
		// 写入APP参数失败
		data[2] = 0x03;
		
		SysLinkTx_PostData(BLFRM_CMD_DWPRG,data,3);
		return;
	}
	
	PackNum = (ProgramMessage.ProgramSize % 128) ? (ProgramMessage.ProgramSize / 128 + 1) : (ProgramMessage.ProgramSize / 128);
	
	if(IPA_EraserFlash() != 0)
	{
		// 擦除失败
		data[2] = 0x02;
	}
	else
	{
		// 擦除成功,请求第一包数据,此处复用了写入错误指令
		data[0] = 0;
		data[1] = 0;
		data[2] = 0x01;
		// 开始写入,设置新的密钥
		AES_SetKey((tdc1 *)&ProgramMessage.ProgramAESKey[0], 16);
	}

	SysLinkTx_PostData(BLFRM_CMD_DWPRG,data,3);	
	
}

static tdu2 WriteTimes = 0;			// 调试参数
static tdu1 __attribute__ ((aligned (4)))  ProgRamBuffer[256] = {0};
static void DownLoadAppProgram(tdu1 ID,tdu1 *data,tdu2 length)
{
	tdu1 res = 0;
	tdu4 packID = 0;
	tdu4 writeAddr = 0;
	static tdu2 programLength = 0;
	static tdu2 lastPackID = 0xFFFF;

	packID = *(tdu2*)data;
	if(packID > PackNum - 1)
		return;
	if(packID == lastPackID)
	{//存在问题
		//接收到的数据包为上次已经收到的数据包
		data[2] = 0;
		SysLinkTx_PostData(ID,data,3);
		return;
	}
	
	//解密
	if(AES_Decrypt((tdc1*)&data[2], (tds4)length - 2) == 0)
	{
		//判断数据包大小
		if((length - 2) < 129)
		{
			if((packID % 2) == 0)
				memcpy((void *)(&ProgRamBuffer[0]), (void *)(data + 2), length - 2);
			else
				memcpy((void *)(&ProgRamBuffer[128]), (void *)(data + 2), length - 2);
			programLength += length - 2;
		}
		
		//判断是否为偶数包，或最后一包
		if((packID == (PackNum - 1)) || (packID % 2 == 1))
		{
			//写入Flash
			writeAddr = BL_APPLICATION_START_ADDR + (packID * 128 / 256) * 256;
			res = IPA_WriteAppToFlash(&writeAddr, (tdu4*)ProgRamBuffer, programLength / 4);
			WriteTimes++;
			programLength = 0;
			if(res)
				data[2] = 1;	//写入错误
			else
			{
				data[2] = 0;	//成功写入
				if(packID == (PackNum - 1))
				{
					// 最后一包写入成功,清除升级标志位
					UPLOADFLAG = 0xFFFFFFFFU;
				}
					
			}
		}
		else
			data[2] = 0;	//成功接收
	}
	else
		data[2] = 1;	//写入错误

	SysLinkTx_PostData(ID,data,3);
	
} 


static void RunApp(tdu1 ID,tdu1 *data,tdu2 length)
{
	BLTask_JumptoApp();
}

static void EraserFlash(tdu1 ID,tdu1 *data,tdu2 length)
{
	IPA_EraserFlash();
}

void SysLinkRx_Init(void)
{
	memset((void *)&RxState, 0x00, sizeof(tXDLinkRxState));
	RxState.Header[0] = 0xEB;
	RxState.Header[1] = 0x90;
	 
	XDLinkDDS_Register(XDLINK_ENTER_BOOTLOADER,EnterBootLoader, &DDS);
	XDLinkDDS_Register(XDLINK_DEVICE_VERSION,GetDeviceVersion, &DDS);
	XDLinkDDS_Register(XDLINK_DEVICE_BUILD_TIME,GetDeviceBuildTime, &DDS);
	XDLinkDDS_Register(XDLINK_DEVICE_SN,GetDeviceSN, &DDS);
	XDLinkDDS_Register(XDLINK_ACCREDIT,DeviceAccredit, &DDS);

	XDLinkDDS_Register(BLFRM_CMD_RDPARA, GetAppInfo, &DDS);
	XDLinkDDS_Register(BLFRM_CMD_DWPARA, SetAppInfo, &DDS);
	XDLinkDDS_Register(BLFRM_CMD_DWPRG, DownLoadAppProgram, &DDS);
	XDLinkDDS_Register(BLFRM_CMD_RUNAPP, RunApp, &DDS);
	XDLinkDDS_Register(BLFRM_CMD_ERASE, EraserFlash, &DDS);
}

