/*
 * File       : SysLinTx.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#include "../SysLink/SysLinkTx.h"
#include "../CommProtocol/XDLinkTx.h"
#include "string.h"

#define SYS_MSG_DATA_MAX_LENGTH					(128)
#define SYS_PACKAGE_NUM							(10)


__packed typedef struct
{
	tdbl Valid;
	tdu1 ID;
	tdu1 Buf[SYS_MSG_DATA_MAX_LENGTH];
	tdu2 Length;
}tSysPackage;

static tSysPackage SysPackage[SYS_PACKAGE_NUM];
static tdbl SysPackageInit = FALSE;


static tdu1 SysFrameStream[128 + SYS_MSG_DATA_MAX_LENGTH];

static tpFnWrite FnWrite = NULL;
void SysLinkTx_SetWriteHandler(tpFnWrite pf)
{
	FnWrite = pf;
}

tdbl SysLinkTx_PostData(tdu1 ID, tdu1 *data, tdu2 length)
{
	tdu2 i;
	if(SysPackageInit == FALSE)
	{
		for(i = 0; i < SYS_PACKAGE_NUM; i++)
			SysPackage[i].Valid = FALSE;
		SysPackageInit = TRUE;
	}
	if(length > SYS_MSG_DATA_MAX_LENGTH)
		return FALSE;
	for(i=0; i < SYS_PACKAGE_NUM; i++)
	{
		if(SysPackage[i].Valid == FALSE)
		{
			SysPackage[i].ID = ID;
			SysPackage[i].Length = length;
			memcpy((void*)SysPackage[i].Buf, (void*)data, length);
			SysPackage[i].Valid = TRUE;

			return TRUE;
		}
	}
	return FALSE;
}

void SysLinkTx_Process(void)
{
	tdu2 length = 0;
	tdbl packageValid = FALSE;
	tdu2 i;
	tdbl result = FALSE;
	if(FnWrite != NULL)
	{
		for(i=0; i < SYS_PACKAGE_NUM;i++)
		{
			if(SysPackage[i].Valid == TRUE)
			{	
				packageValid = TRUE;
				break;
			}
		}
		if(packageValid == FALSE)
			return;
		length = XDLinkTx_FormatHeader(SysFrameStream, sizeof(SysFrameStream), 0xEB, 0x90);	// 添加帧头
		
		
		if(packageValid == TRUE)	// 添加其他指令数据
		{
			for(i=0;i<SYS_PACKAGE_NUM;i++)
			{
				if(SysPackage[i].Valid == TRUE)
				{	
					SysPackage[i].Valid = FALSE;
					result = XDLinkTx_AddPackage(SysFrameStream, sizeof(SysFrameStream), &length,
												 SysPackage[i].ID, SysPackage[i].Buf, SysPackage[i].Length);
					if(result != TRUE)
						break;
				}
			}
			
		}		
		length = XDLinkTx_AddCRC(SysFrameStream, sizeof(SysFrameStream));
		if(length != 0)
			FnWrite(SysFrameStream, length);
	}
}
