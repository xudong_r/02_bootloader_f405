/*
 * File      : MaroDef.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef __MACRO_DEF_H
#define __MACRO_DEF_H


#include "TypeDef.h"



#define S8ToS32(x,y,z,v)	(Int32)(((((Int32)x)<<24)|(((Int32)y)<<16)|(((Int32)z)<<8)|((Int32)v))& 0xffffffff)
#define S8ToS16(x,y)		(Int16)(((((Int16)x)<<8)|((Int16)y))&0xffff)
#define S8ToU16(x,y)		(Uint16)(((((Uint16)x)<<8)|((Uint16)y))&0xffff)

#define U8ToU32(x,y,z,v)	(Uint32)(((((Uint32)x)<<24)|(((Uint32)y)<<16)|(((Uint32)z)<<8)|((Uint32)v))& 0xffffffff)
#define U8ToS32(x,y,z,v)	(Int32)(((((Int32)x)<<24)|(((Int32)y)<<16)|(((Int32)z)<<8)|((Int32)v))& 0xffffffff)
#define U8ToS16(x,y)		(Int16)(((((Int16)x)<<8)|((Int16)y))&0xffff)
#define U8ToU16(x,y)		(Uint16)(((((Uint16)x)<<8)|((Uint16)y))&0xffff)

#define U8GetBit(data,x)	((data >> x) & 0x01)







#endif /* __MACRO_DEF_H */
