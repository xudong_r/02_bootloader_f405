/*
 * File      : TypeDef.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef _TYPE_DEF_H
#define _TYPE_DEF_H

typedef	unsigned char			Bool;
typedef	unsigned char			U8;
typedef	signed char				S8;
typedef	unsigned int			U32;
typedef	signed int				S32;
typedef	float					F32;
typedef	double					F64;
typedef	unsigned short			U16;
typedef	signed short			S16;
typedef	unsigned long long		U64;
typedef	signed long long		S64;
typedef unsigned char   		byte;

typedef S16             		Int16;
typedef S32            			Int32;
typedef U16    					Uint16;
typedef U32   					Uint32;
typedef U64 					Uint64;
typedef S64 					Int64;
typedef float           		Float32;
typedef double     				Float64;
typedef unsigned char   		Byte;


typedef unsigned char 			tdu1;
typedef signed char 			tds1;
typedef char		 			tdc1;
typedef unsigned short 			tdu2;
typedef signed short 			tds2;
typedef unsigned int 			tdu4;
typedef signed int 				tds4;
typedef float 					tdf4;
typedef double 					tdf8;
typedef int 					tboo;
typedef unsigned char			tdbl;
typedef unsigned long long 		tdu8;
typedef signed long long 		tds8;

#ifndef __cplusplus
typedef unsigned char  			bool;
#endif

#define TRUE   1
#define FALSE  0
#define NULL   0

#define LED_POLARITY_NEGATIVE	(0)		// 引脚输出低电平，LED点亮
#define LED_POLARITY_POSITIVE	(1)		// 引脚输出高电平，LED点亮

#define EXGPIO_DIR_INPUT    	(0)		// 引脚为输入
#define EXGPIO_DIR_OUTPUT   	(1)		// 引脚为输出



typedef union 
{
   tds4  s4;
   tdu4  u4;
   tdf4  f4;
   tdu1  byte[4];
}  tDWordBit;	      


//u16        ------->>> byte
typedef union 
{
   tds2  s2;
   tdu2  u2;
   tdu1  byte[2];
}  tWordBit;


typedef void (*tpFnGetBytes)(tdu1 *data,tdu2 length); 

typedef void (*tpFnGetIDBytes)(tdu1 ID,tdu1 *data,tdu2 length); 

typedef void (*tpFnWriteIDBytes)(tdu2 ID,tdu1* data,tdu2 lenghth);

typedef void (*tpFnGetIDBytes2)(tdu2 ID,tdu1 *data,tdu2 length); 

typedef tdbl (*tpFnWrite)(tdu1 *data, tdu2 length);

typedef void (*tpFnTxBytes)(tdu1 *data,tdu2 length); 

typedef void (*tpFnAction)(void); 

typedef void (*tpFnGetInt)(tds4 data);


#ifdef __arm    
#define ARM_PACKED	__packed
#else    
#define ARM_PACKED
#endif

#ifdef WIN32
#pragma pack(1)
#endif

#endif
