/*
 * File      : BLConfig.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-10-21     Ren Xudong   first implementation
 */
 
#ifndef __BL_CONFIG_H
#define __BL_CONFIG_H

#include "../BSP/BSPFile.h"
#include "stm32f4xx_hal.h"
#include "BSPFile.h"
#include "../Bsp/Flash.h"

#define BL_USE_UART_INDEX						(1)									// 

#define BL_APPLICATION_PARA_PAGENUM				MCU_FLASH_DR_PAGE_NUM				// 

#define BL_BOOTLOADER_START_PAGE				(0)
#define BL_APPLICATION_START_PAGE				(20)
#define BL_BOOTLOADER_INFO_START_PAGE			(BL_APPLICATION_START_PAGE - 1)


#define BL_APPLICATION_PARA_START_PAGE			(MCU_FLASH_PAGE_NUM - 2 - BL_APPLICATION_PARA_PAGENUM)
#define BL_APPLICATION_INFO_START_PAGE			(MCU_FLASH_PAGE_NUM - 2)
#define BL_UPDATEFLAG_START_PAGE				(MCU_FLASH_PAGE_NUM - 1)

#define BL_BOOTLOADER_START_ADDR				FLASH_PAGEADDR(BL_BOOTLOADER_START_PAGE)
#define BL_BOOTLOADER_INFO_ADDR					FLASH_PAGEADDR(BL_BOOTLOADER_INFO_START_PAGE)

// APP 启动地址
#define BL_APPLICATION_START_ADDR				(0x08010000)

// APP内需要保留在片内Flash的参数
//#define BL_APPLICATION_PARA_ADDR				FLASH_PAGEADDR(BL_APPLICATION_PARA_START_PAGE)

// APP信息
#define BL_APPLICATION_INFO_ADDR				(0x0800C000)
#define BL_APPLICATION_INFO_SECTOR_INDEX		(0x03)

#define BL_UPDATEFLAG_START_ADDR				FLASH_PAGEADDR(BL_UPDATEFLAG_START_PAGE)

//版本信息结构体定义
__packed typedef struct
{
    tdu2 SysType;
    tdu1 Major;
    tdu1 Minor;
    tdu1 Revision;
}tBLVersion;

//时间信息结构体
__packed typedef struct
{
    tdu2 Year;  	//年 2010
    tdu1 Month; 	//月 1-12
    tdu1 Day;   	//日 1-31
    tdu1 Hour;    	//时 0-23
    tdu1 Minute;    //分 0-59
    tdu1 Second;    //秒 0-59
}tTime;

//程序信息结构体
__packed typedef struct
{
    tBLVersion PackageVer;    	// 打包方式版本
    tdu4 ProgramSize;       	// 程序代码大小，单位字节
    tBLVersion ProgramVer;    	// 程序版本
    tTime ProgramTime;       	// 程序文件时间
    tdu4 ProgramCRC32;      	// 程序文件CRC32校验,指对原始bin文件进行CRC32校验的值
    tdu1 ProgramAESKey[16];
    tdu2 CRC16;             	// 此结构体首字节到CRC16前一个字节校验
}tProgramMessage;



#endif // __BL_CONFIG_H
