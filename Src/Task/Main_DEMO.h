/*
 * File      : Main_Demo.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef _MAIN_DEMO_H
#define _MAIN_DEMO_H

#ifdef __cplusplus
extern "C"
{
#endif
#include "TypeDef.h"
extern void DemoLed(void);

extern void DemoUart(void);
	
extern void TelemetryTask(void);

//extern void DemoFunction1(tdu8 tick);
//
//extern void DemoFunction2(tdu8 tick);

#ifdef __cplusplus
}
#endif
#endif /* __MAIN_DEMO_H */

