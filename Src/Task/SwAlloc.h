/*
 * File      : SwAlloc.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef __SW_ALLOC_H
#define __SW_ALLOC_H

#include "TypeDef.h"

extern void SwInit(void);

#endif /* __SW_ALLOC_H */
