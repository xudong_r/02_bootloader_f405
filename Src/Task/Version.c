/*
 * File      : Version.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "TypeDef.h"
#include "Version.h"

#include <string.h>
#include <stdio.h>

static tdc1 CompileDate[]=__DATE__;
static tdc1 CompileTime[]=__TIME__;
static tBuildTime BuildTime;
static tVersion Version;
static tdc1 BuildTimeBuf[sizeof(CompileDate) + sizeof(CompileTime)];

tdc1 *Version_GetBuildTimeStr(void)
{
	memcpy((void*)BuildTimeBuf,(void*)CompileDate,sizeof(CompileDate));
	BuildTimeBuf[sizeof(CompileDate) - 1] = ' ';
	BuildTimeBuf[sizeof(CompileDate)] = 0x00;
	strcat(BuildTimeBuf,CompileTime);
	return BuildTimeBuf;
}
static char* strMonth[]=
	{	"NUL","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

static tdu1 MonthDecode(char *str)
{	
	tdu1 i;		
	if(str==NULL)		
		return 0;		
	for(i=1;i<=12;i++)
	{		
		if( strcmp(str,strMonth[i]) ==0)			
			return i;	
	}		
	return 0;
}

tBuildTime *Version_GetBuildTime(void)
{
	tds4 year=0;	
	tds4 day=0;	
	tdu1 month=0;		
	tds4 hour=0,minute=0,second=0;		
	tdc1 tmpStr[32];		
	tdc1 strMon[4];		
	memset(tmpStr,0,32);		
	sprintf(tmpStr,"%s",__DATE__);		
	sscanf(tmpStr,"%s %d %d",strMon,&day,&year);
	month=MonthDecode(strMon);	
	memset(tmpStr,0,32);
	sprintf(tmpStr,"%s",__TIME__);
	sscanf(tmpStr,"%d:%d:%d",&hour,&minute,&second);	

	BuildTime.Year = year;	
	BuildTime.Month =month;	
	BuildTime.Day = day;	
	BuildTime.Hour = hour;	
	BuildTime.Minute = minute;	
	BuildTime.Second = second;	

	return &BuildTime;
}

tVersion *Version_Get(void)
{
	Version.SysType = VERSION_SYS_TYPE;
	Version.Major = VERSION_MAJOR_ID;
	Version.Minor = VERSION_MINOR_ID;
	Version.Revision = VERSION_REVISION_ID;
	return &Version;
}
