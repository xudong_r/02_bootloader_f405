/*
 * File      : Version.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef __VERSION_H
#define __VERSION_H

#include "TypeDef.h"
#include "VersionDef.h"

#ifdef __arm
    #define ARM_PACKED	__packed
#else
    #define ARM_PACKED
#endif

#ifdef WIN32
#pragma pack(1)
#endif



__packed typedef struct
{
	tdu1 SysType;
	tdu1 Major;
	tdu1 Minor;
	tdu1 Revision;
}tVersion;


__packed typedef struct 
{
	tdu2 Year;  	//年 如2010
	tdu1 Month; 	//月 1-12
	tdu1 Day;   	//日 1-31
	tdu1 Hour;    	//时 0-23
	tdu1 Minute;    //分 0-59
	tdu1 Second;    //秒 0-59
}tBuildTime;


extern tBuildTime *Version_GetBuildTime(void);

extern tdc1 *Version_GetBuildTimeStr(void);

extern tVersion *Version_Get(void);


#ifdef WIN32
#pragma pack()
#endif

#undef ARM_PACKED

#endif


