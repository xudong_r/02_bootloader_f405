/*
 * File      : HwAlloc.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __HW_ALLOC_H
#define __HW_ALLOC_H


extern void HwInit(void);


#endif /* __HW_ALLOC_H */
