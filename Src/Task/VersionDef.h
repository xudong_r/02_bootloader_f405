/*
 * File      : VersionDef.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef _VERSION_DEF_H
#define _VERSION_DEF_H

/**
 * 产品设备ID
 */
#define VERSION_SYS_TYPE				0x01
 
 
#define VERSION_MAJOR_ID	1
#define VERSION_MINOR_ID	0
#define VERSION_REVISION_ID	0

#endif

