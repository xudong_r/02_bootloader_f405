/*
 * File      : Start.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "HwAlloc.h"
#include "SwAlloc.h"
#include "../BSP/Flash.h"
#include "../XDOS/XDOS.h"
#include "../MacroLib/Accredit.h"    
#include "stm32f4xx_hal.h"
#include "CommTask.h"
#include "Main_DEMO.h"
#include "BLTask.h"
#include "Uart.h"


#ifdef BOOTLOADER_ENABLE
#include "../Bsp/BootLoader.h"
#endif

void Init(void);


static tdbl Boot(void)
{
	tdu1 res = 0;
	
	res = BLTask_CheckUpdate();

	if(res == 0) 
	{
		// 跳转APP
		BLTask_JumptoApp();
	}
	
	return res;
}

static void Config()
{
#ifdef ADC1_ENABLE
	ADC_Start(1);
#endif

#ifdef ADC2_ENABLE
	ADC_Start(2);
#endif
}
/**
 * @brief 注册执行任务
 *
 */
static void TaskInit(tdbl res)
{
	/**
	 * 设置Task分频系数
	 */
	XDOS_SetTaskDiv(10);

	if (res == TRUE)
	{
		//系统启动成功，注册授权相应启动任务
	}
	else if (res == FALSE)
	{
		//系统启动失败,注册未授权相应启动任务
	}

	XDOS_RegisterTask(DemoLed);

	XDOS_RegisterTask(BLTask_Entry);

}


/**************************以下代码无需修改***********************/


static void Init()
{
	#ifdef BOOTLOADER_ENABLE
	SCB->VTOR = FLASH_BASE | BOOTLOADER_OFFSET; /* Vector Table Relocation in Internal FLASH. */
	#warning "Bootloader Enable"
	#else
	#warning "Bootloader Disable"
	#endif
	
	HwInit();//硬件初始化

	SwInit();//软件初始化
}

static void Loop()
{
	while(1)
	{
		if (XDOS_GetTickFlag() == TRUE)
		{
			XDOS_ExecTask();
			XDOS_SetTickFlag(FALSE);
		}
	}
}


int main(void)
{
	tdbl bootRes = FALSE;
	//HAL_Delay(200);
	
	//初始化流程，此流程中需要初始化时钟、硬件、软件等
	Init();
	
	bootRes = Boot();
	
	//启动成功执行分支
	//配置流程，该流程中需要根据启动参数进行相应设备配置
	Config();

	//前后台系统的任务调度器初始化
	XDOS_Init();

	//任务初始化
	TaskInit(bootRes);

	//前后台系统的任务调度器启动
	XDOS_Start();

	Loop();
}
