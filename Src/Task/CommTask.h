/*
 * File      : CommTask.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef _COMMTASK_H
#define _COMMTASK_H

#ifdef __cplusplus
extern "C"
{
#endif
#include "TypeDef.h"
extern void CommTask_Entry(void);
extern void CommTask_RecCmd(tds4 length);

#ifdef __cplusplus
}
#endif
#endif /* __COMMTASK_H */

