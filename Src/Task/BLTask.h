/*
 * File      : BLTask.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-10-21     Ren Xudong   first implementation
 */

#ifndef __BL_TASK_H
#define __BL_TASK_H

#include "TypeDef.h"



#define UPLOADFLAG                 (*(volatile unsigned long *)0x20000000)	
	
extern void BLTask_Entry(void);
extern void BLTask_Init(void);
extern void BLTask_EnterApp_Init(void);

extern void BLTask_UserTask(void);
extern void BLTask_JumptoApp(void);

extern tdbl BLTask_CheckUpdate(void);

#endif /* end of __BL_TASK_H */
