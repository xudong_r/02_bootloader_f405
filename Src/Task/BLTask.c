/*
 * File      : BLTask.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-10-21     Ren Xudong   first implementation
 */

#include "BLTask.h"
#include "BLConfig.h"
#include "../SysLink/SysLinkRx.h"
#include "../SysLink/SysLinkTx.h"
#include "../MacroLib/IPA.h"
#include "../MacroLib/CRC.h"
#include "../BSP/Flash.h"

void BLTask_Entry(void)
{

	//接收处理
	SysLinkRx_Process();
	//发送处理
	SysLinkTx_Process();

}

void BLTask_Init(void)
{
	IPA_SetWriteFunc(Flash_GetWriteWorsHandle());//初始化写Flash接口句柄
	IPA_SetEraserFunc(FLASH_GetEraserPagesHandle());
}

void BLTask_EnterApp_Init(void)
{

}

void BLTask_UserTask(void)
{
	return;
}


__asm void MSR_MSP(tdu4 addr) 
{
	MSR MSP, r0 			//set Main Stack value
	BX r14
}
tpFnAction jump2app;
void BLTask_JumptoApp(void)
{
	tdu4 appxaddr = BL_APPLICATION_START_ADDR;
	if(((*(__IO uint32_t*)appxaddr)&0x2FF00000)==0x20000000)
	{ 
		jump2app=(tpFnAction)*(__IO uint32_t*)(0x08010000+4);	
		MSR_MSP(*(__IO uint32_t*)appxaddr);
		jump2app();
	}
}


static tdu4 CheckForceUpdate(void)
{
    unsigned long *pulApp;
	
	tdu4 crc32 = 0;
	
	
	tProgramMessage *programMessage = (tProgramMessage *)BL_APPLICATION_INFO_ADDR;
	
    pulApp = (unsigned long *)BL_APPLICATION_START_ADDR;
	
	if( ( (pulApp[0]&0x20000000)!=0x20000000) && ((pulApp[0]&0x10000000)!=0x10000000))
		return 1;
	
    if((pulApp[0] == 0xffffffff) ||  (pulApp[1] == 0xffffffff) )
    {
        return 1;
    }
	
	if(programMessage->ProgramSize > 1024 * 1024)
	{
		return 2;
	}
	
	crc32 = CRC32((tdu1 *)BL_APPLICATION_START_ADDR, programMessage->ProgramSize);
	
	if(crc32 != programMessage->ProgramCRC32)
		return 3;
	
	return 0;
}

tdbl BLTask_CheckUpdate(void)
{
	tdu1 res = 0xFF;
	if((UPLOADFLAG != 0xEBAB90A5) && (CheckForceUpdate() == 0))
	{
		res = 0;
	}
	else
		res = 1;
	
	return res;
	
}
