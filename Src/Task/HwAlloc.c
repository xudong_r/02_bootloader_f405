/*
 * File      : HwAlloc.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "../Bsp/Rcc.h"
#include "../Bsp/Led.h"
#include "../Bsp/ExGpio.h"
#include "../BSP/Uart.h"
#include "../BSP/UID.h"
#include "../BSP/ADC.h"
#include "../BSP/CAN.h"



//static void AdcInit(void)
//{
//#ifdef ADC1_ENABLE
//	ADC_HwInit(1);
//#endif

//#ifdef ADC2_ENABLE
//	ADC_HwInit(2);
//#endif
//}

//static void CanInit(void)
//{
//#ifdef CAN1_ENABLE
//	CAN_HwInit(1);
//#endif
//	
//#ifdef CAN2_ENABLE
//	CAN_HwInit(2);
//#endif

//}

//static void I2c_Init()
//{
//#ifdef I2C1_ENABLE
//    I2C_HwInit(1,400000);
//#endif

//#ifdef I2C2_ENABLE
//    I2C_HwInit(2,400000);
//#endif
//}




void HwInit(void)
{
	//时钟初始化
	Rcc_Init();

	//LED初始化
	LED_Init();

	//串口初始化
	Uart_Init();

	//CAN总线初始化
	UID_Init();
}

