/*
 * File      : CommTask.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#include "../SysLink/SysLinkRx.h"
#include "../SysLink/SysLinkTx.h"


void CommTask_Entry(void)
{
	SysLinkRx_Process();
	SysLinkTx_Process();
}
