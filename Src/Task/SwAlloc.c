/*
 * File      : SwAlloc.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#include "SwAlloc.h"
#include "../MacroLib/Accredit.h"    
#include "../BSP/UID.h"
#include "../BSP/Uart.h"
#include "../BSP/Can1.h"
#include "Flash.h"
#include "BLTask.h"



#include "../SysLink/SysLinkRx.h"
#include "../SysLink/SysLinkTx.h"

void SwInit(void)
{
//	tUartPara *pUartPara;
//	Accredit_Init(UID_GetUID(), UID_GetFlashCap());
	
	// 配置协议端口初始化
	SysLinkRx_Init();
	Uart_SetRxISRFunc(1, SysLinkRx_RawDataRx);
	SysLinkTx_SetWriteHandler(Uart_GetWriteHandler(1));
	
	BLTask_Init();
}

