/*
 * File      : Main_Demo.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */



#include "Main_DEMO.h"
#include "../Bsp/LED.h"
#include "../Bsp/Uart.h"
#include "../Bsp/Uart1.h"
#include "../Bsp/Uart2.h"
#include "../Bsp/Uart4.h"
#include "../Bsp/Uart5.h"
#include "Can.h"
typedef struct 
{
	tdu1 i0;
	tdu1 i1;
	tdu1 i2;
	tdu1 i3;
	tdu1 i4;
	tdu1 i5;
	tdu1 i6;
	tdu1 i7;
	tdu1 i8;
	tdu1 i9;
	tdu1 i10;
	tdu1 i11;
	tdu1 i12;
	tdu1 i13;
}tTemp;

tTemp Temp;


void DemoLed(void)
{
	static tdu1 cnt = 0;
	cnt++;
	if (cnt == 50)
	{
//		LED1_Toggle();
		LED2_Toggle();
//		LED3_Toggle();
		cnt = 0;
	}
}


static tdu1 testBuffer[10] = {0x0,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09};
void DemoUart(void)
{
	static tdu1 cnt = 0;	
	cnt++;

	if (cnt == 50) 
	{
		Temp.i13 = Temp.i12;
		Temp.i12 = Temp.i11;
		Temp.i11 = Temp.i10;
		Temp.i10 = Temp.i9;
		Temp.i9 = Temp.i8;
		Temp.i8 = Temp.i7;
		Temp.i7 = Temp.i6;
		Temp.i6 = Temp.i5;
		Temp.i5 = Temp.i4;
		Temp.i4 = Temp.i3;
		Temp.i3 = Temp.i2;
		Temp.i2 = Temp.i1;
		Temp.i1 = Temp.i0;
//		AviBus_SendMsg(0x21, &Temp.i0, sizeof(tTemp),FALSE);
		testBuffer[0]++;
		Temp.i0++;
		Uart1_WriteBytes_IT(&testBuffer[0],10);
//		Uart2_WriteBytes_IT(&testBuffer[0],10);
//		Uart4_WriteBytes_IT(&testBuffer[0],10);
//		Uart5_WriteBytes_IT(&testBuffer[0],10);
//		CAN_Write(1,0x20,&testBuffer[0] , 8);
//		CAN_Write(2,0x20,&testBuffer[0] , 8);
		cnt = 0;
		
	}
}
