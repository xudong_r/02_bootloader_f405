/*
 * File      : Accredit.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __ACCREDIT_H
#define __ACCREDIT_H

#ifdef __cplusplus
extern "C" 
{
#endif

#include "TypeDef.h"

extern void Accredit_Init(tdu1* pChipUid,tdu1* pChipFlashCap);

extern tdbl Accredit_VerifyKey(void);

extern tdu1* Accredit_GetSN(void);

extern tdu1* Accredit_GetKey(void);

extern tdu1* Accredit_GetExKey(void);

extern void Accredit_SetExKey(tdu1 *exKey);

extern tdbl Accredit_GetAccreditStatus(void);
#ifdef __cplusplus
}
#endif
#endif /* __ACCREDIT_H */ 
