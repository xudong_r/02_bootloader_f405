/*
 * File      : IPA.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-10-23     Ren Xudong   first implementation
 */

#ifndef __IPA_H
#define __IPA_H

#ifdef __cplusplus

#endif


#ifdef __cplusplus
extern "C"{
#endif

typedef unsigned int (*tpFnWriteFlash)(unsigned int pFlashAddress, unsigned int* pWordByte, unsigned int wordLength);

typedef unsigned int (*tpFnEraserFlash)(unsigned short sectorIndex, unsigned short numOfSector);

extern void IPA_SetWriteFunc(tpFnWriteFlash fp);

extern void IPA_SetEraserFunc(tpFnEraserFlash fp);

extern unsigned int IPA_WriteAppToFlash(volatile unsigned int *pFlashAddress, unsigned int* pData, unsigned int nDataLength);

extern unsigned int IPA_EraserFlash(void);

#ifdef __cplusplus
}
#endif


#endif //__IPA_H

