/*
 * File      : AES.h
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-10-26     Ren Xudong   first implementation
 */
#ifndef _AES_H
#define _AES_H

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
/**
 * 参数 p: 明文的字符串数组。
 * 参数 plen: 明文的长度,长度必须为16的倍数。
 * 返回值 0:成功
 *        1:失败
 */
unsigned char AES_Encrypt(char *p, int plen);

/**
 * 参数 c: 密文的字符串数组。
 * 参数 clen: 密文的长度,长度必须为16的倍数。
 * 返回值 0:成功
 *        1:失败
 */
unsigned char AES_Decrypt(char *c, int clen);

/**
 * 参数 k: 密钥字符串
 * 参数 kLen: 密钥长度
 * 返回值：0 成功, 1 长度错误
 */
unsigned char AES_SetKey(char *k, int kLen);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _AES_H
