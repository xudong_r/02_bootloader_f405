/*
 * File      : IPA.c
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-10-23     Ren Xudong   first implementation
 */

#include "../MacroLib/IPA.h"

tpFnWriteFlash WriteToFlash = 0;
void IPA_SetWriteFunc(tpFnWriteFlash fp)
{
	if(WriteToFlash == 0)
	{
		WriteToFlash = fp;
	}
	else
		return;
}

static unsigned char IsFlashEraser = 0xFF;
unsigned int IPA_WriteAppToFlash(volatile unsigned int *pFlashAddress, unsigned int* pData, unsigned int nDataLength)
{
	// 不擦除成功,也能写,但写入错误
	
	unsigned int res = 0xFF;
	
	if((IsFlashEraser == 0) && (WriteToFlash != 0))
	{
		res = WriteToFlash(*pFlashAddress, pData, nDataLength);
	}
	else
		res = 1;
	return res;
	
}

tpFnEraserFlash EraserFlashHandle = 0;
void IPA_SetEraserFunc(tpFnEraserFlash fp)
{
	if(EraserFlashHandle == 0)
	{
		EraserFlashHandle = fp;
	}
	else
		return;
}


unsigned int IPA_EraserFlash(void)
{
	// Flash 尝试擦除失败后，再次尝试擦除次数
	unsigned int eraserRetryTime = 5;
	
	//每次擦除前复位擦除成功标志位
	IsFlashEraser = 0xFF;
	do
	{
		IsFlashEraser = EraserFlashHandle(4, 8);
	}
	while(IsFlashEraser && eraserRetryTime--);
	
	//擦除成功,返回0; 擦除失败,返回0xFF
	return IsFlashEraser;
}

