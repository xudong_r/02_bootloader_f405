/*
 * File      : CRC.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */
#ifndef __CRC_H
#define __CRC_H

#include "TypeDef.h"

//#define USE_CRC_STANDARD
#define USE_CRC_PRIVATE


extern tdu2 CRC_16(tdu1 *pBytes,tdu4 Length);

extern tdu4 CRC32(tdu1 *string, tdu4 size);

#endif /* __CRC_H */
