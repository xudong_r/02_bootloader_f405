#include "Accredit.h"
#include "../BSP/UID.h"
#include <string.h>
static tdbl AccreditStatus = FALSE;
static tdu1 AccreditSN[16];
static tdu1 AccreditKey[16]; //用于存储根据SN计算得到的Key
static tdu1 AccreditExKey[16];//用于存储外部注入的Key
/**
 * @brief 初始化函数
 *
 * @param pChipUid 芯片UID，芯片12字节
 *
 * @param pChipFlashCap 芯片容量，长度2字节
 */
void Accredit_Init(tdu1* pChipUid,tdu1* pChipFlashCap)
{
//    ACCREDIT_GetSN(&AccreditSN[0],pChipUid,pChipFlashCap);

//    ACCREDIT_GetKey(&AccreditKey[0],&AccreditSN[0]); 

	memset((void *)&AccreditExKey[0],0,16);
}


/**
 * @brief 验证授权码函数
 * 
 * @param Key 授权码指针
 * 
 * @return tdu1 0 授权码错误 1 授权码正确
 */
tdbl Accredit_VerifyKey()
{
    if (memcmp(&AccreditKey[0],&AccreditExKey[0],16)==0) 
	{
		// 授权码校验合格
		AccreditStatus = TRUE;
    }
	else
    {
		// 授权码校验失败
		AccreditStatus = FALSE;
    }
	return AccreditStatus;
}

tdu1* Accredit_GetSN()
{
	return &AccreditSN[0];
}

tdu1* Accredit_GetKey()
{
	return &AccreditKey[0];
}

tdu1* Accredit_GetExKey(void)
{
	return &AccreditExKey[0];
}

void Accredit_SetExKey(tdu1 *exKey)
{
	memcpy((void *)&AccreditExKey[0], (void *)exKey,16);
}

tdbl Accredit_GetAccreditStatus(void)
{
	return AccreditStatus;
}
