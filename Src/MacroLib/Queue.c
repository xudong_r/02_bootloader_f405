/*
 * File       : Queue.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-13     Ren Xudong   first implementation
 */
#include <string.h>
#include "Queue.h"
#include "MemApply.h"

static void Queue_StartAdd(tQueue* q)
{
    q->Start++;
    if (q->Start>=q->Length)
    {
        q->Start-=q->Length;
    }
}

// 初始化队列
void Queue_Init(tQueue* q, tdu4 length)
{
	q->Start = 0;
	q->Count = 0;
	q->Length = length;
	q->Data = (tdu1*)MemApply(length*sizeof(tdu1));
}

// 清除队列中的数据
void Queue_Clear(tQueue* q)
{
	q->Start=0;
	q->Count=0;
	memset((void*)q->Data, 0x00, q->Length);
}
// 返回值 = TRUE时，说明队列已满，新插入的数据已经挤占最老的数据的位置
tdu1 Queue_Insert(tQueue *q, tdu1 c)
{
    tdu4 t=q->Start + q->Count;
    if (t>=q->Length)
    {
        t-=q->Length;
    }
    q->Data[t]=c;
    if (q->Count==q->Length)
    {
        Queue_StartAdd(q);
        return TRUE;
    }
    else
    {
        q->Count++;
        return FALSE;
    }
}
tdbl Queue_Pop(tQueue *q, tdu1 *data)
{
	tdu1 c;
	if(q->Count>0)
	{
		c=q->Data[q->Start];
		Queue_StartAdd(q);
		q->Count--;
		*data = c;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

// 队列中出丢一个数组，length为数组的长度，返回值为真是出兑的数据的长度
// 会从队列中删除数据
tdu4 Queue_PopBytes(tQueue *q,tdu1 *data,tdu4 length)
{
    tdu4 copyLength;
	if(q->Count == 0)
		return 0;
	if(length >= q->Count)
	{
		if(q->Start + q->Count < q->Length)
			memcpy(data, &q->Data[q->Start], q->Count);
		else
		{
			memcpy(data, &q->Data[q->Start], q->Length - q->Start);
			memcpy(&data[q->Length - q->Start], &q->Data[0], q->Count - q->Length + q->Start);
		}
		copyLength = q->Count;
	}
	else//length < q->Count
	{
		if(q->Start + length < q->Length)
			memcpy(data, &q->Data[q->Start], length);
		else
		{
			memcpy(data, &q->Data[q->Start], q->Length - q->Start);
			memcpy(&data[q->Length - q->Start], &q->Data[0], length - q->Length + q->Start);
		}
		copyLength= length;
	}
    // 删除队列中数据，修改数据指针的起始位置，修改队列中元素的数据量
    q->Start += copyLength;
    q->Start %= q->Length;
    q->Count -= copyLength;
	return copyLength;
}
// 返回TRUE，说明已经挤出队列元素
tdbl Queue_Enqueue(tQueue *q, tdu1 *data, tdu4 length)
{
	tdbl flag = FALSE;
	tdu4 i;
	for(i=0;i<length;i++)
		flag = Queue_Insert(q, data[i]);
	return flag;
}
// 返回FALSE，操作不成功，因为对列中的元素少于length
tdbl Queue_Dequeue(tQueue *q, tdu1 *data, tdu4 length)
{
	//tdu1 temp;
	tdu4 i;
	if(q->Count < length)
		return FALSE;
	for(i=0;i<length;i++)
		Queue_Pop(q, &data[i]);

	return TRUE;
}

// 从队列中拷贝出一个数组，length为数组的长度，返回值为是拷贝出来数据真实长度
// 不会删除队列中的任何数据
tdu4 Queue_ToArray(tQueue *q, tdu1 *data, tdu4 length)
{
	tdu4 copyLength;
	if(q->Count == 0)
		return 0;
	if(length >= q->Count)
	{
		if(q->Start + q->Count < q->Length)
			memcpy(data, &q->Data[q->Start], q->Count);
		else
		{
			memcpy(data, &q->Data[q->Start], q->Length - q->Start);
			memcpy(&data[q->Length - q->Start], &q->Data[0], q->Count - q->Length + q->Start);
		}
		copyLength = q->Count;
	}
	else//length < q->Count
	{
		if(q->Start + length < q->Length)
			memcpy(data, &q->Data[q->Start], length);
		else
		{
			memcpy(data, &q->Data[q->Start], q->Length - q->Start);
			memcpy(&data[q->Length - q->Start], &q->Data[0], length - q->Length + q->Start);
		}
		copyLength= length;
	}
	return copyLength;
}

// 按插入顺序读取数据，如果指定位置不存在则返回FALSE，成功TURE
// 不会删除队列中的元素
tdbl Queue_Read(tdu1 *data, tQueue *q, tdu4 index)
{
	tdu4 t;
	//tdu1 c;
    if(index < q->Count)
    {
		t=q->Start+index;
        if (t >= q->Length)
        {
            t-=q->Length;
        }
		*data=q->Data[t];
        return TRUE;
    }
    return FALSE;
}
