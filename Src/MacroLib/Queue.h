#ifndef _QUEUE_H
#define _QUEUE_H

#include "TypeDef.h"

__packed typedef struct 
{
	tdu1 *Data;
	tdu4 Length;
	tdu4 Start;
	tdu4 Count;
}tQueue;

// 初始化队列
extern void Queue_Init(tQueue* q, tdu4 length);

// 清楚队列中的数据
extern void Queue_Clear(tQueue* q);

// 返回值 = TRUE时，说明队列已满，新插入的数据已经挤占最老的数据的位置
extern tdu1 Queue_Insert(tQueue *q, tdu1 c);

// 出队一个元素，返回TRUE：正确弹出，返回FALSE：未弹出
extern tdbl Queue_Pop(tQueue *q, tdu1 *data);

// 入队一个数组，返回TRUE：已经挤出队列元素
extern tdbl Queue_Enqueue(tQueue *q, tdu1 *data, tdu4 length);

// 出队一个数组，返回FALSE：队列中的元素少于length
extern tdbl Queue_Dequeue(tQueue *q, tdu1 *data, tdu4 length);

// 队列中出丢一个数组，length为数组的长度，返回值为真是出兑的数据的长度
// 会从队列中删除数据
extern tdu4 Queue_PopBytes(tQueue *q,tdu1 *data,tdu4 length);

// 从队列中拷贝出一个数组，length为数组的长度，返回值为是拷贝出来数据真实长度
// 不会删除队列中的任何数据
extern tdu4 Queue_ToArray(tQueue *q, tdu1 *data, tdu4 length);

// 按插入顺序读取数据，如果指定位置不存在则返回FALSE，成功TURE
// 不会删除队列中的元素
extern tdbl Queue_Read(tdu1 *data, tQueue *q, tdu4 index);


#endif
