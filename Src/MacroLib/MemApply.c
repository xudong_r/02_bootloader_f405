/*
 * File      : MemApply.c
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#include "TypeDef.h"
#include "MemApply.h"


//#define MEM_BUF_SIZE		58000//78000 //20160627 ������������44000
#define MEM_BUF_SIZE		8192
__align(4) tdu1 MemBuf[MEM_BUF_SIZE];

static tdu4 MemP = 0x00;

tdu4 GetMemUsed(void)
{
	return MemP;
}

tdu1 *MemApply(tdu4 size)
{
	tdu1 *p;
	tdu4 usage;
	//ENTER_CRITICAL();
	p = &MemBuf[MemP];
	
	if( (size%4)!=0)
		size=size+(4-size%4);
		
	MemP += size;
	usage = MemP;
	//LEAVE_CRITICAL();
	if(usage>MEM_BUF_SIZE)
	{	
		while(1);
	}
	else 
		return p;
}









