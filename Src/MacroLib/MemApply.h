/*
 * File      : MemApply.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */


#ifndef __MEM_APPLY_H
#define __MEM_APPLY_H

#include "../include/TypeDef.h"




extern tdu4 GetMemUsed(void);

extern tdu1 *MemApply(tdu4 size);




#endif /* __MEM_APPLY_H */
