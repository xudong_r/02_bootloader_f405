/*
 * File       : OsPort.h
 * 
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-15     Ren Xudong   first implementation
 */

#ifndef __OSCONFIG_H
#define __OSCONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************** 
                   STEP1：任务调度器配置
*******************************************************/
//#define RTOS_ENABLE				//使用实时操作系统进行任务调度
#define XDOS_ENABLE				//使用前后台系统进行任务调度

/******************************************************/ 

/****************************************************** 
                   STEP2：任务调度器调度配置
*******************************************************/
#ifdef RTOS_ENABLE

#endif

/**
 * 前后台系统，约定如下： 
 * 前台任务（Task）为Main循环中执行Task，执行顺序与任务注册顺序有关，执行频率为XDOS_TICK_TIM_FRE/Task分频系数 
 * 后台任务(IntTask)为定时器中断中执行Task，执行频率为XDOS_TICK_TIM_FRE/任务注册时分频系数 
 * XDOS_TICK_TIM_INDEX指定前后台系统中用于提供中断的时钟序号 
 */
#ifdef XDOS_ENABLE

#define XDOS_TASK_MAX_NUM		(15)		//前台任务最大数量
#define XDOS_INTTASK_MAX_NUM	(5)			//后台任务最大数量
#define XDOS_TICK_TIM_INDEX		(4)			//用于产生时钟的定时器序号
#define XDOS_TICK_TIM_FRE		(1000)		//用于产生时钟的定时器产生中断频率


#endif


#ifdef __cplusplus
}
#endif
#endif /* __OSCONFIG_H */ 



